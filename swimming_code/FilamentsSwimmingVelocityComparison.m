function FilamentsSwimmingVelocityComparison(varargin)

% A prescribed active moment travelling wave is applied to filaments of
% different characteristic sperm number. The swimming velocity is
% calculated and compared between filaments to assertain how mechanical
% stiffness affects swimming speed. The experiment is repeated for two
% types of travelling wave; sperm-like and worm-like.

if ~isempty(varargin)
    skip = varargin{1};
else
    skip = 0;
end

close all; clc;

%% Setup and preliminaries.

Spvec   = [11.3:0.25:15];
NSp     = length(Spvec);
type    = {'sperm','worm'};
tmax    = 30;

% Travelling wave parameters:

m0      = 0.022;
wavk    = 4*pi;

% Filament array parameters:

grid    = [1,1];
sep     = [1,1];
Q       = 40;

%% Get distance data.

if skip == 0
    for t = 1:2
        probtype = type{t};
        for j = 1:NSp
            Sp4     = Spvec(j)^4;
            timer   = tic;
            [sol(t,j).d, tps(t,j).d, crds(t,j).d] = FilamentsSwimmingFunction(Sp4,m0,wavk,grid,sep,Q,tmax,probtype);
            wtime(t,j)  = toc(timer);
        end
    end
    save('~/Documents/mf-eif/swimming-code/workspaces/SwimmingVelocityData-3.mat')
end

%% Get velocity and other info from solution data.

load('~/Documents/mf-eif/swimming-code/workspaces/SwimmingVelocityData.mat')

tps     = tps(1,1).d;
Ntps    = length(tps);

% Extract evolving filament head position over time.
for t=1:2
    for j = 1:NSp
        fpos(t,j).X = horzcat(crds(t,j).d(:).x);
        fpos(t,j).Y = horzcat(crds(t,j).d(:).y);
        
        head(t,j).x     = fpos(t,j).X(1,:);
        head(t,j).y     = fpos(t,j).Y(1,:);
    end
end

% Compute head movement wavelength and peroid by calculating intersections
% with the line y=0.
for t = 1:2
    for j = 1:NSp
        [head(t,j).inx,~]       = intersections(head(t,j).x,head(t,j).y,[-10,10],[0,0]);
        head(t,j).wavL          = abs(head(t,j).inx(1)-head(t,j).inx(3));
        
        for n = 1:length(head(t,j).inx) % hi res dt (>5000) needed to resolve this.
            J                   = find( abs(head(t,j).x - head(t,j).inx(n)) < 1e-4);
            head(t,j).tinxidx(n)= J(1);
            head(t,j).tinx(n)   = tps(J(1));
        end     
        head(t,j).period        = head(t,j).tinx(1) - head(t,j).tinx(3);
        head(t,j).vel           = head(t,j).wavL/head(t,j).period;
    end
end

%% Config trace for specific filaments
%  These plots draw the cartesian shape of the swimming filaments.
%  Can be pretty but require more setup to get looking right.

f1 = figure;
p = panel(f1,'no-manage-font');
p.pack(2); p.margin = 25;

for sf = 1:2
    % sf = 1: sperm-like swimming.
    % sf = 2: worm-like swimming.
    
    p(sf).select();
    hold on; box on;

    fbox            = gca;
    fbox.LineWidth  = 1.5;
    fbox.FontSize   = 12;
    fbox.Layer      = 'Top';

    % Find time point sufficiently long enough through simulation so that
    % initial flexing is complete.
    tstart  = find(abs(tps-4)<1e-1); tstart = tstart(1);
    tplot   = [tps(tstart),tps(end)];
    colors  = parula(Ntps);
    
    hvec    = [];
    for i = tstart:Ntps
        [~,~,fil,~]     = GetFilamentData(sol(sf,3).d(:,i),1,Spvec(3)^4);
        if mod(i,600)==1
            axSize      = GetAxes(fil,grid,0.7);
            axis(axSize)  
            plot(fil.x,fil.y,'r','LineWidth',1.5,'color',colors(i,:))
        end
        hvec = [hvec,[fil.x(1);fil.y(1)]];
    end
    plot(hvec(1,:),hvec(2,:),'k-','LineWidth',1.6)

    currax = gca;
    % axis([currax.XLim(1),currax.XLim(2),-0.1,0.1])
    cbar                        = colorbar;
    cbar.Label.String           = 'Time, $t$';
    cbar.Label.Interpreter      = 'latex';
    cbar.TickLabelInterpreter   = 'latex';
    cbar.Limits                 = [tplot(1),tplot(2)];
    cbar.FontSize               = 14;
    caxis([tplot(1),tplot(end)])

    title('$  $','Interpreter','latex')

    set(gca,'TickLabelInterpreter','latex')
    xlabel('$x$','Interpreter','latex','FontSize',15)
    ylabel('$y$','Interpreter','latex','FontSize',15)
end

%% Tangent angle shape
%  These plots draw the tangent angle of chosen filaments as it evolves in
%  time. Nicer to compare as all relative to the body frame of the swimmer.

f2 = figure;
p2 = panel(f2,'no-manage-font');
p2.pack(2,2); 

for sf = 1:2
    % sf = 1: sperm-like swimming.
    % sf = 2: worm-like swimming.
    
    % Find time point sufficiently long enough through simulation so that
    % initial flexing is complete.
    tstart  = find(abs(tps-4)<1e-1); tstart = tstart(1);
    tplot   = [tps(tstart),tps(end)];
    colors  = parula(Ntps);
    
    % first sperm number choice
    p2(1,sf).select();
    hold on; box on;
    fbox            = gca;
    fbox.LineWidth  = 1.5;
    fbox.FontSize   = 16;
    fbox.Layer      = 'Top';
    SpPick          = 1;
    for i = tstart:Ntps
        [~,~,fil,~]     = GetFilamentData(sol(sf,SpPick).d(:,i),1,Spvec(SpPick)^4);
        if mod(i,600)==1
            plot(fil.sm,fil.th-fil.thav,'r','LineWidth',1.5,'color',colors(i,:))
        end
    end
    if sf == 1
        ylabel('$\theta^{[m]}(s,t)$','Interpreter','latex','FontSize',16)
    elseif sf == 2
        cbar                        = colorbar;
        cbar.Label.String           = 'Time, $t$';
        cbar.Label.Interpreter      = 'latex';
        cbar.TickLabelInterpreter   = 'latex';
        cbar.Limits                 = [tplot(1),tplot(2)];
        cbar.FontSize               = 14;
        caxis([tplot(1),tplot(end)])
    end
    set(gca,'TickLabelInterpreter','latex')
    
    % second sperm number choice
    p2(2,sf).select();
    hold on; box on;
    fbox            = gca;
    fbox.LineWidth  = 1.5;
    fbox.FontSize   = 16;
    fbox.Layer      = 'Top'; 
    SpPick          = 5;
    for i = tstart:Ntps
        [~,~,fil,~]     = GetFilamentData(sol(sf,SpPick).d(:,i),1,Spvec(SpPick)^4);
        if mod(i,600)==1
            plot(fil.sm,fil.th-fil.thav,'r','LineWidth',1.5,'color',colors(i,:))
        end
    end
    if sf == 1
        ylabel('$\theta^{[m]}(s,t)$','Interpreter','latex','FontSize',16)
    elseif sf == 2
        cbar                        = colorbar;
        cbar.Label.String           = 'Time, $t$';
        cbar.Label.Interpreter      = 'latex';
        cbar.TickLabelInterpreter   = 'latex';
        cbar.Limits                 = [tplot(1),tplot(2)];
        cbar.FontSize               = 14;
        caxis([tplot(1),tplot(end)])
    end
    set(gca,'TickLabelInterpreter','latex')
    xlabel('$\tilde{s}^{[m]}$','Interpreter','latex','FontSize',16)
end

set(f2,'Position',[300 300 646 520]);

%% Filament VAL swimming velocity graph.

f3 = figure;
hold on; box on;
fbox            = gca;
fbox.LineWidth  = 1.5;
fbox.FontSize   = 16;
fbox.Layer      = 'Top';

spermV  = horzcat(head(1,:).vel);
wormV   = horzcat(head(2,:).vel);

plot(Spvec,spermV,'-','LineWidth',1.5)
plot(Spvec,wormV,'--','LineWidth',1.5)

xlabel('$\textup{Sp}$','Interpreter','latex','FontSize',16)
ylabel('$VAL$','Interpreter','latex','FontSize',16)
set(gca,'TickLabelInterpreter','latex')

lgd = legend('$ m_1(s,t) $','$m_2(s,t) $');
lgd.Interpreter = 'latex';
lgd.FontSize    = 16;
lgd.EdgeColor   = 'none';
lgd.Location    = 'northwest';

end  % function