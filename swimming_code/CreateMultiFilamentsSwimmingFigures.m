function CreateMultiFilamentsSwimmingFigures(source)

close all;

switch source
    case 'local'
        if exist('./workspaces/figure16-data.mat','file')
            solve_skip = 1;
        else
            solve_skip = 0;
        end
    case 'online'
        solve_skip = 1;
        if isempty(dir('./workspaces/figure16-data*')) == 1
            disp('Downloading online solution data... ')
            url  = 'http://edata.bham.ac.uk/409/4/figure16-data.mat';
            websave('./workspaces/figure16-data.mat',url);
            disp('Download complete!')
        end
end

%% Setup and prerequisites.

grid    = [2,1];            % dimensions of filament array.
Q       = 40;               % discretisation parameter.
m0      = 5e-2;             % active moment amplitude.
Sp      = [8,8];            % swimming parameter for each filament.
wavk    = 4*pi;             % active moment wave number.
sep     = 0.1:0.01:0.6;     % vector of filament separations Y0.
Nsep    = length(sep);
tmax    = 24*pi;            % max time of simulations.
Nfil    = grid(1)*grid(2);  % number of filaments.

%% Calculate solution data:

% Create model structures before parfor loop.
for k = 1:Nsep
    model(k).M = CreateModelStruct(Q,grid,[0,sep(k)],'swimming',Sp.^4,wavk,[m0,m0]);
end

if solve_skip == 0
    parfor k = 1:Nsep
        disp(['k=',num2str(k),'/',num2str(Nsep)]) % progress tracker
        
        % Problem initialisation:
        Y0 = GetParabolicIntCond(1e-3,model(k).M); 
        Y0_1 = [ Y0(1);   Y0(2);  -Y0(3:Q+2)   ];  % initial condition of 'top' filament.
        Y0_2 = [ Y0(Q+3); Y0(Q+4); Y0(Q+5:end) ];  % initial condition of 'bottom' filament.
        
        % Run simulation:
        [S,T,C] = FilamentsSwimmingFunction( model(k).M, [Y0_1; Y0_2], tmax, 'sperm', 'nobar');
        
        % Store data:
        sol(k).S = S;
        tps(k).T = T;
        crd(k).T = C;
    end
    save('./workspaces/figure16-data.mat')
else
    fprintf('Loading solution data... ')
    load('./workspaces/figure16-data.mat','sol','tps','sep','model')
    fprintf('complete!\n')
end

clear fil

%% Compute VAL from solution data:

VALav   = zeros(Nsep,1);
for k = 1:Nsep
    sol_fil_1               = sol(k).S(1:Q+2,:);
    sol_fil_2               = sol(k).S(Q+3:end,:);
    [VAL_fil_1(k),dX(k)]    = CalcVAL(sol_fil_1, tps(k).T, 'x0');
    VAL_fil_2(k)            = CalcVAL(sol_fil_2, tps(k).T, 'x0');
    VALav(k)                = 0.5 * (VAL_fil_1(k) + VAL_fil_2(k));
end

%% Plot: two filaments swimming shape.

f1  = figure;
p1  = panel(f1,'no-manage-font'); p1.pack('h',[{1/3} {2/3}]);
p1(2).select();
box on; hold on;
fbox            = gca;
fbox.LineWidth  = 1.5;
fbox.FontSize   = 15;
fbox.Layer      = 'Top';
fbox.TickLabelInterpreter = 'latex';

t       = tps(1).T;
Ntps    = length(t);
tplot   = 6*pi:8:tmax;
col     = parula(length(tplot));

for k = 6
    for i   = 1:Ntps
        [~,~,fil,~]  = GetFilamentData(sol(k).S(:,i),grid(1)*grid(2),Sp);
        for n = 1:Nfil
            hpth(n).fil(:,i) = [fil(n).x(1); fil(n).y(1)];
        end
        [check,idx]  = max(abs(t(i)-tplot)<0.78e-1);
        if check == 1 && i ~= 1
            trang(idx) = i;
            axis([-0.4 1 -0.1 0.25])
            for n = 1:Nfil
                plot(fil(n).x,fil(n).y,'-','LineWidth',1.5,'Color',col(idx,:));
            end
        end
    end
    for n = 1:Nfil
        plot(hpth(n).fil(1,trang(1):trang(end)), hpth(n).fil(2,trang(1):trang(end)),'m','LineWidth',1.6);
    end
end

xlabel('$x$','Interpreter','latex','FontSize',16)
ylabel('$y$','Interpreter','latex','FontSize',16)

cbar                    = colorbar;
cbar.Ticks              = [6*pi,24*pi];
cbar.TickLabels         = {'$6\pi$','$24\pi$'};
cbar.Label.String       = 'Time, $t$';
cbar.Label.Interpreter  = 'latex';
cbar.TickLabelInterpreter='latex';
cbar.Limits             = [tplot(1),24*pi];
cbar.FontSize           = 16;
caxis([tplot(1),tplot(end)])

%% Plot: swimming velocity against separation.

p1(1).select();
box on; hold on;
fbox            = gca;
fbox.LineWidth  = 1.5;
fbox.FontSize   = 15;
fbox.Layer      = 'Top';
fbox.TickLabelInterpreter = 'latex';

plot(sep(1:Nsep),VALav(1:Nsep),'-','LineWidth',1.5)

axis tight
% axis([0.1 0.6 1.85e-4 2.2e-4])

xlabel('$\Delta Y_0$','Interpreter','latex','FontSize',16)
ylabel('$\overline{\textup{VAL}}$','Interpreter','latex','FontSize',16)

xticks([0.1 0.5 1])
xticklabels({'0.1','0.5','1'})

set(gcf,'Position',[300 300 840 350])
p1.margin = 30;
p1(1).marginright = 20;

%% Subfigure annotations

annotation(...
    'textbox',[.1,.565,.3,.3],...
    'interpreter','latex',...
    'string','(a)',...
    'fitboxtotext','on',...
    'edgecolor','none',...
    'backgroundcolor','none',...
    'fontsize',18 ...
    );

annotation(...
    'textbox',[.43,.565,.3,.3],...
    'interpreter','latex',...
    'string','(b)',...
    'fitboxtotext','on',...
    'edgecolor','none',...
    'backgroundcolor','none',...
    'fontsize',18 ...
    );

end % function
