% NEW CreateSwimmingFilamentPlots

function CreateSwimmingFilamentPlots(source)

% 'source' indicates whether to compute simulations locally ('local') or
% download from an online repository ('online').

%% Compute/Download and then load simulation data

switch source
    case 'local'
        M0      = round(linspace(3e-3,3e-2,30),4);  % varying moment amplitude.
        NM0     = length(M0);
        S       = 5:0.1:20;                         % varying swimming parameter.
        NS      = length(S);
        N_sim   = NS * NM0;                         % total number of simulations to run.
        Q       = 40;                               % discretisation parameter.
        
        if ~exist(['workspaces/figure8/','data_',num2str(N_sim),'_',num2str(N_sim),'.mat'],'file')
            
            % Create folders to contain solution data:
            mkdir('workspaces/figure8');
            
            % Run simulations (takes a long time!):
            k       = 4*pi;
            arr     = [1,1];
            sep     = [0,0];
            tmax    = 6*pi;
            
            % Run sperm-like simulations:
            sim_count = 1; % simulation number.
            for s = 1:NS
                for m = 1:NM0
                    
                    disp([num2str(s*m),'/',num2str(N_sim)])     % track progress in command window.
                    
                    model   = CreateModelStruct(Q, arr, sep, 'swimming', S(s), k, M0(m));   % model parameters.
                    Y0      = GetParabolicIntCond(1e-3,model);                              % parabolic initial condition.
                    
                    [sol_s,tps_s,~]     = FilamentsSwimmingFunction(model, Y0, tmax, 'sperm', 'nobar');  % sperm solution data.
                    [sol_w,tps_w,~]     = FilamentsSwimmingFunction(model, Y0, tmax, 'worm',  'nobar');  % worm solution data.
                    
                    % Store data by (S,M0) pairs:
                    data(s,m).S         = S(s);
                    data(s,m).M0        = M0(m);
                    data(s,m).sol_s     = sol_s;
                    data(s,m).sol_w     = sol_w;
                    data(s,m).tps_s     = tps_s;
                    data(s,m).tps_w     = tps_w;
                    
                    % Remove previous workspace (save disk space!)
                    delete('workspaces/figure8/*.mat')
                    
                    % Incrementally save data:
                    save(['workspaces/figure8/data_',num2str(sim_count),'_',num2str(N_sim),'.mat'], 'data');
                    sim_count = sim_count + 1;
                    
                end % M0 loop
            end % S loop
            
        else 
            % Load pre-computed simulation data:
            load(['workspaces/figure8/','data_',num2str(N_sim),'_',num2str(N_sim),'.mat'],'data')
        end
end

%% Compute VAL for each simulation

t_length    = 120;              % length of tps if simulation completed successfully (without self-intersection).
val_s       = zeros(NS,NM0);
val_w       = zeros(NS,NM0);
val_type    = 'xc';

for s = 1:NS
    for m = 1:NM0
        
        % Check if simulation actually completed - if not, set VAL to 0.
        if length(data(s,m).tps_s) < t_length
            val_s(s,m) = 0;
        else
            val_s(s,m) = CalcVAL( data(s,m).sol_s, data(s,m).tps_s, val_type );   % calculate sperm VAL.
        end
        if length(data(s,m).tps_w) < t_length
            val_w(s,m) = 0;
        else
            val_w(s,m) = CalcVAL( data(s,m).sol_w, data(s,m).tps_w, val_type );   % calculate worm VAL.
        end
    end
end

%% Plot VAL for sperm swimmer
% Panel 1 (left) of VAL figure.

% Instantiate subplot and set axes:
figure;
subplot(1,2,1);

% Plot data:
[S_mesh, M0_mesh] = meshgrid( S, M0 );
pcol = surf( S_mesh, M0_mesh, val_s', 'EdgeColor', 'None' ); 
view(2); axis tight

% Add box:
fbox = gca; box on; grid off; fbox.LineWidth = 1.5; fbox.Layer = 'Top';

% Axis labels:
yticks([0.003 0.016 0.03])
xlabel('$\mathcal{S}$','Interpreter','latex','FontSize',16)
fbox.TickLabelInterpreter = 'latex';
fbox.FontSize = 15; fbox.YAxis.Exponent = -2; 

% Colorbar:
cbar = colorbar; cbar.Location = 'southoutside'; cbar.FontSize = 14;
cbar.TickLabelInterpreter = 'Latex'; cbar.Label.Interpreter = 'Latex';
cbar.Label.String = 'VAL';
ctix = 1e-3 * [0, 3, 6]; cbar.Ticks = ctix; caxis([ctix(1), ctix(end)])

%% Plot VAL for worm swimmer
% Panel 2 (right) of VAL figure.

% Instantiate subplot and set axes:
subplot(1,2,2);

% Plot data:
[S_mesh, M0_mesh] = meshgrid( S, M0 );
pcol = surf( S_mesh, M0_mesh, val_w', 'EdgeColor', 'None' ); 
view(2); axis tight

% Add box:
fbox = gca; box on; grid off; fbox.LineWidth = 1.5; fbox.Layer = 'Top';

% Axis labels:
yticks([0.003 0.016 0.03])
xlabel('$\mathcal{S}$','Interpreter','latex','FontSize',16)
fbox.TickLabelInterpreter = 'latex';
fbox.FontSize = 15; fbox.YAxis.Exponent = -2; 

% Colorbar:
cbar = colorbar; cbar.Location = 'southoutside'; cbar.FontSize = 14;
cbar.TickLabelInterpreter = 'Latex'; cbar.Label.Interpreter = 'Latex';
cbar.Label.String = 'VAL'; cbar.Ruler.Exponent = -2;
ctix = 1e-2 * [0, 0.6, 1.2]; cbar.Ticks = ctix; caxis([ctix(1), ctix(end)])

% Resize figure 8:
set(gcf,'Position',[200 255 766 420],'PaperPositionMode','auto')

% Save figure 8:
save2pdf('figures/figure8.pdf')

%% Plot fast sperm swimmer
% Panel 1 (top) of fast swimmers figure.

% Instantiate figure and set axes:
figure; 
subplot(2,1,1); hold on; box on; 
fbox = gca; fbox.LineWidth = 1.5; fbox.FontSize = 15; fbox.Layer = 'Top';
fbox.TickLabelInterpreter = 'Latex';

% Select 'fast' sperm swimmer:
s_idx = 1;
s_idy = 1;
s_sol = data(s_idx, s_idy).sol_s;
s_tps = data(s_idx, s_idy).tps_s;

% Choose time points at which to plot:
t_plot = linspace( 2*pi, 4*pi, 10);
cols = parula( length(t_plot) );

% Find closest solution points and plot:
tol = 0.25e-1;
for n = 1:length(s_tps)
    [check, idx] = max( abs(s_tps(n) - t_plot) < tol );
    if check == 1 && n > 1
        plot_range(idx) = n; % tracks indices of plotted shapes for X0 path plot
        [x, y, ~] = GetFilamentCoordinates( s_sol(:,n), 1/Q );
        plot( x, y, 'LineWidth', 1.5, 'Color', cols(idx,:) );
        axis( [-0.0672 0.9082 -0.1482 0.1415] )
    end
end

% Calculate X0 path and plot overlay:
for n = 1:length(s_tps)
    [x, y, ~] = GetFilamentCoordinates( s_sol(:,n), 1/Q );
    x0(n) = x(1);
    y0(n) = y(1);
end
plot( x0(plot_range(1):plot_range(end)), y0(plot_range(1):plot_range(end)), 'm:', 'LineWidth', 2.5 );

% Configure labels and colorbar:
ylabel('$y$','Interpreter','latex','FontSize',16)
cbar = colorbar; cbar.Ticks = [2*pi, 4*pi]; cbar.TickLabels = {'$2\pi$','$4\pi$'};
cbar.Label.String = 'Time, $t$'; cbar.Label.Interpreter = 'latex';
cbar.TickLabelInterpreter = 'latex'; cbar.Limits = [t_plot(1), t_plot(end)];
cbar.FontSize = 14; 
caxis( [t_plot(1), t_plot(end)] )

% Annotation:
annotation(...
    'textbox',[.135,.545,.3,.3],...
    'interpreter','latex',...
    'string',sprintf('$\\mathcal{S}=%g$', data(s_idx, s_idy).S),...
    'fitboxtotext','on',...
    'edgecolor','none',...
    'backgroundcolor','white',...
    'fontsize',15 ...
    );

clearvars s_sol s_tps s_idx s_idy

%% Plot fast worm swimmer
% Panel 2 (bottom) of fast swimmers figure.

% Set axes:
subplot(2,1,2); hold on; box on; 
fbox = gca; fbox.LineWidth = 1.5; fbox.FontSize = 15; fbox.Layer = 'Top';
fbox.TickLabelInterpreter = 'Latex';

% Select 'fast' worm swimmer:
w_idx = 1;
w_idy = 1;
w_sol = data(w_idx, w_idy).sol_w;
w_tps = data(w_idx, w_idy).tps_w;

% Find closest solution points and plot:
tol = 0.25e-1;
for n = 1:length(w_tps)
    [check, idx] = max( abs(w_tps(n) - t_plot) < tol );
    if check == 1 && n > 1
        plot_range(idx) = n; % tracks indices of plotted shapes for X0 path plot
        [x, y, ~] = GetFilamentCoordinates( w_sol(:,n), 1/Q );
        plot( x, y, 'LineWidth', 1.5, 'Color', cols(idx,:) );
        axis( [-0.0672 0.9082 -0.1482 0.1415] )
    end
end

% Calculate X0 path and plot overlay:
for n = 1:length(w_tps)
    [x, y, ~] = GetFilamentCoordinates( w_sol(:,n), 1/Q );
    x0(n) = x(1);
    y0(n) = y(1);
end
plot( x0(plot_range(1):plot_range(end)), y0(plot_range(1):plot_range(end)), 'm:', 'LineWidth', 2.5 );

% Configure labels and colorbar:
xlabel('$x$','Interpreter','latex','FontSize',16)
ylabel('$y$','Interpreter','latex','FontSize',16)
cbar = colorbar; cbar.Ticks = [2*pi, 4*pi]; cbar.TickLabels = {'$2\pi$','$4\pi$'};
cbar.Label.String = 'Time, $t$'; cbar.Label.Interpreter = 'latex';
cbar.TickLabelInterpreter = 'latex'; cbar.Limits = [t_plot(1), t_plot(end)];
cbar.FontSize = 14; 
caxis( [t_plot(1), t_plot(end)] )

% Annotation:
annotation(...
    'textbox',[.135,.120,.3,.3],...
    'interpreter','latex',...
    'string',sprintf('$\\mathcal{S}=%g$', data(w_idx, w_idy).S),...
    'fitboxtotext','on',...
    'edgecolor','none',...
    'backgroundcolor','white',...
    'fontsize',15 ...
    );

set( gcf, 'PaperPositionMode', 'auto' ) 

clear w_sol w_tps w_idx w_idy

% Save figure 9a
save2pdf('figures/figure9a.pdf')

%% Plot bad sperm swimmer
% Panel 1 (top) of bad swimmers figure.

% Instantiate figure and set axes:
figure; 
subplot(2,1,1); hold on; box on; 
fbox = gca; fbox.LineWidth = 1.5; fbox.FontSize = 15; fbox.Layer = 'Top';
fbox.TickLabelInterpreter = 'Latex';

% Select 'bad' sperm swimmer:
s_idx = 1;
s_idy = 1;
s_sol = data(s_idx, s_idy).sol_s;
s_tps = data(s_idx, s_idy).tps_s;

% Choose time points at which to plot:
t_plot = linspace( 0, pi/4, 10);
cols = parula( length(t_plot) );

% Find closest solution points and plot:
tol = 0.25e-1;
for n = 1:length(s_tps)
    [check, idx] = max( abs(s_tps(n) - t_plot) < tol );
    if check == 1 && n > 1
        plot_range(idx) = n; % tracks indices of plotted shapes for X0 path plot
        [x, y, ~] = GetFilamentCoordinates( s_sol(:,n), 1/Q );
        plot( x, y, 'LineWidth', 1.5, 'Color', cols(idx,:) );
        axis( [-0.0672 0.9082 -0.1482 0.1415] )
    end
end

% Calculate X0 path and plot overlay:
for n = 1:length(s_tps)
    [x, y, ~] = GetFilamentCoordinates( s_sol(:,n), 1/Q );
    x0(n) = x(1);
    y0(n) = y(1);
end
plot( x0(plot_range(1):plot_range(end)), y0(plot_range(1):plot_range(end)), 'm:', 'LineWidth', 2.5 );

% Configure labels and colorbar:
ylabel('$y$','Interpreter','latex','FontSize',16)
cbar = colorbar; cbar.Ticks = [t_plot(1), t_plot(end)]; cbar.TickLabels = {'$0$','$\pi/4$'};
cbar.Label.String = 'Time, $t$'; cbar.Label.Interpreter = 'latex';
cbar.TickLabelInterpreter = 'latex'; cbar.Limits = [t_plot(1), t_plot(end)];
cbar.FontSize = 14; 
caxis( [t_plot(1), t_plot(end)] )

% Annotation:
annotation(...
    'textbox',[.135,.545,.3,.3],...
    'interpreter','latex',...
    'string',sprintf('$\\mathcal{S}=%g$', data(s_idx, s_idy).S),...
    'fitboxtotext','on',...
    'edgecolor','none',...
    'backgroundcolor','white',...
    'fontsize',15 ...
    );

clear s_sol s_tps s_idx s_idy

%% Plot bad worm swimmer
% Panel 2 (bottom) of bad swimmers figure.

% Set axes:
subplot(2,1,2); hold on; box on; 
fbox = gca; fbox.LineWidth = 1.5; fbox.FontSize = 15; fbox.Layer = 'Top';
fbox.TickLabelInterpreter = 'Latex';

% Select 'fast' worm swimmer:
w_idx = 1;
w_idy = 1;
w_sol = data(w_idx, w_idy).sol_w;
w_tps = data(w_idx, w_idy).tps_w;

% Choose time points at which to plot:
t_plot = linspace( 0, 5*pi/4, 10);
cols = parula( length(t_plot) );

% Find closest solution points and plot:
tol = 0.25e-1;
for n = 1:length(w_tps)
    [check, idx] = max( abs(w_tps(n) - t_plot) < tol );
    if check == 1 && n > 1
        plot_range(idx) = n; % tracks indices of plotted shapes for X0 path plot
        [x, y, ~] = GetFilamentCoordinates( w_sol(:,n), 1/Q );
        plot( x, y, 'LineWidth', 1.5, 'Color', cols(idx,:) );
        axis( [-0.0672 0.9082 -0.1482 0.1415] )
    end
end

% Calculate X0 path and plot overlay:
for n = 1:length(w_tps)
    [x, y, ~] = GetFilamentCoordinates( w_sol(:,n), 1/Q );
    x0(n) = x(1);
    y0(n) = y(1);
end
plot( x0(plot_range(1):plot_range(end)), y0(plot_range(1):plot_range(end)), 'm:', 'LineWidth', 2.5 );

% Configure labels and colorbar:
xlabel('$x$','Interpreter','latex','FontSize',16)
ylabel('$y$','Interpreter','latex','FontSize',16)
cbar = colorbar; cbar.Ticks = [t_plot(1), t_plot(end)]; cbar.TickLabels = {'$0$','$5\pi / 4$'};
cbar.Label.String = 'Time, $t$'; cbar.Label.Interpreter = 'latex';
cbar.TickLabelInterpreter = 'latex'; cbar.Limits = [t_plot(1), t_plot(end)];
cbar.FontSize = 14; 
caxis( [t_plot(1), t_plot(end)] )

% Annotation:
annotation(...
    'textbox',[.135,.120,.3,.3],...
    'interpreter','latex',...
    'string',sprintf('$\\mathcal{S}=%g$', data(w_idx, w_idy).S),...
    'fitboxtotext','on',...
    'edgecolor','none',...
    'backgroundcolor','white',...
    'fontsize',15 ...
    );

set( gcf, 'PaperPositionMode', 'auto' ) 

clear w_sol w_tps w_idx w_idy

% Save figure 9b
save2pdf('figures/figure9b.pdf')

end % function

