function [solutions,tps,coords] = FilamentsSwimmingFunction(model,Y0,tmax,type,varargin)

%% Filaments relaxing

% Code by A.L. Hall-McNair, University of Birmingham (2018).
% Nfil filaments relaxing in Stokes flow using the method of lines w/ the method of
% regularized stokeslets, accounting for elastodynamics of the filaments
% and non-local hydrodynamic interactions between filaments.

% Inputs
% - model:  model parameters from CreateModelStruct().
% - Y0:     initial condition.
% - tmax:   max time to simulate up to.
% - type:   string "worm" or "sperm" to decide moment wave-type to apply.
% - varargin: entry: 'nobar' will turn off the ODE progress bar.

% Outputs
% - solutions:  solution data at time points tps.
% - tps:        vector of time points at which solution is given.
% - coords:     coordinate data of filament positions.

%% Global parameter setup

close all; fprintf('\nSWIMMING FILAMENTS\n')

tmin = 0;
warning off     % suppress near-singular warnings when filament is nearly aligned.

global epsilon
Q       = model.Q;
Nfil    = model.Nfil;
epsilon = model.eps;

%% Solve

disp('Solve started...')

dY_RSM  = @(t,Y) RatesRSMSwimming(t,Y,model,type);

if ~isempty(varargin)
    bar_toggle = varargin{1};
    if strcmp(bar_toggle,'nobar') == 1
        options = odeset('Events',@CheckFilSelfIntersection);
    else
        options = odeset('Events',@CheckFilSelfIntersection,'OutputFcn',@odetpbar);
    end
else  
    options = odeset('Events',@CheckFilSelfIntersection,'OutputFcn',@odetpbar);
end

RSM_sol = ode15s(dY_RSM, [tmin tmax], Y0, options);

roundval    = round(RSM_sol.x(end),2);
if roundval > RSM_sol.x(end)
    roundval = round(RSM_sol.x(end)-0.05,2);
end
tps         = tmin:0.05*pi:roundval;
solutions   = deval(RSM_sol, tps);

disp('Solve complete!')

%% Get filament coordinates.

Ntps = length(tps);
for i = 1:Ntps
    soldata = solutions(:,i);
    soldata = reshape(soldata,[],Nfil)';
    for n = 1:Nfil
        fil(n).sol                          = soldata(n,:);
        [coords(n,i).x, coords(n,i).y,~]    = GetFilamentCoordinates(fil(n).sol,1/Q);
    end
end

fprintf('Complete!\n\n')
    
end % function

