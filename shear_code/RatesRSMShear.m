function [dY] = RatesRSMShear(t,Y,model)

% This code calculates the rate of movement of N filaments using
% the EHD model with forces decribed by the method of
% regularised stokeslets. Code by Smith (2018).

% Inputs:
% - t:      time point to evaluate rates at.
% - Y:      vector [x0,y0,theta] defining the geometry of the filament at time t.
% - model:  struct containing modelling parameters.

% Outputs:
% - dY:     rates vector of d/dt(x0,y0,theta).

%% Get filament data
%  Using input data, the joint coordinates and segment midpoints are
%  calculated.

Nfil            = model.Nfil;
V               = model.V;
[Q,ds,fil,all]  = GetFilamentData(Y,Nfil,V);
epsilon         = model.eps;

%% Hydrodynamics: the method of regularized stokeslets
% Compute using a semi-analytic method using the analytic solutions of the
% stokeslet integral along a straight line segment (Smith 2009).
   
R  = [ cos(all.th)  -sin(all.th)   zeros(1,Nfil*Q)   ;
       sin(all.th)   cos(all.th)   zeros(1,Nfil*Q)   ;
       zeros(1,Nfil*Q)  zeros(1,Nfil*Q)  ones(1,Nfil*Q)   ];

AH  = -RegStokesletAnalyticIntegrals(all.Xm,all.Xm,ds/2,R,epsilon);
D   = size(AH,2)/3;
AH  = AH(1:2*D,1:2*D);

%% Kinematics
%  Calculates the kinematic filament velocity from the X0,theta
%  configuration.

for n = 1:Nfil
    fil(n).K  = [ ones(Q,1)  zeros(Q,1) tril(repmat(-ds*sin(fil(n).th(:))',Q,1),-1)+diag(-ds/2*sin(fil(n).th(:))')    ;
                  zeros(Q,1) ones(Q,1)  tril(repmat( ds*cos(fil(n).th(:))',Q,1),-1)+diag( ds/2*cos(fil(n).th(:))')   ];
    fil(n).Kx = fil(n).K(1:Q,:);
    fil(n).Ky = fil(n).K(Q+1:2*Q,:);
end
AK = [blkdiag(fil(1:Nfil).Kx); blkdiag(fil(1:Nfil).Ky)];

%% Elastodynamics
%  Moment balance across each segment and the entire filament, as well as
%  total force balance.

for n = 1:Nfil
    M = -[ triu(repmat( ds*fil(n).y(1:Q)',1,Q)) + triu(repmat(-ds*fil(n).ym,Q,1))  ...
           triu(repmat(-ds*fil(n).x(1:Q)',1,Q)) + triu(repmat( ds*fil(n).xm,Q,1))  ;
           ds*ones(1,Q)                           zeros(1,Q)                       ;
           zeros(1,Q)                             ds*ones(1,Q)                ]    ;

    fil(n).Mx   = fil(n).param*M(:,1:Q);
    fil(n).My   = fil(n).param*M(:,Q+1:2*Q);
    
    clear M
end

AE = [blkdiag(fil.Mx), blkdiag(fil.My)];

%% Construct linear system
% Shear flow alters the RHS hydrodynamic velocity rows.

for n = 1:Nfil
    fil(n).velx = fil(n).ym';
    fil(n).curv = diff(fil(n).th)'/ds;
    fil(n).RHS  = [0; fil(n).curv; zeros(2,1)];
end

b   = [vertcat(fil(1:Nfil).RHS); vertcat(fil(1:Nfil).velx); zeros(Nfil*Q,1)];
A   = [zeros(Nfil*(Q+2)) AE; AK AH];
dZ  = A\b;
dY  = dZ(1:Nfil*(Q+2));

end % function
