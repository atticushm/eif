function [solutions,tps,coords] = FilamentsShearFunction(model, Y0, tmax)
%% Filaments in shear

% Code by A.L. Hall-McNair, University of Birmingham (2018).
% Nfil filaments in shear flow using the method of lines w/ the method of
% regularized stokeslets, accounting for elastodynamics of the filaments
% and non-local hydrodynamic interactions between filaments.

% Inputs
% - model:      Structure created by CreateModelStruct() containing problem data, including V, the viscous-elastic parameter.
% - Y0:         Initial condition.
% - tmax:       Time at which to stop the simulation.

% Outputs
% - solutions: 	Solution data at time points tps.
% - tps:        Vector of time points at which solution is given.
% - coords:     Coordinate data of filament positions.

%% Global parameter setup

disp('FILAMENTS IN SHEAR FLOW')

global epsilon
Q       = model.Q;
epsilon = model.eps;
Nfil    = model.Nfil;

%% Solve
%  An ode15s option "CheckFilAligned" is created, which aims to reduce
%  unnecessary solving by checking when the filament has re-aligned after
%  the buckling period.

fprintf('Solve started... \n')
options     = odeset('OutputFcn',@odetpbar);

dY_RSM      = @(t,Y) RatesRSMShear(t,Y,model);
RSM_sol     = ode15s(dY_RSM, [0 tmax], Y0, options);

tps_dt      = .1;
tps         = 0:tps_dt:round(RSM_sol.x(end),1);
solutions   = deval(RSM_sol, tps);

fprintf('\nSolve complete!\n')

%% Get filament coordinates.

Ntps = length(tps);
for i = 1:Ntps
    soldata = solutions(:,i);
    soldata = reshape(soldata,[],Nfil)';
    for n = 1:Nfil
        fil(n).sol                          = soldata(n,:);
        [coords(n,i).x, coords(n,i).y,~]    = GetFilamentCoordinates(fil(n).sol,1/Q);
    end
end

fprintf('Complete!\n\n')

end %function

