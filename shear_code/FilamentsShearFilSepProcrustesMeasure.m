function FilamentsShearFilSepProcrustesMeasure

% Code by A.L. Hall-McNair, University of Birmingham (2019).
% Analysises the effect of increasing the non-dimensional sperm number
% parameter in systems of multiple filaments, organised in a line, in
% linear shear flow, whilst also changing the degree of separation between
% them.

% Inputs
% - skip: boolean to decide whether solve step is skipped.

% Outputs
% - fig: figure.
% - savestr: string to be used with save2pdf(), to save figure.

close all

V       = [5000,10000,40000];
sep     = [0.5 1];
NV      = length(V);
Nsep    = length(sep);
th_0    = 0.9 * pi;
dth_0   = 0.1;
tmax    = 6;

%% Test for various eta.

if ~exist('./workspaces/figure12-data.mat','file')==1
    for j = 1:Nsep
        for v = 1:NV
            
            model               = CreateModelStruct(40,[1,2],[sep(j),0],'shear',V(v),th_0);
            Y0                  = GetPerturbedIntCondYoung(model,dth_0);
            [sol,tps,coords]    = FilamentsShearFunction(model, Y0, tmax);
            Ntps                = length(tps);
            
            fil(1).sol  = sol(1:42,:);
            fil(2).sol  = sol(43:end,:);
            for n = 1:Ntps  % calculate procrustes distance
                proc(j,v).t(n)    = procrustes( [coords(1,n).x;coords(1,n).y],[coords(2,n).x;coords(2,n).y] );
            end
        end
    end
    save('./workspaces/figure12-data.mat')
else
    load('./workspaces/figure12-data.mat')
end

%% Plot

rgb = get(gca,'colororder'); 
rgb = rgb(1:3,:);

hold on; box on;
for j = 1:Nsep
    for v = 1:NV 
        if j == 1
            l1 = plot(tps,proc(j,v).t,'-', 'Color',rgb(v,:),'LineWidth',1.5);
        elseif j == 2
            l2 = plot(tps,proc(j,v).t,'--','Color',rgb(v,:),'LineWidth',1.5);   
        end
    end
end

lgd1            = legend([l1 l2],{'$\Delta X_0$ = 0.5','$\Delta X_0$ = 1'},'Interpreter','latex');
lgd1.Location   = 'northoutside';
lgd1.NumColumns = 4;
lgd1.FontSize   = 16;
lgd1.EdgeColor  = 'none';

fbox            = gca;
fbox.LineWidth  = 1.5;
fbox.FontSize   = 12;
fbox.Layer      = 'Top';

colormap(rgb);
cbar                    = colorbar;
cbar.Location           = 'eastoutside';
cbar.Label.Interpreter  = 'latex';
cbar.Label.String       = '$\quad\mathcal{V}$';
cbar.Label.Rotation     = 0;
cbar.Ticks              = [.175 .5 .825];
cbar.TickLabels         = [{'$5\times 10^3$'},{'$1\times 10^4$'},{'$4\times 10^4$'}];
cbar.TickLabelInterpreter = 'latex';
cbar.FontSize           = 15;

xlabel('Time $t$','interpreter','latex','fontsize',16)
ylabel('Procrustes score','interpreter','latex','fontsize',16)

set(gcf,'Position',[500 300 793 350])


end % global function