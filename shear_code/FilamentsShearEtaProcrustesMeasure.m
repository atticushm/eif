function [fig,savestr] = FilamentsShearEtaProcrustesMeasure(skip)
% Analysises the effect of increasing the non-dimensional sperm number
% parameter in systems of multiple filaments, organised in a line, in
% linear shear flow.

close all

if nargin == 0
    skip = 0;
end

etavec  = [5000,7500,10000,15000,20000,25000,30000,40000,70000,100000];
Neta    = length(etavec);
Nfil    = 2;

%% Test for various eta.

if skip == 0
    for k = 1:Neta
        [sol,tps]       = FilamentsShearFunction(etavec(k),Nfil,40,0);
        Ntps            = length(tps);

        fil(1).sol  = sol(1:42,:);
        fil(2).sol  = sol(43:end,:);
        for n = 1:81  % calculate procrustes distance
            [x1,y1,~]       = GetFilamentCoordinates(fil(1).sol(:,n),1/40);
            [x2,y2,~]       = GetFilamentCoordinates(fil(2).sol(:,n),1/40);
            proc(k).t(n)   = procrustes([x1;y1],[x2;y2]);
       end
    end

    fprintf('Test runs complete!\n')
    save('C:/Users/attih/Documents/multiple-filaments/shear-scripts/workspaces/proc-data-results.mat')
end

%% Plot

clear all; close all; box on
load('C:/Users/attih/Documents/multiple-filaments/shear-scripts/workspaces/proc-data-results.mat')

cmap = parula(Neta+2);

hold on; box on;
for k = 1:Neta
    if max(etavec(k) == [25000,40000])~=1
        fig = plot(tps(1:81),proc(k).t(1:81),'color',cmap(k,:));
    end
end

xlabel('Time')
ylabel('Procrustes distance')
lgd = legend('$\eta=5000$','$\eta=7500$','$\eta=10000$','$\eta=15000$','$\eta=20000$','$\eta=30000$','$\eta=70000$','$\eta=100000$',...
        'Location','northeast', ...
        'Interpreter','Latex'   ...
        );
    
set(gcf,'Position',[500 300 730 515])
set(findall(gcf,'-property','FontSize'),'FontSize',16)
set(findall(gcf,'-property','LineWidth'),'LineWidth',2)

savestr = ['C:/Users/attih/Documents/multiple-filaments/images/shear-flow-2-filaments/'...
           'multi-eta-procrustes-test'];

end % global function