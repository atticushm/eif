function model = CreateModelStruct(Q,grid,sep,type,varargin)

% Creates the model structure for use in problem functions.

if exist('model','var')==1
    clearvars model
end

% Assign elements.
model.Q     = Q;
model.grid  = grid;
model.sep   = sep;
model.Nfil  = grid(1)*grid(2);
model.eps   = 0.01;

% Calculate filament initial points.
x0 = []; y0 = [];
for i = 1:model.grid(2)
    x0  = [x0, (i-1)*model.sep(1)];
end
for j = 1:model.grid(1)
    y0  = [y0, (j-1)*model.sep(2)];
end
X0          = combvec(x0,y0); clearvars x0 y0
model.x0    = X0(1,:);
model.y0    = X0(2,:);

% Assign problem-specific parameters.
switch type
    case 'shear'
        model.V     = varargin{1};
        model.th_0  = varargin{2};
    case 'sedimenting'
        model.G     = varargin{1};
    case 'swimming'
        order       = floor(log(abs(varargin{1}))./log(10));
        S           = varargin{1};
        m0          = varargin{3};
        if length(S) < model.Nfil
            S       = repmat(S,1,model.Nfil);
        end
        if length(m0) < model.Nfil
            m0      = repmat(m0,1,model.Nfil);
        end
        if order < 3
            model.S4    = S.^4;
        else
            model.S4    = S;
        end
        model.k     = varargin{2};
        model.m0    = m0;
    case 'relaxing'
        if length(varargin) == 2 
            model.K     = varargin{1};        
            model.int   = varargin{2};  % toggle to use int cond for the BLM convergence tests.
        elseif length(varargin) == 1
            model.K     = varargin{1};
        end
end

end % function
