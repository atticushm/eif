function [Y0, x, y, th] = GetParabolicIntCond(a,model,varargin)

% varargin contains a string identifying which construction method to use:
%   - scaled:       construct the initial condition by scaling a parabola.
%   - sample:       construct the initial condition by sampling a parabola.

if isempty(varargin)
    type    = 'scale';
else
    type    = varargin{1};
end

global parab_type
if exist(parab_type,'var')
    type    = parab_type;
end

Q   = model.Q;

switch type
    case 'scale'
    %% Scaling method
    % Construct initial condition by sampling from the curve y = ax^2 and then
    % scaling the sampled result.

    x       = linspace(-1,1,1e6);
    y       = a.*x.^2;

    ds      = sqrt((x(2:end) - x(1:end-1)).^2 + (y(2:end) - y(1:end-1)).^2);
    s       = [0,cumsum(ds)];
    delta   = max(s)/Q;
    ss      = 0:delta:max(s);

    yy      = spline(s,y,ss);
    xx      = spline(s,x,ss);
    x1      = xx/max(s); y1 = yy/max(s);

    dy      = diff(yy);
    dx      = diff(xx);
    th      = atan(dy./dx);

    x   = x1(:);
    y   = y1(:);
    th  = th(:);

    case 'sample'
    %% Reflection method
    % Construct initial condition by sampling symmetrically from the curve y = ax^2.
    
    if mod(Q,2) == 1
        Q_p     = Q+1;
    else
        Q_p     = Q;
    end

    % "continuous" parabolic curve (restricted to x>0):
    x_cont  = linspace(0,1,5e5);
    y_cont  = a.*x_cont.^2;

    ds_cont = sqrt((x_cont(2:end) - x_cont(1:end-1)).^2 + (y_cont(2:end) - y_cont(1:end-1)).^2);
    s_cont  = [0, cumsum(ds_cont)];
    L_cont  = max(s_cont);

    % spline continuous half-parabola to get Q/2 points:
    s_spline    = 0:(L_cont/(Q_p/2)):L_cont;
    x_spline    = spline(s_cont,x_cont,s_spline);
    y_spline    = spline(s_cont,y_cont,s_spline);

    % scale spline and re-spline to get half-flagellum:
    s_scaled    = s_spline/(2*L_cont);
    x_scaled    = spline(s_spline,x_spline,s_scaled);
    y_scaled    = spline(s_spline,y_spline,s_scaled);

    % reflect in y-axis to obtain symmetical parabola (removing duplicate nodes):
    x_parab             = unique([fliplr(-x_scaled),x_scaled]);
    y_parab             = [fliplr(y_scaled),y_scaled];
    y_parab(Q_p/2 + 1)  = [];

    ds_parab            = sqrt((x_parab(2:end) - x_parab(1:end-1)).^2 + (y_parab(2:end) - y_parab(1:end-1)).^2);
    s_parab             = [0, cumsum(ds_parab)];

    % spline full parabola to obtain initial condition:
    s_int       = 0:(1/Q):1;
    x           = spline(s_parab,x_parab,s_int);
    y           = spline(s_parab,y_parab,s_int);

    % calculate theta from int condition:
    th      = atan( diff(y)./diff(x) ); 

    % rearrange outputs:
    x   = x(:);
    y   = y(:);
    th  = th(:);
end

% Ensure initial conditions have the correct X0 initial location.
Nfil    = model.Nfil;
Y0      = zeros(Q+2,Nfil);
for n = 1:Nfil  
    Y                   = [model.x0(n); model.y0(n); th(:)];
    [x(:,n),y(:,n),th]  = GetFilamentCoordinates(Y,1/Q);
    Y0(:,n)             = [x(1,n); y(1,n); th(:)];
end

% Reshape to stack Y0 vectors:
Y0  = Y0(:);

end % function

