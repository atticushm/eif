function Y0 = ConstructInitialCondition(model,Y0)

%% Perform short-time solve to get low-resolution filament shape

Q       = model.Q; 
Q10     = 10;
ds      = 1/Q10;

global Nfil % needed as a global var to communicate with CheckFilCurvature
Nfil    = model.Nfil;
if Nfil > 1
    tmax    = 5e-4;
else
    tmax    = 2e-3;
end

%{
% global Nfil
Nfil = model.Nfil;
x0   = [];
y0   = [];

for i = 1:model.grid(2)
    x0  = [x0, (i-1)*model.sep(1)];
end
for j = 1:model.grid(1)
    y0  = [y0, (j-1)*model.sep(2)];
end
X0coords    = combvec(x0,y0); 
% if model.sep(2) > 1 || Nfil == 9 % Identifies F14b and F15.
%     [~,~,th]    = GetParabolicIntCond(1e-3,Q10);
if Nfil <= 2
    [~,~,th]    = GetParabolicIntCond(1e-7,Q10);  
else
    [~,~,th]    = GetParabolicIntCond(1e-3,Q10);
end
for n = 1:Nfil
    fil(n).x                = X0coords(1,n);
    fil(n).y                = X0coords(2,n);
    if IsField(model,'phi') == 1
        if size(model.phi) == Nfil
            fil(n).th = th + model.phi(n);
        else
            fil(n).th = th + model.phi;
        end
    else
        fil(n).th = th;
    end
    fil(n).Y0               = [fil(n).x(1); fil(n).y(1); fil(n).th(:)];
    [fil(n).x, fil(n).y,~]  = GetFilamentCoordinates(fil(n).Y0,ds);
    fil(n).comx             = mean(fil(n).x);
    fil(n).comy             = mean(fil(n).y);
end
Y0 = vertcat(fil.Y0);
%}

fprintf('Pre-solve for initial conition started: ')

global tol
if Nfil == 9
    tol = 0.5;
else
    tol = 0.5;
end

options = odeset('Events',@CheckFilCurvature,'OutputFcn',@odetpbar);
dY_RSM  = @(t,Y) RatesRSMSedimenting(t,Y,model);
sol     = ode15s(dY_RSM, [0 tmax], Y0, options);
fprintf('Pre-solve complete!\n')

soldata = reshape(sol.y(:,end),[],Nfil)';
for n = 1:Nfil
    fil(n).sol                          = soldata(n,:);
    [crds(n).x,crds(n).y,crds(n).th]    = GetFilamentCoordinates(fil(n).sol(:),1/Q10);
end

%% Calculate interpolating spline
%  We use intermediate value theorem to find p (smoothing value in csaps)
%  that produces an interpolant of unit length.

fprintf('Calculating spline(s)... ')
tol = 1e-7;     % tolerance at which to determine "unit" arclength.
for n = 1:Nfil
    xpts    = linspace(crds(n).x(1),crds(n).x(end),1e6);
    L       = 0;
    k       = 1;
    p(1,:)  = [0.8,1];
    
    while 1
        clear pp
        pp      = csaps(crds(n).x,crds(n).y,mean(p(k,:)));
        ypts    = fnval(pp,xpts);
        L(k)    = CheckArclength(xpts,ypts);
        if L(k) > 1+tol
            p(k+1,:) = [p(k,1),mean(p(k,:))]; 
        elseif L(k) < 1-tol
            p(k+1,:) = [mean(p(k,:)),p(k,2)];
        else
            break
        end
        k       = k+1; 
    end
    intrp(n).x  = xpts;
    intrp(n).y  = ypts;
end
fprintf('complete!\n')

%% Spline the interpolant to obtain the new initial condition

% clear fil
for n = 1:Nfil
    x       = intrp(n).x;
    y       = intrp(n).y;
    ds      = sqrt((x(2:end) - x(1:end-1)).^2 + (y(2:end) - y(1:end-1)).^2);
    s       = [0,cumsum(ds)];
    delta   = max(s)/Q;
    ss      = 0:delta:max(s);
    
    yy      = spline(s,y,ss);
    xx      = spline(s,x,ss);
    
    fil(n).xs   = xx(:)/max(s); 
    fil(n).ys   = yy(:)/max(s);

    dy          = diff(yy);
    dx          = diff(xx);
    fil(n).ths  = atan(dy./dx);
    fil(n).ths  = fil(n).ths(:);
    
    fil(n).Y0                           = [fil(n).xs(1);fil(n).ys(1);fil(n).ths];  
    [fil(n).x0, fil(n).y0, fil(n).th0]  = GetFilamentCoordinates(fil(n).Y0,1/Q);
    
    % GetFilamentCoordinates produces a curve arclength 1.
    % However, the splined curve may be <1. In this case, (x0,y0) curve will
    % be slightly misaligned with splined curve.
    % To combat this we tranlate nodes so that centre of mass of (x0,y0)
    % curve matches spline curve CoM.
    
    dX  = [mean(fil(n).xs)-mean(fil(n).x0); mean(fil(n).ys)-mean(fil(n).y0)];
    
    fil(n).x0 = fil(n).x0+dX(1);
    fil(n).y0 = fil(n).y0+dX(2);
end

Y0 = vertcat(fil.Y0);

%% Test plots

%{
figure; box on; hold on
for n = 1:Nfil
    plot(fil.x,fil.y,'m.-','Linewidth',1.5)
    plot(crds(n).x,crds(n).y,'r.-','Linewidth',1.5)
    plot(fil(n).x0,fil(n).y0,'b.-','Linewidth',1.5)
    %axis equal
end
xlabel('x','Interpreter','latex')
ylabel('y','Interpreter','latex')
title(sprintf('Pre-solved initial condition, t=%g',sol.x(end)))
set(gca,'TickLabelInterpreter','latex')
fil.th
drawnow
%}

end % function