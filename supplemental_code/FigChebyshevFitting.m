function [f,g] = FigChebyshevFitting(V,sol,tps,grid)
% Only set up for single filament problems, take care when using for
% multiple filament setups.

Nfil    = grid(1)*grid(2);
Ntps    = length(tps); 
tsave   = [1.1, 2.2, 3.3, 4.4];

for i = 1:Ntps
    [check,idx] = max(abs(tps(i)-tsave)<5e-5);
    [Q,~,fil,~] = GetFilamentData(sol(:,i),Nfil,V);
    if check == 1
        [cheb.func,avth] = GetChebInterpolant(fil,Q,5e-2);
        cheb.order(i)    = length(cheb.func)-1;

        annX = [.131, .337, .544, .750];
        annY = .618;
        if check == 1
            figure(2);
            f = subplot(2,4,idx); hold on; box on;
            fbox            = gca;
            fbox.LineWidth  = 1.5;
            fbox.FontSize   = 12;
            fbox.Layer      = 'Top';

            l1 = plot(fil.sm,avth,'LineWidth',1.4);
            l2 = plot(fil.sm,cheb.func(fil.sm),'d--','LineWidth',1.4);

            chebOrder   = cheb.order(i);
            annotation(...
                'textbox', [annX(idx),annY,0.3,0.3],...
                'interpreter','latex',...
                'string', sprintf('$ \\mathcal{N} = %g $',chebOrder),...
                'fitboxtotext','on',...
                'edgecolor','none',...
                'fontsize',14);

            xlabel('$\tilde{s}^{[m]}(t)$','Interpreter','latex','FontSize',15)
            ylabel('$\tilde{\theta}_{\textup{body}}(t)$','Interpreter','latex','FontSize',15)
            title(sprintf('$t=%g$',tsave(idx)),'Interpreter','latex')
            set(gca,'TickLabelInterpreter','latex')

            if V==5e3
                switch idx
                    case 1
                        set(gca,'YLim',[-6e-3 6e-3]);
                        yticks([-6e-3,0,6e-3])
                        ax = gca;
                        ax.YAxis.Exponent = -3;
                    case 2
                        set(gca,'YLim',[-0.3 0.3]);
                        yticks([-0.3 0 0.3])
                        ax = gca;
                        ax.YAxis.Exponent = -1;
                    case 3
                        set(gca,'YLim',[-0.4 0.4]);
                        yticks([-0.4 0 0.4])
                        ax = gca;
                        ax.YAxis.Exponent = -1;
                    case 4
                        set(gca,'YLim',[-0.02 0.02]);
                        yticks([-0.02 0 0.02])
                        ax = gca;
                        ax.YAxis.Exponent = -2;
                end
            elseif V==4e4
                switch idx
                    case 1
                        set(gca,'YLim',[-0.6 0.6]);
                        yticks([-0.6,0,0.6])
                        ax = gca;
                        ax.YAxis.Exponent = -1;
                    case 2
                        set(gca,'YLim',[-2 2]);
                        yticks([-2 0 2])
                    case 3
                        set(gca,'YLim',[-2 2]);
                        yticks([-2 0 2])
                    case 4
                        set(gca,'YLim',[-0.2 0.2]);
                        yticks([-0.2 0 0.2])
                        ax = gca;
                        ax.YAxis.Exponent = -1;
                end
            end
            if idx == 4
                dummy            = subplot(2,4,8);
                lgd1             = legend(dummy,[l1,l2],{'Filament shape','Chebyshev interpolant at 5\% error'});
                lgd1.Location    = 'southoutside';
                lgd1.Orientation = 'horizontal';
                lgd1.FontSize    = 16;
                lgd1.Interpreter = 'latex';
                lgd1.EdgeColor   = 'none';
                lgd1.Position    = [0.3962, 0.4385, 0.25, 0.0524];
                axis(dummy,'off')
            end

        end
    end
end
set(gcf,'Position',[500 500 1340 670])

g       = figure;
epsvec  = [0.05];
for i = 1:Ntps
    [Q,~,fil,~] = GetFilamentData(sol(:,i),Nfil,V);
    for k = 1:length(epsvec)
        [cheby(k,i).f,~]    = GetChebInterpolant(fil,Q,epsvec(k));                
        cheby(k,i).O        = length(cheby(k,i).f)-1;
        cheby(k,i).coeffs   = chebcoeffs(cheby(k,i).f);
        cheby(k,i).ncoeffs  = length(cheby(k,i).coeffs);
    end
end

for k = 1
    [minval(k),minidx(k)] = min(horzcat(cheby(k,:).ncoeffs));
    [maxval(k),maxidx(k)] = max(horzcat(cheby(k,:).ncoeffs));
    for i = 1:Ntps
        if cheby(k,i).ncoeffs < maxval(k)
            nzeros              = maxval(k)-cheby(k,i).ncoeffs; 
            cheby(k,i).coeffs   = [cheby(k,i).coeffs; zeros(nzeros,1)];
        end
    end
    coeffs(k).mat = horzcat(cheby(k,:).coeffs);
    if V == 5000
        type = 1;
    elseif V == 40000
        type = 2;
    end
    ConvergenceBoxPlot(tps,1:maxval(k),abs(coeffs(k).mat),'normal',type)
    figure(3);
    set(gcf,'Position',[255 540 420 285])
end

end % function
