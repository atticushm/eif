function [value,isterminal,direction] = CheckFilOverlap(t,Y)

global Nfil

val = zeros(2);
[~,~,fil,~] = GetFilamentData(Y,Nfil,[]);
for n = 1:Nfil
    for m = 1:Nfil
        [xint,~]    = intersections(fil(n).x,fil(n).y,fil(m).x,fil(m).y);
        val(n,m)    = length(xint);
    end
end

value       = (max(max(val)==0));
isterminal  = 1; % stop integrating
direction   = 0;

end % function