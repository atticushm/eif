function [value,isterminal,direction] = CheckFilSelfIntersection(t,Y)

[~,~,fil,~] = GetFilamentData(Y,1,[]);
[x0,~] = intersections(fil(1).x,fil(1).y);

value       = ~isempty(x0);
isterminal  = 1; % stop integrating
direction   = 0;

end % function