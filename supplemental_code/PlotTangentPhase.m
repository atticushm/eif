function [f1,f2] = PlotTangentPhase(sol,tps,Nfil,Q,eta)
% N is number of filaments.
% sol is solution data at time points tps.
% Q is number of segments per filament.

%% Extract filament curvature from solution data

Ntps = length(tps);
sol  = reshape(sol,Nfil*(Q+2),Ntps,[]);
for n = 1:Ntps
    for i = 1:Nfil
        sol_t                   = reshape(sol(:,n),Q+2,Nfil);
        fil(i).data(:,n)        = sol_t(:,i);
        fil(i).curv(:,n)        = diff(fil(i).data(3:end,n))/(1/Q);
        [~,~,fil(i).tang(:,n)]  = GetFilamentCoordinates(fil(i).data(:,n), 1/Q);
    end
end

%% Tangent angle phase diagram

f1 = figure;

for i = 1:Nfil
    
    subplot(1,Nfil,i);
    s       = (1:Q)/Q;
    [S,T]   = meshgrid(s,tps);
    tangle  = fil(i).tang';
    surf(T,S,tangle,'EdgeColor','None');
    xlabel('time');
    ylabel('arclength'); yticks([0 1])
    view(2); axis tight;
    colbar = colorbar('northoutside');
    colbar.Label.String = 'tangent angle';
    
    if length(eta) == 1
        annstr = sprintf('$\\eta = %g, Q = %g$',eta,Q);
        x = .14;
        y = .47;
        figtext = annotation('textbox',[x y .3 .3],   ...
            'Interpreter','latex',      ...
            'string',annstr,           ...
            'FitBoxToText','on',       ...
            'EdgeColor','none',        ...
            'Tag','figann');
    elseif length(eta) == 2
        annstr1 = sprintf('eta = %g\nQ = %g',eta(1),Q);
        annstr2 = sprintf('eta = %g\nQ = %g',eta(2),Q);
        x = [.14, .58];
        y = .47;
        figtext = annotation('textbox',[x(1) y .3 .3],   ...
            'Interpreter','latex',      ...
            'string',annstr1,           ...
            'FitBoxToText','on',        ...
            'EdgeColor','none',         ...
            'Tag','figann');
        figtext = annotation('textbox',[x(2) y .3 .3],   ...
            'Interpreter','latex',      ...
            'string',annstr2,           ...
            'FitBoxToText','on',        ...
            'EdgeColor','none',         ...
            'Tag','figann');
    end
    
    box on
    set(gca,'Layer','Top')
    figbox = gca;
    figbox.LineWidth = 1.5;
end
set(gcf,'Position',[100 100 Nfil*630 470])
set(findall(gcf,'-property','FontSize'),'FontSize',16)

%% Curvature angle phase diagram

f2 = figure;

for i = 1:Nfil
    
    subplot(1,Nfil,i);
    s       = (1:Q-1)/Q;
    [S,T]   = meshgrid(s,tps);
    curv    = fil(i).curv';
    surf(T,S,curv,'EdgeColor','None');
    xlabel('time');
    ylabel('arclength'); yticks([0 1]);
    view(2); axis tight;
    colbar = colorbar('northoutside');
    colbar.Label.String = 'curvature';
    
    if length(eta) == 1
        annstr = sprintf('$\\eta = %g,Q = %g$',eta,Q);
        x = .14;
        y = .5;
        figtext = annotation('textbox',[x y .3 .3],   ...
            'Interpreter','latex',      ...
            'string',annstr,           ...
            'FitBoxToText','on',       ...
            'EdgeColor','none',        ...
            'Tag','figann');
    elseif length(eta) == 2
        annstr1 = sprintf('$\\eta = %g \n Q = %g$',eta(1),Q);
        annstr2 = sprintf('$\\eta = %g \n Q = %g$',eta(2),Q);
        x = [.14, .58];
        y = .5;
        figtext = annotation('textbox',[x(1) y .3 .3],   ...
            'Interpreter','latex',      ...
            'string',annstr1,           ...
            'FitBoxToText','on',        ...
            'EdgeColor','none',         ...
            'Tag','figann');
        figtext = annotation('textbox',[x(2) y .3 .3],   ...
            'Interpreter','latex',      ...
            'string',annstr2,           ...
            'FitBoxToText','on',        ...
            'EdgeColor','none',         ...
            'Tag','figann');
    end
    
    box on
    set(gca,'Layer','Top')
    figbox = gca;
    figbox.LineWidth = 1.5;
end
set(gcf,'Position',[100 100 Nfil*630 470])
set(findall(gcf,'-property','FontSize'),'FontSize',16)

end % function
