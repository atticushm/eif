function FigFilamentCurvature(Q,V,sol,tps)

figure;
p = panel('no-manage-font'); p.pack(1);

[S,T,curv] = GetCurvPhaseData(sol,tps,1,Q);
surf(T,S,curv,'EdgeColor','None'); view(2); axis tight;

xlabel('Time $t$',      'interpreter','latex','fontsize',16);
ylabel('Arclength $s$', 'interpreter','latex','fontsize',16)
yticks([0 1])
yticklabels ({'0','1'});

set(gca,'TickLabelInterpreter','latex')

if V == 34000
    hold on
    plot3(1.6*ones(1,100),linspace(0.025,0.975),50*ones(1,100),'r--','LineWidth',1.5)
    plot3(2.7*ones(1,100),linspace(0.025,0.975),50*ones(1,100),'r--','LineWidth',1.5)
end

cbar                        = colorbar;
vals                        = [-40 0 40];
caxis([vals(1) vals(end)])
cbar.Location               = 'eastoutside';
cbar.Label.Interpreter      = 'latex';
cbar.Ticks                  = vals;
cbar.TickLabelInterpreter   = 'latex';
cbar.FontSize               = 14;

set(gca,'TickLabelInterpreter','latex')

box on;
fbox            = gca;
fbox.Layer      = 'top';
fbox.FontSize   = 15;
fbox.LineWidth  = 1.5;

set(gcf,'position',[600 600 440 330]);

end % function
