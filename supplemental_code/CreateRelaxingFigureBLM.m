function p = CreateRelaxingFigureBLM(Nfil,filGrid,Q,solutions,tps,drawStreams,sublabel)

% Code by A.L. Hall-McNair, University of Birmingham (2019).

% Inputs
% - Nfil: number of filaments in the system.
% - filGrid: dimensions of filament arrangement grid.
% - Q: number of segments in each filament.
% - solutions: solution data.
% - tps: time points at which solution data is given.
% - subLabel: figure sublabel (for latex).
% - drawStreams: boolean data to plot streamlines (optional).

if nargin == 5
    streams  = [];
    sublabel = '';
elseif nargin == 6
    sublabel = '';
elseif nargin == 7 && isempty(sublabel) == 1
    sublabel = '';
elseif nargin == 7
    sublabel = sublabel{1};
end


%% Main
% Extract field point velocities at tpoints=tps.

Ntps    = length(tps);
nsave   = 4;
tsave   = [0.0025,0.005,0.0075,0.01];

f = figure;
p   = panel(f); p.pack(1,nsave);
for i = 1:Ntps
    
    [check,idx] = max(abs(tps(i)-tsave)<1e-7);
    if check == 1
        
        p(1,idx).select(); hold on; box on;
        
        % Calculate filament coordinates.
        soldata = solutions(:,i);
        soldata = reshape(soldata,[],Nfil)';
        for n = 1:Nfil
            N                       = Q+1;
            fil(n).sol              = soldata(n,:);
            fil(n).x                = soldata(n,1:N);
            fil(n).y                = soldata(n,N+1:2*N);
            fil(n).comx             = mean(fil(n).x);
            fil(n).comy             = mean(fil(n).y);
        end
        
        % Dynamically scale figure axes, centered on the average centre of mass.
        com         = [mean(horzcat(fil.comx)); mean(horzcat(fil.comy))];
        xticks      = [-1 0 1];
        axtweak     = 0.7;
        axisSize    = [com(1)-axtweak*filGrid(1), com(1)+axtweak*filGrid(1),...
                       com(2)-axtweak*filGrid(2), com(2)+axtweak*filGrid(2)];
        axDx        = (axisSize(2)-axisSize(1))/2;
        axDy        = (axisSize(4)-axisSize(3))/2;
        
        % Square the axes (for non-sqiare filGrid configurations).
        axis equal
        if filGrid(1) > filGrid(2)
            axisSize = [com(1)-axDx,com(1)+axDx,com(2)-axDx,com(2)+axDx];
        elseif filGrid(1) < filGrid(2)
            axisSize = [com(1)-axDy,com(1)+axDy,com(2)-axDy,com(2)+axDy];
        end
        axis(axisSize)
        
        % Plot streamlines.
        if drawStreams == 1
            
            streamGrid = [filGrid(1)*10,filGrid(2)*10];
            
            streams = GetStreamlines(solutions(:,i),tps(i),Nfil,axisSize,'relaxing',0,filGrid,streamGrid);            
            x       = streams.xmesh;
            y       = streams.ymesh;
            D       = size(x);            
            u       = reshape(streams.vx,[D(1),D(2)]);
            v       = reshape(streams.vy,[D(1),D(2)]);
            
            slines = streamslice(x,y,u,v,0.9);
            set(slines,'color',[163,198,255]./255,'LineWidth',1.2)
        end
        
        % Plot filaments
        for n = 1:Nfil
            plot(fil(n).x,fil(n).y,'r','LineWidth',2.5)
        end
        
        ann(idx).str = sprintf('Q = %g, t = %g',Q,tps(i));
        
        set(gca,'ytick',[],'yticklabel',[]);
        set(gca,'xtick',[])
        set(gca,'Layer','Top')
        
        if idx == 1
            xlabel([sublabel,'i)'])
        elseif idx == 2
            xlabel([sublabel,'ii)'])
        elseif idx == 3
            xlabel([sublabel,'iii)'])
        else
            xlabel([sublabel,'iv)'])
        end
        figBox = gca;
        figBox.LineWidth = 1.5;
    end
end

set(gcf,'Position',[640 1400,1300 420])   % for 1,2 filament diagrams
set(gcf,'PaperPositionMode','auto')

p.margin = 2;
p(1,1).marginleft = 10;

%% Draw annotations.

x = [.01, .26, .51, .76];
y = .55;

figtext = annotation('textbox',[x(1) y .3 .3],   ...
                     'interpreter','latex',      ...
                     'string',ann(1).str,        ...
                     'FitBoxToText','on',        ...
                     'BackgroundColor','w',      ...
                     'EdgeColor','k');
figtext = annotation('textbox',[x(2) y .3 .3],   ...
                     'interpreter','latex',      ...
                     'string',ann(2).str,        ...
                     'FitBoxToText','on',        ...
                     'BackgroundColor','w',      ...
                     'EdgeColor','k');
figtext = annotation('textbox',[x(3) y .3 .3],   ...
                     'interpreter','latex',      ...
                     'string',ann(3).str,        ...
                     'FitBoxToText','on',        ...
                     'BackgroundColor','w',      ...
                     'EdgeColor','k');
figtext = annotation('textbox',[x(4) y .3 .3],   ...
                     'interpreter','latex',      ...
                     'string',ann(4).str,        ...
                     'FitBoxToText','on',        ...
                     'BackgroundColor','w',      ...
                     'EdgeColor','k');

set(findall(gcf,'-property','FontSize'),'FontSize',16)

end
