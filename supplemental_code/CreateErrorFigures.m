function CreateErrorFigures(Q_1,HR,source)

switch source
    case 'local'
        disp('Loading locally calculated solution data...')
        load('./workspaces/figure2-data.mat')
        if ~exist('./workspaces/figure2-hr-data.mat','file') == 1
            fprintf('Computing high-resolution BLM solution...')
            global parab_a
            model               = CreateModelStruct(HR,[1,1],[0,0],'relaxing',5e6);
            Y0                  = GetParabolicIntCond(parab_a,model);
            [~,~,blmHRcoords]   = FilamentsRelaxingFunctionBLM(model, Y0, 2e-2);
            fprintf(' complete!')
            save('./workspaces/figure2-hr-data.mat','blmHRcoords')
        else
            load('./workspaces/figure2-hr-data.mat','blmHRcoords');
        end
        disp('Loading complete!')
    case 'online'
        if isempty(dir('./workspaces/figure2-data*'))
            disp('Downloading online solution data...')
            url = 'http://edata.bham.ac.uk/409/8/figure2-data.zip';
            websave('./workspaces/figure2-data.zip',url);
            disp('Download complete!')
            unzip('./workspaces/figure2-data.zip','./workspaces')
        end
        fprintf('Loading downloaded data...')
        load('./workspaces/figure2-data.mat')
        load('./workspaces/figure2-hr-data.mat','blmHRcoords')

        fprintf(' complete!\n')
end

t           = tps(1).rsm;
Q           = Q_1;
Q_plot      = Q(1:end-1);
Ntps        = length(t);
NQ          = length(Q);

%% Calculate errors against HR bead model solution.

for n = 1:NQ  % skip Q=100 in error plots to avoid MGG issue.
    for m = 1:Ntps
        
        dsLR    = 1/Q(n);
        dsHR    = 1/HR;        
        sLR     = 0:dsLR:1; sLR = sLR(:);
        sHR     = 0:dsHR:1; sHR = sHR(:);
        
        % Interpolate low res method to match HR BLM data.
        
        Xblm    = [blmHRcoords(m).x(:); blmHRcoords(m).y(:)];
        
        Xrsm    = [interp1(sLR, coords(n).rsm(m).x(:), sHR, 'linear');...
                   interp1(sLR, coords(n).rsm(m).y(:), sHR, 'linear')];
        Xrft    = [interp1(sLR, coords(n).rft(m).x(:), sHR, 'linear');...
                   interp1(sLR, coords(n).rft(m).y(:), sHR, 'linear')];
        
        error(n).RSM(m) = MeanSqError(Xblm,Xrsm,1);
        error(n).RFT(m) = MeanSqError(Xblm,Xrft,1);
        if Q(n) == 100
            error(n).MGG(m) = 0;
        else
            Xmgg            = [interp1(sLR,  coords(n).hg(m).x(:), sHR, 'linear');...
                               interp1(sLR,  coords(n).hg(m).y(:), sHR, 'linear')];
            error(n).MGG(m) = MeanSqError(Xblm,Xmgg,1);
        end
        
        Xrsm    = reshape(Xrsm,[],2)';
        Xrft    = reshape(Xrft,[],2)';
        Xblm    = reshape(Xblm,[],2)';
        Xmgg    = reshape(Xmgg,[],2)';
        
        com(n).rsm(m)   = norm([mean(Xrsm(1,:)),mean(Xrsm(2,:))]);
        com(n).rft(m)   = norm([mean(Xrft(1,:)),mean(Xrft(2,:))]);
        com(n).blm(m)   = norm([mean(Xblm(1,:)),mean(Xblm(2,:))]);
        com(n).hg(m)    = norm([mean(Xmgg(1,:)), mean(Xmgg(2,:))]);        
    end
    
end

E.RSM = vertcat(error.RSM);
E.RFT = vertcat(error.RFT);
E.MGG = vertcat(error.MGG);

%% Filament shape EIF-RSM vs BLM

p = panel('no-manage-font'); p.pack('h',3);

p(1).select();
hold on; box on; axis tight; axis equal
fbox            = gca;
fbox.Layer      = 'Top';
fbox.LineWidth  = 1.5;
fbox.FontSize   = 16;
set(gca,'TickLabelInterpreter','latex')
set(gca,'FontSize',16)

f1=plot(coords(end).rsm(1).x, coords(end).rsm(1).y, 'LineStyle','-','Linewidth',1.5);
f2=plot(coords(end).blm(1).x, coords(end).blm(1).y, 'LineStyle','--','Linewidth',1.5);

plot(coords(end).rsm(end).x, coords(end).rsm(end).y,'color',f1.Color,'LineStyle','-','Linewidth',1.5)
plot(coords(end).blm(end).x, coords(end).blm(end).y,'color',f2.Color,'LineStyle','--','Linewidth',1.5)

xlabel('$x$','FontSize',15,'interpreter','latex')
ylabel('$y$','FontSize',15,'interpreter','latex')

lgd1            = legend([f1 f2],{'EIF-RSM','BLM'});
lgd1.Interpreter= 'latex';
lgd1.NumColumns = 2;
lgd1.FontSize   = 16;
lgd1.Location   = 'northoutside';
lgd1.Position   = [0.0907789910580919 0.8507 0.166461685473844 0.0925000000000002];
legend boxoff

axis([-0.1 1.1 -0.4 0.2])
% axis equal

% Add subfigure label (a)
annotation(...
    'textbox',[.03 .65 .3 .3],...
    'interpreter','latex',...
    'string','(a)',...
    'fitboxtotext','on',...
    'fontsize',18,...
    'edgecolor','none');

%% Point-wise error at tmax against high-res BLM solution.

% subplot(1,2,2);

p(2).select();
hold on; box on; axis tight
fbox            = gca;
fbox.Layer      = 'Top';
fbox.LineWidth  = 1.5;
fbox.FontSize   = 15;
set(gca,'TickLabelInterpreter','latex')

f1  = plot(Q,E.RSM(:,Ntps),'LineWidth',2);
f2  = plot(Q,E.RFT(:,Ntps),'LineWidth',2);
f3  = plot(Q_plot,E.MGG(1:end-1,Ntps),'LineWidth',2);

set(gca,'YScale','log','XScale','log')

xlabel('$Q$','Interpreter','latex')
ylabel('$\textup{RMSE}$','Interpreter','latex')

xlim([0,10^2])
xticks([10^1, 10^2])

yticks(1e-3.*[0 3 6])
ylim('auto')

lgd2            = legend([f1 f2 f3],{'EIF-RSM','EIF-RFT','MGG'});
lgd2.Interpreter= 'latex';
lgd2.Orientation= 'horizontal';
lgd2.FontSize   = 16;
lgd2.Location   = 'northoutside';
lgd2.Position   = [0.369318486079452 0.860713376526027 0.266415585942709 0.084571427481515];
legend boxoff


% Add subfigure label (b)
annotation(...
    'textbox',[.34 .65 .3 .3],...
    'interpreter','latex',...
    'string','(b)',...
    'fitboxtotext','on',...
    'fontsize',18,...
    'edgecolor','none');


%% Solve-time comparison (without RFT) (loglog)

p(3).select();
hold on; box on; axis tight
fbox            = gca;
fbox.Layer      = 'Top';
fbox.LineWidth  = 1.5;
fbox.FontSize   = 15;
set(gca,'TickLabelInterpreter','latex')

plot(Q, times.rsm(1:end),'LineWidth',1.5)
plot(Q, times.blm(1:end),'LineWidth',1.5)
plot(Q, times.hg(1:end), 'LineWidth',1.5)

set(gca,'YScale','log','XScale','log')

xlabel('$Q$','FontSize',16,'interpreter','latex')
ylabel('$T$','Fontsize',16,'interpreter','latex')

xlim([0 10^2])
ylim([0 10^2])

lgd4            = legend('EIF-RSM','BLM','MGG');
lgd4.Interpreter= 'latex';
lgd4.NumColumns = 4;
lgd4.FontSize   = 16;
lgd4.Location   = 'northoutside';
lgd4.Position   = [0.69653470033473 0.860713378569614 0.23850954669896 0.084571427481515];
legend boxoff

% Add subfigure label (c)
annotation(...
    'textbox',[.661 .65 .3 .3],...
    'interpreter','latex',...
    'string','(c)',...
    'fitboxtotext','on',...
    'fontsize',18,...
    'edgecolor','none');

p.margin            = 20;
set(gcf,'Position',[200 300 1444 350])

end % function