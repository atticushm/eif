function [value,isterminal,direction] = CheckFilCurvature(t,Y)

global Nfil tol

[~,ds,fil,~] = GetFilamentData(Y,Nfil,[]);
for n = 1:Nfil
    fil(n).curv     = diff(fil(n).th)/ds;
    fil(n).dcurv    = diff(fil(n).curv);  
    mdc(n)          = max(abs(fil(n).curv));
end

value       = any(mdc<tol);
isterminal  = 1; % stop integrating
direction   = 0;

end % function