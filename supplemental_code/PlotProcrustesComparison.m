function [fig, titlestr] = PlotProcrustesComparison(Nfil,Q,sol,tps)
% Creates and draws figure comparing NFil filaments using the procrustes
% measure of each filament.

%% Extract filament curvature from solution data

Ntps = length(tps);
sol  = reshape(sol,Nfil*(Q+2),Ntps,[]);
for n = 1:Ntps
    for i = 1:Nfil
        sol_t                               = reshape(sol(:,n),Q+2,Nfil);
        fil(i).data(:,n)                    = sol_t(:,i);
        [fil(i).x,fil(i).y,fil(i).th(:,n)]  = GetFilamentCoordinates(fil(i).data(:,n), 1/Q);
        fil(i).xy                           = [fil(i).x;fil(i).y];
    end
    for i = 1:Nfil
        for j = 1:Nfil
            proc(n).val(i,j) = procrustes(fil(i).xy, fil(j).xy);
        end
    end
end

%% Procrustes measure plots

global phi eta
titlestr = sprintf('Q=%g, phi=%g, eta=%g',Q,phi,eta);

fig = figure; hold on; box on;
for i = 1:Nfil
    for j = i+1:Nfil
        x = [];
        for n = 1:Ntps
            v = proc(n).val;
            x = [x, v(i,j)]; 
        end
        label = sprintf('fil %g v. fil %g',i,j);
        plot(tps,x,'DisplayName',[label])
    end
end
xlabel('time')
ylabel('procrustes distance')
legend(gca,'show')

end % function