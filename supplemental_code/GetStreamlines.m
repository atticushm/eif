function [sdata] = GetStreamlines(sol,tps,Nfil,axisSize,problem,param,filGrid,streamGrid,drawPcolor)

% Code by A.L. Hall-McNair, University of Birmingham (2019).

% Inputs
% - sol: solution data at tps time points.
% - tps: time points at which solution data is given.
% - Nfil: number of filaments in the system.
% - axisSize: defines size of axis which streamlines need to cover.
% - problem: string input defining which problem is being solved (sedimenting, shear, or relaxing).
% - filGrid: dimensions of filament arrangement grid.
% - streamGrid: vector defining how many streamline origin points should be in each x,y direction, per filament.
% - drawPcolor: boolean to decide whether to draw flow pcolor (flow velocity magnitude).

% Outputs
% - sdata: streamline data, to be used with streamlines() function.

epsilon = 0.01;
Ntps    = length(tps);
for n = 1:Ntps
    t            = tps(n);
    slines       = GetFieldPointVelocities(t,sol(:,n),Nfil,epsilon,axisSize,problem,param,filGrid,streamGrid,drawPcolor);
    sdata(n)     = slines;
end

end % function

function [slines] = GetFieldPointVelocities(t,Y,Nfil,epsilon,axisSize,problem,param,filGrid,streamGrid,drawPcolor)

% Code by A.L. Hall-McNair, Univeristy of Birmingham (2019).

% Inputs
% - t: time point at which to calcualte velocities.
% - Y: input filament data.
% - Nfil: number of filaments in the system.
% - axisSize: defines size of axis which streamlines need to cover.
% - problem: string input defining which problem is being solved (sedimenting, shear, or relaxing).
% - param: characteristic parameter of the system: either calG (sedimenting problem) or eta (shear problem).
% - filGrid: dimensions of filament arrangement grid.
% - streamGrid: vector defining how many streamline origin points should be in each x,y direction, per filament.
% - drawPcolor: boolean to decide whether to draw flow pcolor (flow velocity magnitude).

% Outputs
% - slines: struct containing everything needed to plot streamlines, using streamlines().

if nargin == 8
    drawPcolor = 0;
end

%% Check dimensions of Y and transpose accordingly
%  ode15s takes 1xQ input Y and transposes to Qx1
%  my manual solver does not do this, so we need to check and transpose

if size(Y,1)==1
    Y = Y';
end

%% Get filament data
%  Using input data, the joint coordinates and segment midpoints are
%  calculated.

[Q,ds,fil,all] = GetFilamentData(Y,Nfil,param);

%% Hydrodynamics: the method of regularized stokeslets
% Compute using a semi-analytic method using the analytic solutions of the
% stokeslet integral along a straight line segment (Smith 2009).

R  = [ cos(all.th)     -sin(all.th)      zeros(1,Nfil*Q)   ;
       sin(all.th)      cos(all.th)      zeros(1,Nfil*Q)   ;
       zeros(1,Nfil*Q)  zeros(1,Nfil*Q)  ones(1,Nfil*Q)   ];

AH  = -RegStokesletAnalyticIntegrals(all.Xm,all.Xm,ds/2,R,epsilon);
D   = size(AH,2)/3;
AH  = AH(1:2*D,1:2*D);

%% Kinematics
%  Calculates the kinematic filament velocity from the X0,theta
%  configuration.

for n = 1:Nfil
    fil(n).K  = [ ones(Q,1)  zeros(Q,1) tril(repmat(-ds*sin(fil(n).th(:))',Q,1),-1)+diag(-ds/2*sin(fil(n).th(:))')    ;
        zeros(Q,1) ones(Q,1)  tril(repmat( ds*cos(fil(n).th(:))',Q,1),-1)+diag( ds/2*cos(fil(n).th(:))')   ];
    fil(n).Kx = fil(n).K(1:Q,:);
    fil(n).Ky = fil(n).K(Q+1:2*Q,:);
end
AK = [blkdiag(fil(1:Nfil).Kx); blkdiag(fil(1:Nfil).Ky)];

%% Construct correct linear system
%  Elastics block and RHS vector will change depending on the problem.
%  For more detail on each method please check the respective "RatesRSM---"
%  code in /multiple-filaments/supplemental-code.

if strcmp(problem,'sedimenting') == 1 % sedimenting filament(s)
    for n = 1:Nfil
        for i = 1:Q-1
            for j = i:Q-1
                fil(n).M(i,j)       = ds*(fil(n).ym(j+1)-fil(n).y(i+1));
                fil(n).M(i,j+Q)     = ds*(fil(n).x(i+1)-fil(n).xm(j+1));
            end
        end
        fil(n).M    = [zeros(Q-1,1),fil(n).M];
        fil(n).M    = [fil(n).ym, -fil(n).xm; fil(n).M];
        fil(n).M    = [fil(n).M; ds*ones(1,Q), zeros(1,Q); zeros(1,Q), ds*ones(1,Q)];
        fil(n).Mx   = fil(n).param*fil(n).M(:,1:Q);
        fil(n).My   = fil(n).param*fil(n).M(:,Q+1:2*Q);
    end
    AE = [blkdiag(fil.Mx), blkdiag(fil.My)];
    
    fgx     = zeros(Nfil*Q,1);
    fgy     = -ones(Nfil*Q,1);
    fg      = [fgx; fgy];
    vgrav   = -AH*fg;
    
    for n = 1:Nfil
        fil(n).RHS = [0; diff(fil(n).th)'/ds; 0; 0];
    end
    b   = [vertcat(fil(1:Nfil).RHS); vgrav];
    A   = [zeros(Nfil*(Q+2)) AE; AK AH];
    dZ  = A\b;
    
elseif strcmp(problem,'relaxing') == 1  % relaxing filament(s)
    for n = 1:Nfil
        for i = 1:Q-1
            for j = i:Q-1
                fil(n).M(i,j)       = ds*(fil(n).ym(j+1)-fil(n).y(i+1));
                fil(n).M(i,j+Q)     = ds*(fil(n).x(i+1)-fil(n).xm(j+1));
            end
        end
        fil(n).M    = [zeros(Q-1,1),fil(n).M];
        fil(n).M    = [fil(n).ym, -fil(n).xm; fil(n).M];
        fil(n).M    = [fil(n).M; ds*ones(1,Q), zeros(1,Q); zeros(1,Q), ds*ones(1,Q)];
        fil(n).Mx   = fil(n).M(:,1:Q);
        fil(n).My   = fil(n).M(:,Q+1:2*Q);
    end
    AE = [blkdiag(fil.Mx), blkdiag(fil.My)];
    
    for n = 1:Nfil
        fil(n).RHS = [0; diff(fil(n).th)'/ds; zeros(2,1)];
    end
    b   = [vertcat(fil(1:Nfil).RHS); zeros(Nfil*2*Q,1)];
    A   = [zeros(Nfil*(Q+2)) AE; AK AH];
    dZ  = A\b;
    
elseif strcmp(problem,'shear') == 1 % filament(s) in shear flow.
    for n = 1:Nfil
        for i = 1:Q-1
            for j = i:Q-1
                fil(n).M(i,j)       = ds*(fil(n).ym(j+1)-fil(n).y(i+1));
                fil(n).M(i,j+Q)     = ds*(fil(n).x(i+1)-fil(n).xm(j+1));
            end
        end
        fil(n).M    = [zeros(Q-1,1),fil(n).M];
        fil(n).M    = [fil(n).ym, -fil(n).xm; fil(n).M];
        fil(n).M    = [fil(n).M; ds*ones(1,Q), zeros(1,Q); zeros(1,Q), ds*ones(1,Q)];
        fil(n).Mx   = fil(n).param*fil(n).M(:,1:Q);
        fil(n).My   = fil(n).param*fil(n).M(:,Q+1:2*Q);
    end
    AE = [blkdiag(fil.Mx), blkdiag(fil.My)];
    
    for n = 1:Nfil
        fil(n).velx = fil(n).ym';
        fil(n).curv = diff(fil(n).th)'/ds;
        fil(n).RHS  = [0; fil(n).curv; zeros(2,1)];
    end
    
    b   = [vertcat(fil(1:Nfil).RHS); vertcat(fil(1:Nfil).velx); zeros(Nfil*Q,1)];
    A   = [zeros(Nfil*(Q+2)) AE; AK AH];
    dZ  = A\b;
    
elseif strcmp(problem,'swimming') == 1 % filament(s) swimming in stationary fluid.
    
    Sp4     = param(1);
    m0      = param(2);
    wavk    = param(3);
    
    for n = 1:Nfil
        M = -[ triu(repmat( ds*fil(1).y(1:Q)',1,Q)) + triu(repmat(-ds*fil(1).ym,Q,1))  ...
            triu(repmat(-ds*fil(1).x(1:Q)',1,Q)) + triu(repmat( ds*fil(1).xm,Q,1))  ;
            ds*ones(1,Q)                           zeros(1,Q)                       ;
            zeros(1,Q)                             ds*ones(1,Q)                ]    ;
        
        fil(n).Mx   = M(:,1:Q);
        fil(n).My   = M(:,Q+1:2*Q);
        
        clear M
    end 
    AE = [blkdiag(fil.Mx), blkdiag(fil.My)];
    A   = [zeros(Nfil*(Q+2)) AE; AK AH];
    for n = 1:Nfil
        fil(n).act  = -(m0/wavk)*( sin(wavk- t)*ones(Q,1) - sin(wavk*fil(n).s(1:end-1)' -t));
        fil(n).act  = fil(n).act(:);
        fil(n).mom  = [0; diff(fil(n).th(:))/ds] + fil(n).act;
        fil(n).RHS  = [fil(n).mom; zeros(2,1)];
    end
    b   = [vertcat(fil(1:Nfil).RHS); zeros(Nfil*2*Q,1)];
    dZ  = A\b;
    
end

%% Calculate velocities at field points
%  z is the centre of mass of filaments, around which to decide streamline origin points.
%  Nfp number of field points in each dimension (x,y).
%  xf are the field point coordinates, from which streamlines are calculated.

z           = [mean(horzcat(fil.comx)); mean(horzcat(fil.comy))];
Nfp         = [streamGrid(1)*filGrid(1),streamGrid(2)*filGrid(2)];

slines.x    = linspace(axisSize(1),axisSize(2),streamGrid(1));
slines.y    = linspace(axisSize(3),axisSize(4),streamGrid(2));

[slines.xmesh,slines.ymesh] = meshgrid(slines.x,slines.y);
slines.coords               = [slines.xmesh(:),slines.ymesh(:),zeros(size(slines.xmesh(:),1),1)]';
slines.nlines               = size(slines.coords,2);

forces  = dZ(Nfil*(Q+2)+1:end);
F       = forces(1:Nfil*Q);
G       = forces(Nfil*Q+1:end);

Nxf     = size(slines.coords,2);
xf      = [slines.coords(1,:),slines.coords(2,:),slines.coords(3,:)]';

I       = RegStokesletAnalyticIntegrals(xf,all.Xm,ds/2,R,epsilon);
D1      = size(I,1)/3;
D2      = size(I,2)/3;
I       = I(1:2*D1,1:2*D2);

dxf     = I*forces;

slines.vels = dxf;
slines.vx   = dxf(1:slines.nlines);
slines.vy   = dxf(slines.nlines+1:2*slines.nlines);

% Account for surrounding flow in shear and sedimenting problems.
% In flow pcolor plots, we want to remove this `background' flow.

if strcmp(problem,'shear') == 1 && Nfil == 1
    slines.bg.vy    = slines.vy;
    slines.bg.vx    = slines.vx + slines.coords(2,:)';
    slines.vx       = slines.vx + slines.coords(2,:)';
end

end % function

