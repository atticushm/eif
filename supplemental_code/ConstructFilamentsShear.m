function [fil, Y0] = ConstructFilamentsShear(N,M,Q,sep)

global phi

fil_sep = sep;
ds      = 1/Q;

% Create NxM grid of points at which to place filament starting points X0.
domx     = [-N*0.4, N*0.4]; 
domy     = [-M*0.2, M*0.2]; 
dim      = [N,M];
if dim(1) == 1
    x    = (domx(1)+domx(2))/2;
    y    = (domy(1)+domy(2))/2;
else
    x    = linspace(domx(1),domx(2),dim(1)); 
    y    = linspace(domy(1),domy(2),dim(2));
end

% int cond ala Young (2009)
delta    = 1e-1;
[~,~,th] = GetPerturbedIntCond1(Q,delta);
th       = th + phi;

% int cond ala Tornberg & Shelley (2004)
% theta0   = pi - asin((49.664^2+1)^(-.5));
% deltay   = -1e-4;
% [~,~,th] = GetPerturbedIntCond2(Q,theta0,deltay);

X0coords = combvec(x,y);        % coordinates of X0s.
Nfil     = size(X0coords,2);    % number of filaments.

% Construct filament data.
for n = 1:Nfil
    fil(n).x                = X0coords(1,n);
    fil(n).y                = X0coords(2,n);
    fil(n).th               = th;
    fil(n).Y0               = [fil(n).x(1); fil(n).y(1); fil(n).th]; 
    [fil(n).x, fil(n).y,~]  = GetFilamentCoordinates(fil(n).Y0,ds);
      
    fil(n).X0               = [fil(n).x(1);fil(n).y(1)];
    fil(n).comx             = mean(fil(n).x);
    fil(n).comy             = mean(fil(n).y);
    
    % transform filament so centre is at midpoint
    fil(n).xt               = fil(n).x - fil(n).comx + fil_sep*(n-1);
    fil(n).yt               = fil(n).y - fil(n).comy;
    fil(n).Y0               = [fil(n).xt(1); fil(n).yt(1); fil(n).th]; 
    [fil(n).x, fil(n).y,~]  = GetFilamentCoordinates(fil(n).Y0,ds);
    
    fil(n).X0               = [fil(n).x(1);fil(n).y(1)];
    fil(n).comx             = mean(fil(n).x);
    fil(n).comy             = mean(fil(n).y);
end
Y0 = vertcat(fil.Y0);

end
