function p = FigShapeWithStreamlines(param,sol,tps,grid,problem,drawPcolor)

Nfil    = grid(1)*grid(2);
Ntps    = length(tps);
tmax    = tps(end);
switch problem
    case 'shear'
        tsave   = [1.1, 2.2, 3.3, 4.4];
        figure;
    case 'sedimenting'
        if Nfil == 1
            tsave   = [0 3e-3 6e-3 9e-3];
        else
            tsave   = [0 1e-3 2e-3 3e-3];
        end
        figure;
    case 'swimming'
        tsave   = [10 15 20 25];
        figure;
    otherwise
        tsave   = [tmax/4, tmax/2, (3*tmax)/4, tmax];
        warning('Prescribing manual tsave (plotting time-points) advised!')
end
figData = gcf;
p = panel(figData.Number,'no-manage-font');
p.pack('h',4)
p.margin = 3;
for i = 1:Ntps
    switch problem
        case 'swimming' % requires larger pick tolerance due to larger dt
            [check,idx]    = max(abs(tps(i)-tsave)<1e-1);
        case 'sedimenting'
            if Nfil == 1
                [check,idx] = max(abs(tps(i)-tsave)<5e-5);
            elseif Nfil >= 2
                [check,idx] = max(abs(tps(i)-tsave)<1e-5);
            end
        case 'shear'
            if param == 2e5 && Nfil == 1
                [check,idx] = max(abs(tps(i)-tsave)<7e-2);
            else
                if Nfil == 1
                    [check,idx] = max(abs(tps(i)-tsave)<5e-5);
                else
                    [check,idx] = max(abs(tps(i)-tsave)<4e-5);
                end
            end
        otherwise
            if Nfil == 1
                [check,idx] = max(abs(tps(i)-tsave)<5e-5);
            else
                [check,idx] = max(abs(tps(i)-tsave)<4e-5);
            end
    end
    [Q,~,fil,~]     = GetFilamentData(sol(:,i),Nfil,param);
    if check == 1
        
        p(idx).select();
        hold on; box on;
        fbox            = gca;
        fbox.LineWidth  = 1.5;
        fbox.FontSize   = 12;
        fbox.Layer      = 'Top';
        switch problem
            case 'relaxing'
                axZoom = 0.7;
                set(gca,'ytick',[],'yticklabel',[]);
                set(gca,'xtick',[])
                streamGrid  = [grid(1)*200,grid(2)*200];
            case 'shear'
                switch drawPcolor
                    case 1
                        if Nfil == 1
                            axZoom = 0.8;
                        elseif Nfil == 2
                            axZoom = 0.55;
                        end
                    otherwise
                        axZoom = 0.8;
                end
                set(gca,'ytick',[],'yticklabel',[]);
                set(gca,'xtick',[])
                streamGrid  = [grid(1)*200,grid(2)*200];
                
                annotationx = [.015, 0.265, 0.51, 0.757];
                if drawPcolor == 1
                    annotationy = .51;
                else
                    annotationy = .59;
                end
                if param == 2e5
                    annotation(...
                        'textbox',[annotationx(idx),annotationy,.3,.3],...
                        'interpreter','latex',...
                        'string',sprintf('$Q=%g,t=%g$',Q,tps(i)),...
                        'fitboxtotext','on',...
                        'edgecolor','none',...
                        'backgroundcolor','white',...
                        'fontsize',16 ...
                        );
                else
                    annotation(...
                        'textbox',[annotationx(idx),annotationy,.3,.3],...
                        'interpreter','latex',...
                        'string',sprintf('$\\mathcal{V} = %g,\\,t=%g$',param,tps(i)),...
                        'fitboxtotext','on',...
                        'edgecolor','none',...
                        'backgroundcolor','white',...
                        'fontsize',16 ...
                        );
                end
                
            case 'sedimenting'
                if Nfil == 1
                    axZoom = 0.6;
                elseif Nfil == 2
                    axZoom = 0.8;
                elseif Nfil == 4
                    axZoom = 0.8;
                elseif Nfil == 9
                    axZoom = 0.8;
                end
                set(gca,'ytick',[],'yticklabel',[]);
                set(gca,'xtick',[])
                streamGrid  = [grid(1)*20,grid(2)*20];
                annotationx = [.015, 0.265, 0.51, 0.757];
                if (param == 3000 && Nfil == 9) || (param == 3500 && Nfil == 9)
                    annotationy = 0;
                else
                    annotationy = .59;
                end
                annotation(...
                    'textbox',[annotationx(idx),annotationy,.3,.3],...
                    'interpreter','latex',...
                    'string',sprintf('$\\mathcal{G} = %g,\\,t=%g$',param,tsave(idx)),...
                    'fitboxtotext','on',...
                    'edgecolor','none',...
                    'backgroundcolor','white',...
                    'fontsize',16 ...
                    );
            case 'swimming'
                axZoom = 0.7;
                xlabel('$x$','Interpreter','latex','FontSize',16)
                set(gca,'ytick',[],'yticklabel',[]);
                streamGrid  = [grid(1)*200,grid(2)*200];
            otherwise
                warning('Unrecognised problem specified!')
        end
        axSize      = GetAxes(fil,grid,axZoom);
        axis(axSize)
        streams     = GetStreamlines(sol(:,i),tps(i),Nfil,axSize,problem,param,grid,streamGrid,0);
        x           = streams.xmesh;
        y           = streams.ymesh;
        D           = size(x);
        u           = reshape(streams.vx,[D(1),D(2)]);
        v           = reshape(streams.vy,[D(1),D(2)]);
        switch drawPcolor
            case 1
                cbar                    = colorbar;
                cbar.Location           = 'northoutside';
                cbar.FontSize           = 12;
                switch problem
                    case 'shear'
                        uvec    = u(:);
                        vvec    = v(:);
                        Uref    = norm([uvec(1);vvec(1)]);
                        Uflow   = ((u.^2 + v.^2).^(0.5))/Uref;
                        caxis('auto')
                        if idx == 1
                            cbar.Ticks = [20,60,100];
                        elseif idx == 2
                            cbar.Ticks = [50,100,150];
                        elseif idx == 3
                            %                             cbar.Ticks = [20,60,100,140];
                        elseif idx == 4
                            cbar.Ticks = [20,40];
                        end
                    case 'relaxing'
                        Uflow   = ((u.^2 + v.^2).^(0.5));
                    case 'swimming'
                        Uflow   = ((u.^2 + v.^2).^(0.5));
                        caxis([0 500])
                    case 'sedimenting'
                        Uflow   = ((u.^2 + v.^2).^(0.5));
                end
                cbar.Label.Interpreter      = 'latex';
                cbar.TickLabelInterpreter   = 'latex';
                cbar.FontSize               = 15;
                if idx == 3
                    %   cbar.Ticks  = [20 60 100 140];
                    cbar.Ticks  = [500 1000 1500];
                end
                pcol = pcolor(x,y,Uflow);
                set(pcol,'EdgeColor','None')
                shading flat
                
                slines = streamslice(x,y,u,v,0.9);
                set(slines,'color',[163,198,255]./255,'LineWidth',1.05)
                
            case 0
                slines = streamslice(x,y,u,v,0.9);
                set(slines,'color',[163,198,255]./255,'LineWidth',1.15)
        end
        for n = 1:Nfil
            plot(fil(n).x,fil(n).y,'r','LineWidth',2);
        end
        
    end % idx check loop
end

pos = gcf;
if Nfil == 9
    set(gcf,'Position',[pos.Position(1), pos.Position(2), 962, 376])
else
    set(gcf,'Position',[pos.Position(1), pos.Position(2), 962, 282])
end
set(gcf,'PaperPositionMode','auto')

end % function
