function L = CheckArclength(x,y)

% Check the arclength of the curve defined through Cartesian coordinates x
% and y.

dx = diff(x);
dy = diff(y);
L  = sum(vecnorm([dx(:)';dy(:)'],2,1));

end % function