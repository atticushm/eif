function [axSize] = GetAxes(fil,filGrid,axZoom)

% Calculates axis centered on the centre of mass of a filament/group of
% filaments. Axes are squared (for non-square filGrid configurations).

% Inputs:
% - fil: filament data; must contain centre of mass data.
% - filGrid: dimensions of filament arrangement grid.
% - axZoom: used to manually tweak axis `zoom'.

com         = [mean(horzcat(fil.comx)); mean(horzcat(fil.comy))];
axSize      = [com(1)-axZoom*filGrid(1), com(1)+axZoom*filGrid(1),...
               com(2)-axZoom*filGrid(2), com(2)+axZoom*filGrid(2)];
axDx        = (axSize(2)-axSize(1))/2;
axDy        = (axSize(4)-axSize(3))/2;

axis equal
if filGrid(1) > filGrid(2)
    axSize = [com(1)-axDx,com(1)+axDx,com(2)-axDx,com(2)+axDx];
elseif filGrid(1) < filGrid(2)
    axSize = [com(1)-axDy,com(1)+axDy,com(2)-axDy,com(2)+axDy];
end

end % function 