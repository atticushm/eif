function [chebf,avth] =  GetChebInterpolant(filn,Q,eps)

val     = mean(filn.th);
base    = [filn.x(1);filn.y(1);val*ones(Q,1)];
avth    = filn.input(3:end) - base(3:end);
avY     = [filn.x(1);filn.y(1); avth];

% Calculate the order of Chebyshev polynomial required to
% capture the buckling shape theta - theta_av. We use
% chebfun with the 'equi' control (as thetas are equispaced
% data)
chebf   = chebfun(avth,[filn.sm(1),filn.sm(end)],'equi','eps',eps);

end