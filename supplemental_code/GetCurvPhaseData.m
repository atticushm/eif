function [S,T,curv] = GetCurvPhaseData(sol,tps,Nfil,Q)

Ntps = length(tps);
sol  = reshape(sol,Nfil*(Q+2),Ntps,[]);
for n = 1:Ntps
    for i = 1:Nfil
        sol_t                   = reshape(sol(:,n),Q+2,Nfil);
        fil(i).data(:,n)        = sol_t(:,i);
        fil(i).curv(:,n)        = diff(fil(i).data(3:end,n))/(1/Q);
        [~,~,fil(i).tang(:,n)]  = GetFilamentCoordinates(fil(i).data(:,n), 1/Q);
    end
end

for i = 1:Nfil
    s       = (1:Q-1)/Q;
    [S,T]   = meshgrid(s,tps);
    curv    = fil(i).curv';
end

end % function
