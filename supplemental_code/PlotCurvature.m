function [fig1,fig2] = PlotCurvature(sol,tps,N,Q)
% N is number of filaments.
% sol is solution data at time points tps.
% Q is number of segments per filament.

%% Extract filament curvature from solution data

Ntps = length(tps);
sol  = reshape(sol,Q+2,[]);
for n = 1:Ntps
    for i = 1:N
        fil(i).data(:,n)        = sol(1:Q+2,n);
        fil(i).curv(:,n)        = sol(3:Q+2,n);
        [~,~,fil(i).tang(:,n)]  = GetFilamentCoordinates(fil(i).data(:,n), 1/Q);
    end
end

%% Animated s vs kappa plot
%{
figure;
fig1 = panel(); fig1.pack(1,N); box on
for n = 1:Ntps
    for i = 1:N
        s = (0:Q)/Q;
        plot(s(2:end),fil(i).curv(:,n))
        title(sprintf('t=%g',tps(n)))

        axc = mean(fil(i).curv(:,n));
        dax = 1;
        axis([s(1) 1 axc-dax axc+dax])
        xlabel('arclength s')
        ylabel('curvature')
        drawnow
    end
end
%}
%% Tangent angle phase diagram

global eta phi

figure;
fig2 = panel(); fig2.pack(1,N);
for i = 1:N
    fig2(1,i).select(); box on;
    s       = (1:Q)/Q;
    [S,T]   = meshgrid(s,tps);
    tangle  = fil(i).tang';
    surf(T,S,tangle,'EdgeColor','None')
    xlabel('time');
    ylabel('arclength'); yticks([0 1])
    view(2); axis tight; 
    colbar = colorbar;
    colbar.Label.String = 'tangent angle';
    
    title('Q=%g, phi=%g, eta=%g',Q,phi,eta)
end

end % function
