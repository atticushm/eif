function q = FigShapeWithStreamlinesHighG(G,sol,tps,grid)
q       = panel(); q.pack(2,4);
% tsave   = 1e-3*[5.5:0.5:9];
% tsave   = 1e-3*[3:0.5:6.5];
% tsave   = 1e-3*[4:0.5:7.5];
tsave   = 1e-3*[4.5:0.5:8];
Ntps    = length(tps);
Nfil    = grid(1)*grid(2);
for i = 1:Ntps
    [check,idx]      = max(abs(tps(i)-tsave)<4e-5);
    
    % Get initial filament centre of mass:
    if i == 1
        [~,~,fil,~]     = GetFilamentData(sol(:,i),Nfil,G);
        xc_int          = fil.comx; 
    end
    
    if check == 1
        [~,~,fil,~]     = GetFilamentData(sol(:,i),Nfil,G);
        if idx <= 4
            idx2 = [1,idx];
            q(1,idx).select(); hold on; box on;
        else
            idx2 = [2,idx-4];
            q(2,idx-4).select(); hold on; box on;
        end
        fbox            = gca;
        fbox.LineWidth  = 1.5;
        fbox.FontSize   = 16;
        fbox.Layer      = 'Top';
        axZoom          = 0.6;
        set(gca,'ytick',[],'yticklabel',[]);
        set(gca,'xtick',[])
        streamGrid  = [grid(1)*20,grid(2)*20];
        annotationx = [.016, 0.265, 0.512, 0.758];
        annotationy = [.67, .18];
        annotation(...
            'textbox',[annotationx(idx2(2)),annotationy(idx2(1)),.3,.3],...
            'interpreter','latex',...
            'string',sprintf('$t=%g$',tsave(idx)),...
            'fitboxtotext','on',...
            'edgecolor','none',...
            'backgroundcolor','white',...
            'fontsize',16 ...
            );
        axSize  = GetAxes(fil,grid,axZoom);
        axis(axSize)
        set(gca,'TickLabelInterpreter','latex');

        streams     = GetStreamlines(sol(:,i),tps(i),Nfil,axSize,'sedimenting',G,grid,streamGrid,0);
        x           = streams.xmesh;
        y           = streams.ymesh;
        D           = size(x);
        u           = reshape(streams.vx,[D(1),D(2)]);
        v           = reshape(streams.vy,[D(1),D(2)]);
        slines      = streamslice(x,y,u,v,0.9);
        set(slines,'color',[163,198,255]./255,'LineWidth',1.15)       
        plot(fil(1).x,fil(1).y,'r','LineWidth',2);

        % Draw dotted line indicating the filament's initial centre of mass
        % (obtained above):
        Ylim = get(gca,'YLim');
        line([xc_int,xc_int],[Ylim(1),Ylim(2)],'LineWidth',1.4,'Color','k','LineStyle','--')

    end % check loop
end % time loop
set(gcf,'Position',[600 600 953 471])
q.margin = 3;
end % function
