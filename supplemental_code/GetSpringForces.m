function F = GetSpringForces(x, b0)
% Calculates the spring forces on a connected chain of beads in 3-dimensional space.

Np = size(x,2);
F  = zeros(3,Np);

for p = 1:Np
    if p > 1
        bpn1  = x(:,p-1) - x(:,p);
        Nbpn1 = norm(bpn1);
        F1    = -2*(Nbpn1 - b0)*(-bpn1/Nbpn1);
    else
        F1 = 0;
    end
    if p < Np
        bp   = x(:,p) - x(:,p+1);
        Nbp  = norm(bp);
        F2   = -2*(Nbp - b0)*(bp/Nbp);
    else
        F2 = 0;
    end
    F(:,p) = F1 + F2;
end
end % function
