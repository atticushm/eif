function vid = PlayMovie(frames,fps)
% Plays movie from frame struct data.
% Input "frames" should be of the form "frames.data(:)" where the field
% "data" is a struct with fields "cdata" and "colormap".a

for n = 1:size(frames.data,2)
    M(n) = frames.data(n);
end

vid = figure;
movie(vid,M,1,fps)

end %function