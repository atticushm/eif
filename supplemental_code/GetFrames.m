function frames = GetFrames(model,solutions,tps,varargin)

Nfil    = model.Nfil;
Q       = model.Q;

if numel(varargin) == 1
    sdata = [];
    headTrack = varargin{1};
else
    sdata = [];
    headTrack = 0;
end

if isempty(sdata)
    splot = 0;
else
    splot = 1;
end

%% Main
% Extract field point velocities at tpoints=tps.

Ntps = length(tps);

fprintf('Computing movie frame data...\n')
figure('visible','off')  
p = panel(); p.pack(1,1);

for n = 1:Nfil
    head(n).vec = [];
end

for i = 1:Ntps
       
    p(1,1).select();
    cla; hold on; box on;
    
    % Calculate filament coordinates.
    soldata = solutions(:,i);
    soldata = reshape(soldata,[],Nfil)';
    for n = 1:Nfil
        fil(n).sol              = soldata(n,:);
        [fil(n).x, fil(n).y, ~] = GetFilamentCoordinates(fil(n).sol,1/Q);
        fil(n).comx             = mean(fil(n).x);
        fil(n).comy             = mean(fil(n).y);
    end
    
    % Dynamically scale figure axes, centered on the average centre of
    % mass.
    com             = [mean(horzcat(fil.comx)); mean(horzcat(fil.comy))];
    com_vec(:,i)    = com;
    
    if splot == 1
        % Setup streamlines start points
        startx = sdata(i).coords(1,:);
        starty = sdata(i).coords(2,:);
        x      = sdata(i).xmesh;
        y      = sdata(i).ymesh;
        D      = size(x);
        
        % Plot streamlines and filament configs.
        % Setup domain field point velocities.
        nlines  = sdata(i).nlines;
        Svx     = sdata(i).vx;
        Svy     = sdata(i).vy;
        u       = reshape(Svx,[D(1),D(2)]);
        v       = reshape(Svy,[D(1),D(2)]);
        
        dAx     = sdata(i).dAx(1);
        dAy     = sdata(i).dAx(2);
        
        Stream = streamline(x,y,u,v,startx,starty);
        set(Stream,'color',[163, 198, 255]./255)
    else
        dAx     = model.grid(1);
        dAy     = model.grid(2);
    end
    
    % Plot filaments.
    for n = 1:Nfil
        hold on;
        plot(fil(n).x,fil(n).y,'r-','LineWidth',2)
        if headTrack == 1 && tps(i) > 0
            head(n).vec = [head(n).vec, [fil(n).x(1);fil(n).y(1)]];
            plot(head(n).vec(1,:),head(n).vec(2,:),'b','LineWidth',1.2)
        end
        hold off;
    end
                  
    title(sprintf('t=%g',tps(i)))
    xlabel('x'); ylabel('y')
    axis equal
    axis([com(1)-dAx, com(1)+dAx, com(2)-dAy, com(2)+dAy])
    
    % Save frame data for movie.
    frame(i)        = getframe(gcf);
    frames.data(i)  = frame(i);
    
%     delete(findall(gcf,'Tag','figann'))
    clear x y u v D startx starty
    
    disp(['Fr=',num2str(i),'/',num2str(length(tps))])
end

fprintf(' complete!\n\n')

end % functionfig1
