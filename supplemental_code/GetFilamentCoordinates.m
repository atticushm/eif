function [x,y,th] = GetFilamentCoordinates(Y,ds)
% calculates the cartesian coordinates of the final configuration of a
% filament solved using the EHD model. Input is the solution filament
% configuration, and output is a 2Q+1 matrix of xy-coordinates of each joint
% between Q segments. ds is the (uniform) spacing between each segment.

format long

x(1)    = Y(1);
y(1)    = Y(2);
th      = Y(3:end);

for n = 1:length(th)
    x(n+1)  = x(n) + ds*cos(th(n));
    y(n+1)  = y(n) + ds*sin(th(n));
end

x   = x(:);
y   = y(:);
th  = th(:);

end % function
