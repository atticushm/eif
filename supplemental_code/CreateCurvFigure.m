function CreateCurvFigure(Q,eta)

%% Curvature figure

[solutions,tps,~] = FilamentsShearFunction(eta,[1,1],[0,0],Q);

figure;
p = panel('no-manage-font'); p.pack(1);

[S,T,curv] = GetCurvPhaseData(solutions,tps,1,Q);
surf(T,S,curv,'EdgeColor','None'); view(2); axis tight;

xlabel('Time $t$',      'interpreter','latex','fontsize',16);
ylabel('Arclength $s$', 'interpreter','latex','fontsize',16)
yticks([0 1])
yticklabels ({'0','1'});

set(gca,'TickLabelInterpreter','latex')

if eta == 34000
    hold on
    plot3(1.6*ones(1,100),linspace(0.025,0.975),50*ones(1,100),'r--','LineWidth',1.5)
    plot3(2.7*ones(1,100),linspace(0.025,0.975),50*ones(1,100),'r--','LineWidth',1.5)
end

cbar                        = colorbar;
cbar.Location               = 'northoutside';
cbar.Label.Interpreter      = 'latex';
cbar.TickLabelInterpreter   = 'latex';
cbar.FontSize               = 14;

set(gca,'TickLabelInterpreter','latex')

%{
str = sprintf('$\\textup{Sp}^4 = %g$',eta);
annotation( 'textbox',[.14 .45 .3 .3],   ...
            'interpreter','latex',      ...
            'string',str,               ...
            'FitBoxToText','on',        ...
            'FontSize',15,              ...
            'backgroundcolor','w',      ...
            'EdgeColor','none');
%}
     
box on;
fbox            = gca;
fbox.Layer      = 'top';
fbox.FontSize   = 15;
fbox.LineWidth  = 1.5;
        
set(gcf,'position',[600 600 440 330]);


end % function main

%% AUXILLARY FUNCTIONS: Curvature data.

function [S,T,curv] = GetCurvPhaseData(sol,tps,Nfil,Q)

Ntps = length(tps);
sol  = reshape(sol,Nfil*(Q+2),Ntps,[]);
for n = 1:Ntps
    for i = 1:Nfil
        sol_t                   = reshape(sol(:,n),Q+2,Nfil);
        fil(i).data(:,n)        = sol_t(:,i);
        fil(i).curv(:,n)        = diff(fil(i).data(3:end,n))/(1/Q);
        [~,~,fil(i).tang(:,n)]  = GetFilamentCoordinates(fil(i).data(:,n), 1/Q);
    end
end

for i = 1:Nfil
    s       = (1:Q-1)/Q;
    [S,T]   = meshgrid(s,tps);
    curv    = fil(i).curv';   
end

end % function
