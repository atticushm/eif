function [Y0,x,y,th] = GetPerturbedIntCondYoung(model,dth_0)
% Initial condition from Y.N. Young (2009) paper "Hydrodynamic interactions
% between two semiflexible inextensible filaments in Stokes flow".

% model is a structure containing all the problem parameters.
% dth_0 is the initial condition perturbation parameter.

Q        = model.Q;
Q2       = 1000;
X(1:2,1) = [-.5;0];
s        = 0:1/Q2:1;

for m = 2:Q2+1
    I = [0;0];
    for n = 1:m-1
        th(n)   = dth_0*(s(n)^3/3 + s(n)^5/5 - s(n)^4/2);
        I       = I + [cos(th(n)); sin(th(n))];        
    end  
    X(1:2,m) = X(1:2,1) + I/Q2;
    x   = X(1,:);
    y   = X(2,:);
end 

ss = 0:(1/Q):1;
xx = spline(s,x,ss); %x = xx(:);
yy = spline(s,y,ss); %y = yy(:);

dy = diff(yy);
dx = diff(xx);
th = atan(dy./dx); th = th(:);

th = th + model.th_0;          % initial angles after rotation by th_0.

Nfil = model.Nfil;
for n = 1:Nfil
    % Get filament coordinates using new rotated angles:
    Y           = [model.x0(n); model.y0(n); th(:)];
    [x,y,th]    = GetFilamentCoordinates(Y,1/Q);
    
    % Translate filament so y=0 crosses the midpoint:
    xt(:,n)     = x - mean(x) + model.sep(1)*(n-1);
    yt(:,n)     = y - mean(y);
    
    % Reconstruct Y0:
    Y0(:,n)     = [xt(1,n); yt(1,n); th];
end

% Reshape to stack Y0 vectors:
Y0  = Y0(:);
x   = xt;
y   = yt;

end % function 