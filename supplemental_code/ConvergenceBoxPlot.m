function ConvergenceBoxPlot(x,y,z,type1,type2,varargin)
% Original code by M.T. Gallager.
% Edited by A.L. Hall-McNair (2019) for use in Chebyshev interpolation
% figures of buckling filaments in shear flow.
 
switch type1
    case 'normal'
        % Set limits in x and y
        x = x(:);
        y = y(:);
        X = zeros(length(x)+1,1);
        Y = zeros(length(y)+1,1);
        
        X(2:end-1,1) = (x(2:end) - x(1:end-1))/2;
        Y(2:end-1,1) = (y(2:end) - y(1:end-1))/2;
        
        X(1) = x(1)-X(2);
        Y(1) = y(1)-Y(2);
        X(end) = X(end-1);
        Y(end) = Y(end-1);
        
        X(2:end,1) = X(2:end,1) + x(:);
        Y(2:end,1) = Y(2:end,1) + y(:);
        
    case 'fixedWidth'
        
        x = x(:);
        y = y(:);
        
        X = 0 : (length(x)); X = X(:)/2;
        Y = 0 : (length(y)); Y = Y(:)/2;
        
    otherwise
        error('Plot type not known')
end
 
 
%% Specify colours
if isempty(varargin)
    cMin = min(z(:));
    cMax = max(z(:));
else
    cMin = varargin{1}(1);
    cMax = varargin{1}(2);
end
 
colours = parula(255);
c = linspace(cMin,cMax,255);
cR = spline(c,colours(:,1));
cG = spline(c,colours(:,2));
cB = spline(c,colours(:,3));
 
%% Plot
figure
XTP = 0 * x;
YTP = 0 * y;
for ii = 1 : length(x)
    for jj = 1 : length(y)
        
        LX = X(ii+1)-X(ii);
        LY = Y(jj+1)-Y(jj);
        
        X0 = X(ii);
        Y0 = Y(jj);
        
        XTP(ii) = X0 + LX/2;
        YTP(jj) = Y0 + LY/2;
        
        Z = z(jj,ii);
        
        if ~isnan(Z)
            if Z < cMin 
                Z = cMin;
            elseif Z > cMax
                Z = cMax;
            end
            
            hold on
            rectangle('Position',[X0,Y0,LX,LY], ...
                'FaceColor',[fnval(cR,Z),fnval(cG,Z),fnval(cB,Z)], ...
                'EdgeColor',[fnval(cR,Z),fnval(cG,Z),fnval(cB,Z)])
%                 'EdgeColor','k')
        end
    end
end
 
xticks([0,2,4,6])
% xticks(YTP)
% xticklabels(num2str(x))
% yticklabels(num2str(y))

xlabel('Time $t$','interpreter','latex','fontsize',16)
ylabel('Polynomial index $n$','interpreter','latex','fontsize',16)

fbox            = gca;
fbox.LineWidth  = 1.5;
fbox.FontSize   = 12;
fbox.Layer      = 'Top';
fbox.TickLabelInterpreter = 'latex';

shading flat

axis tight
box on

cbar                        = colorbar;
cbar.FontSize               = 12;
cbar.TickLabelInterpreter   = 'latex';
cbar.Label.String           = '$ | \alpha_n | $';
cbar.Label.Rotation         = 0;
cbar.Label.Interpreter      = 'latex';
if type2 == 1
    yticks([0 5 10 15])
    vec = [0 0.25];
    cbar.Ticks = vec;
    caxis([vec(1) vec(end)]);
elseif type2 == 2
    yticks([0 10 20 30])
    vec = [0 0.7];
    cbar.Ticks = vec;
    caxis([vec(1) vec(end)]);
end

set(gcf,'position',[260 425 500 413])
 
end