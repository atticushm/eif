function CreateFilamentFigures(Nfil,problem,filGrid,solutions,tps,param,drawStreams,drawPcolor)

% Code by A.L. Hall-McNair, University of Birmingham (2019).

% This function consolidates the previously seperate functions
% CreateRelaxingFigure, CreateShearFigure & CreateGravFigure into a single
% framework for easy code recollection and more transparent structuring.

% Inputs:
% - Nfil: number of filaments in the system.
% - problem: string input defining the filament problem being examined.
% - filGrid: dimensions of filament arrangement grid.
% - solutions: solution data.
% - tps: time points at which solution data is given.
% - param: non-dimensional parameter of the system (if any).
% - subLabel: figure sublabel (for latex).
% - drawStreams: boolean data to plot streamlines (optional).
% - drawPcolor: boolean to decide whether to draw flow pcolor (flow velocity magnitude).

%% Check inputs and setup contingencies.

warning off; close all

switch nargin
    case 8
        CreateFigures(Nfil,problem,filGrid,solutions,tps,param,drawStreams,drawPcolor);
    case 7
        CreateFigures(Nfil,problem,filGrid,solutions,tps,param,drawStreams,0);
    case 6
        CreateFigures(Nfil,problem,filGrid,solutions,tps,param,0,0);
    case 5
        CreateFigures(Nfil,problem,filGrid,solutions,tps,[],0,0);
    otherwise
        warning('Not enough essential parameters specified!')
end

end % function

%% Check problem and draw plots accordingly.

function CreateFigures(Nfil,problem,filGrid,solutions,tps,param,drawStreams,drawPcolor)

Ntps    = length(tps);
tmax    = tps(end);
switch problem
    case 'shear'
        tsave   = [1.1, 2.2, 3.3, 4.4];
        f1      = figure(1);
    case 'sedimenting'
        if Nfil == 1
            tsave   = [0 3e-3 6e-3 9e-3];
        else
            tsave   = [0 1e-3 2e-3 3e-3];
        end
        f1      = figure(1);
    case 'swimming'
        tsave   = [10 15 20 25];
        f1      = figure(1);
    otherwise
        tsave   = [tmax/4, tmax/2, (3*tmax)/4, tmax];
        warning('Prescribing manual tsave (plotting time-points) advised!')
end

p = panel(1,'no-manage-font');
p.pack('h',4)
p.margin = 3;

for i = 1:Ntps
    switch problem
        case 'swimming' % requires larger pick tolerance due to larger dt
            [check,idx]    = max(abs(tps(i)-tsave)<1e-1);
        otherwise
            if Nfil == 1
                [check,idx] = max(abs(tps(i)-tsave)<5e-5);
            else
                [check,idx] = max(abs(tps(i)-tsave)<5e-5);
            end
    end
    [Q,~,fil,~]     = GetFilamentData(solutions(:,i),Nfil,param);
    if check == 1
        
        p(idx).select();
        hold on; box on;
        fbox            = gca;
        fbox.LineWidth  = 1.5;
        fbox.FontSize   = 12;
        fbox.Layer      = 'Top';
               
        %%  4-pane Configuration plots with streamlines
        
        switch drawStreams
            case 1
                switch problem
                    case 'relaxing'
                        axZoom = 0.7;
                        set(gca,'ytick',[],'yticklabel',[]);
                        set(gca,'xtick',[])
                        streamGrid  = [filGrid(1)*200,filGrid(2)*200];
                    case 'shear'
                        switch drawPcolor
                            case 1
                                if Nfil == 1
                                    axZoom = 0.8;
                                elseif Nfil == 2
                                    axZoom = 0.55;
                                end
                            otherwise
                                axZoom = 0.8;
                        end
                        set(gca,'ytick',[],'yticklabel',[]);
                        set(gca,'xtick',[])
                        streamGrid  = [filGrid(1)*200,filGrid(2)*200];
                        
                        if drawPcolor == 1
                            annotationx = [0.034, 0.277, 0.519, 0.762];
                            annotationy = .5;
                        else
                            annotationx = [.015, 0.265, 0.51, 0.757];
                            annotationy = .59;
                        end
                        if (sum(param == [5000,20000]) == 2) == 1
                            annotation(...
                                'textbox',[annotationx(idx),annotationy,.3,.3],...
                                'interpreter','latex',...
                                'string',sprintf('$t=%g$',tps(i)),...
                                'fitboxtotext','on',...
                                'edgecolor','none',...
                                'backgroundcolor','white',...
                                'fontsize',16 ...
                                );
                        elseif drawStreams == 1
                            annotation(...
                                'textbox',[annotationx(idx),annotationy,.3,.3],...
                                'interpreter','latex',...
                                'string',sprintf('$\\mathcal{V} = %g,\\,t=%g$',param,tps(i)),...
                                'fitboxtotext','on',...
                                'edgecolor','none',...
                                'backgroundcolor','white',...
                                'fontsize',16 ...
                                );
                        else
                            annotation(...
                                'textbox',[annotationx(idx),annotationy,.3,.3],...
                                'interpreter','latex',...
                                'string',sprintf('$\\mathcal{V} = %g,\\,t=%g$',param,tps(i)),...
                                'fitboxtotext','on',...
                                'edgecolor','none',...
                                'backgroundcolor','white',...
                                'fontsize',16 ...
                                );
                        end
                        
                    case 'sedimenting'
                        if Nfil == 1
                            axZoom = 0.6;
                        elseif Nfil == 2
                            axZoom = 0.8;
                        elseif Nfil == 4
                            axZoom = 0.8;
                        elseif Nfil == 9
                            axZoom = 0.8;
                        end                          
                        set(gca,'ytick',[],'yticklabel',[]);
                        set(gca,'xtick',[])
                        streamGrid  = [filGrid(1)*20,filGrid(2)*20];
                        annotationx = [.015, 0.265, 0.51, 0.757];
                        annotation(...
                            'textbox',[annotationx(idx),0.59,.3,.3],...
                            'interpreter','latex',...
                            'string',sprintf('$\\mathcal{G} = %g,\\,t=%g$',param,tsave(idx)),...
                            'fitboxtotext','on',...
                            'edgecolor','none',...
                            'backgroundcolor','white',...
                            'fontsize',16 ...
                            );
                    case 'swimming'
                        axZoom = 0.7;
                        xlabel('$x$','Interpreter','latex','FontSize',16)
                        set(gca,'ytick',[],'yticklabel',[]);                  
                        streamGrid  = [filGrid(1)*200,filGrid(2)*200];
                    otherwise
                        warning('Unrecognised problem specified!')
                end
                axSize  = GetAxes(fil,filGrid,axZoom);
                axis(axSize)
                
                streams     = GetStreamlines(solutions(:,i),tps(i),Nfil,axSize,problem,param,filGrid,streamGrid,drawPcolor);
                x           = streams.xmesh;
                y           = streams.ymesh;
                D           = size(x);
                u           = reshape(streams.vx,[D(1),D(2)]);
                v           = reshape(streams.vy,[D(1),D(2)]);
                
                %% Plots with velocity magnitude background.
                
                switch drawPcolor 
                    case 1
                        cbar                    = colorbar;
                        cbar.Location           = 'northoutside';
                        cbar.FontSize           = 12;
                        switch problem
                            case 'shear'
                                uvec    = u(:);
                                vvec    = v(:);
                                Uref    = norm([uvec(1);vvec(1)]);
                                Uflow   = ((u.^2 + v.^2).^(0.5))/Uref;
                                if idx == 1
                                    if (sum(param == [5000,20000]) == 2) == 1
                                        val = [0 50 100];
                                    else
                                        val = [0,50,100];
                                    end
                                        cbar.Ticks = val;
                                        caxis([val(1),val(end)])
                                elseif idx == 2
                                    if (sum(param == [5000,20000]) == 2) == 1
                                        val = [0 75 150];
                                    else
                                        val = [0,75,150];
                                    end
                                        cbar.Ticks = val;
                                        caxis([val(1),val(end)])
                                elseif idx == 3
                                    global sub
                                    if strcmp(sub,'a')==1
                                        val = [0,75,150];
                                    elseif (sum(param == [5000,20000]) == 2) == 1
                                        val = [0,1000,2000];
                                    else
                                        val = [0,1000,2000];
                                    end
                                    cbar.Ticks = val;
                                    caxis([val(1),val(end)])
                                elseif idx == 4
                                    if (sum(param == [5000,20000]) == 2) == 1
                                        val = [0,25,50];
                                    else
                                        val = [0,25,50];
                                    end
                                    cbar.Ticks = val;
                                    caxis([val(1),val(end)])
                                end
                            case 'relaxing'
                                Uflow   = ((u.^2 + v.^2).^(0.5));
                            case 'swimming'
                                Uflow   = ((u.^2 + v.^2).^(0.5));
                                caxis([0 500])
                            case 'sedimenting'
                                Uflow   = ((u.^2 + v.^2).^(0.5));
                        end
                        cbar.Label.Interpreter      = 'latex';
                        cbar.TickLabelInterpreter   = 'latex';
                        cbar.FontSize               = 15;

                        pcol = pcolor(x,y,Uflow);
                        set(pcol,'EdgeColor','None')
                        shading flat
                        
                        slines = streamslice(x,y,u,v,0.9);
                        set(slines,'color',[163,198,255]./255,'LineWidth',1.05)
                        
                    case 0
                        slines = streamslice(x,y,u,v,0.9);
                        set(slines,'color',[163,198,255]./255,'LineWidth',1.15)
                end
            case 0 
                warning('Suggest turning on drawStreams for better plots!')              
        end
        
        for n = 1:Nfil
            plot(fil(n).x,fil(n).y,'r','LineWidth',2);
        end
        
    end % idx check loop
    
    %% Chebyshev plot for filaments in shear.
    % Not really setup for multiple filaments... take care.
    
    if Nfil == 1
    switch problem
        case 'shear'
            % Get baseline config i.e. remove average angle.        
            for n = 1:Nfil
                [cheb(n).func,avth] = GetChebInterpolant(fil(n),Q,5e-2);
                cheb(n).order(i)    = length(cheb(n).func)-1;
                
                annX = [.153, .359, .566, .772];
                annY = .618;
                if check == 1
                    if Nfil > 1
                        warning('Chebyshev plots not setup for multiple filaments; errors likely')
                    end
                    figure(2);
                    subplot(2,4,idx); hold on; box on;
                    fbox            = gca;
                    fbox.LineWidth  = 1.5;
                    fbox.FontSize   = 12;
                    fbox.Layer      = 'Top';
                    
                    l1 = plot(fil(n).sm,avth,'LineWidth',1.4);
                    l2 = plot(fil(n).sm,cheb(n).func(fil(n).sm),'*--','LineWidth',1.4);
                    
                    chebOrder   = cheb(n).order(i);
                    annotation(...
                        'textbox', [annX(idx),annY,0.3,0.3],...
                        'interpreter','latex',...
                        'string', sprintf('$ \\mathcal{O}(T) = %g $',chebOrder),...
                        'fitboxtotext','on',...
                        'edgecolor','none',...
                        'fontsize',14);
                    
                    xlabel('$\tilde{s}^{[m]}(t)$','Interpreter','latex','FontSize',15)
                    ylabel('$\tilde{\theta}^{[m]}(t)-\bar{\tilde{\theta}^{[m]}}(t)$','Interpreter','latex','FontSize',15)
                    
                    if idx == 4
                        dummy            = subplot(2,4,8);
                        lgd1             = legend(dummy,[l1,l2],{'Filament shape','Chebyshev interpolant at 5\% error'});
                        lgd1.Location    = 'southoutside';
                        lgd1.Orientation = 'horizontal';
                        lgd1.FontSize    = 16;
                        lgd1.Interpreter = 'latex';
                        lgd1.Position    = [0.3962, 0.4385, 0.25, 0.0524];
                        axis(dummy,'off')
                    end
                end
            end % Nfil loop
    end % switch
    end
end % i loop over tps

set(figure(1),'Position',[640 1400,962 282])
set(figure(1),'PaperPositionMode','auto')

if drawPcolor == 1
    p.margin = 8.5;
    set(figure(1),'Position',[560 280 1020 350])
end

if ishandle(2) == 1
    set(figure(2),'Position',[640 1400,962 552])
    set(figure(2),'PaperPositionMode','auto')
end

clear fil

%% Extended plot for large G over large time interval.

switch problem
    case 'sedimenting'
        if param == 3000
            q = panel(); q.pack(2,4);
            tsave   = 1e-3*[3 3.5 4 4.5 5 5.5 6 6.5];
            for i = 1:Ntps
                [check,idx]      = max(abs(tps(i)-tsave)<5e-5);
                if check == 1
                    [Q,~,fil,~]     = GetFilamentData(solutions(:,i),Nfil,param);
                    if idx <= 4
                        idx2 = [1,idx];
                        q(1,idx).select(); hold on; box on;
                    else
                        idx2 = [2,idx-4];
                        q(2,idx-4).select(); hold on; box on;
                    end
                    fbox            = gca;
                    fbox.LineWidth  = 1.5;
                    fbox.FontSize   = 16;
                    fbox.Layer      = 'Top';
                    axZoom          = 0.6;
                    set(gca,'ytick',[],'yticklabel',[]);
                    set(gca,'xtick',[])
                    streamGrid  = [filGrid(1)*20,filGrid(2)*20];
                    annotationx = [.016, 0.265, 0.512, 0.758];
                    annotationy = [.67, .18];
                    annotation(...
                        'textbox',[annotationx(idx2(2)),annotationy(idx2(1)),.3,.3],...
                        'interpreter','latex',...
                        'string',sprintf('$t=%g$',tsave(idx)),...
                        'fitboxtotext','on',...
                        'edgecolor','none',...
                        'backgroundcolor','white',...
                        'fontsize',16 ...
                        );
                    axSize  = GetAxes(fil,filGrid,axZoom);
                    axis(axSize)
                    set(gca,'TickLabelInterpreter','latex');
                    
                    streams     = GetStreamlines(solutions(:,i),tps(i),Nfil,axSize,problem,param,filGrid,streamGrid,drawPcolor);
                    x           = streams.xmesh;
                    y           = streams.ymesh;
                    D           = size(x);
                    u           = reshape(streams.vx,[D(1),D(2)]);
                    v           = reshape(streams.vy,[D(1),D(2)]);
                    slines      = streamslice(x,y,u,v,0.9);
                    set(slines,'color',[163,198,255]./255,'LineWidth',1.15)       
                    plot(fil(1).x,fil(1).y,'r','LineWidth',2);
                    
                    Ylim = get(gca,'YLim');
                    line([0.5,0.5],[Ylim(1),Ylim(2)],'LineWidth',1.4,'Color','k','LineStyle','--')
                    
                end % check loop
            end % time loop
            set(gcf,'Position',[600 600 953 471])
            q.margin = 3;
        end % G=10000 check
end

%% Plots for swimming filaments.
%  Hard coded for a one filament system.

%{
switch problem
    case 'swimming'
        %% Shape trace over time.
        
        figure; hold on; box on
        fbox            = gca;
        fbox.LineWidth  = 1.5;
        fbox.FontSize   = 12;
        fbox.Layer      = 'Top';

        tplot   = round(linspace(tps(14),tps(end),10),2);
        Ntplot  = length(tplot);
        colors  = parula(Ntplot);
        
        for i = 1:Ntps
            [check,idx]     = max(abs(tps(i)-tplot)<1e-1);
            if check == 1
                [Q,~,fil,~] = GetFilamentData(solutions(:,i),1,param);
                axSize      = GetAxes(fil,filGrid,0.7);
                axis(axSize)
                
                plot(fil.x,fil.y,'r','LineWidth',1.5,'color',colors(idx,:))
                scatter3(fil.x(1),fil.y(1),1,15,'k','filled')
                view(2)
            end
        end
        axis([-0.1 1 -0.2 0.2])
        cbar                    = colorbar;
        cbar.Label.String       = 'Time, $t$';
        cbar.Label.Interpreter  = 'latex';
        cbar.Limits             = [tplot(1),tplot(end)];
        cbar.FontSize           = 14;
        caxis([tplot(1),tplot(end)])
        
        xlabel('$x$','Interpreter','latex','FontSize',15)
        ylabel('$y$','Interpreter','latex','FontSize',15)
        
        str = sprintf('$ \\textup{Sp}^4=%g$, $m_0=%g,$ $k=%g$',param(1),param(2),param(3));
        title(str,'fontsize',16,'interpreter','latex')
        
        set(gcf,'Position',[300 500 660 270])
        
        %% Tangent angle phase
        
        figure; hold on; box on
        fbox            = gca;
        fbox.LineWidth  = 1.5;
        fbox.FontSize   = 12;
        fbox.Layer      = 'Top';

        [S,T,tangle]    = GetTangPhaseData(solutions,tps,1,Q);
        surf(T,S,tangle,'EdgeColor','None'); view(2); axis tight

        xlabel('Time, $t$','Interpreter','Latex','FontSize',16)
        ylabel('Arclength, $s$','Interpreter','Latex','FontSize',16)
        yticks([0,1]); yticklabels({'0','1'});

        cbar                    = colorbar;
        cbar.Label.String       = '$ \theta^{[m]}(t) $';
        cbar.Label.Interpreter  = 'latex';
        cbar.FontSize           = 14;   

        titlestr = sprintf('$ \\textup{Sp}=%g$, $m_0=%g,$ $k=%g$',round(param(1)^(0.25),2),param(2),param(3));
        title(titlestr,'fontsize',16,'interpreter','latex')

        set(gcf,'Position',[1000 500 410 255])
        
end
%}

%% Chebyshev interpolant order and coefficient plots.

if Nfil == 1
switch problem    
    case 'shear'
        f3      = figure(3);
        epsvec  = [0.05];
        for i = 1:Ntps
            [Q,~,fil,~] = GetFilamentData(solutions(:,i),Nfil,param);
            for k = 1:length(epsvec)
                [cheby(k,i).f,~]    = GetChebInterpolant(fil(n),Q,epsvec(k));                
                cheby(k,i).O        = length(cheby(k,i).f)-1;
                cheby(k,i).coeffs   = chebcoeffs(cheby(k,i).f);
                cheby(k,i).ncoeffs  = length(cheby(k,i).coeffs);
            end
        end
        
        % Order plot (against time)
        subplot(1,Nfil,n); hold on; box on
        fbox            = gca;
        fbox.LineWidth  = 1.5;
        fbox.FontSize   = 12;
        fbox.Layer      = 'Top';
        plot(tps,horzcat(cheby(1,:).O),'LineWidth',1.4)
%         plot(tps,horzcat(cheby(2,:).O),'LineWidth',1.4)
%         plot(tps,horzcat(cheby(3,:).O),'LineWidth',1.4)
%         plot(tps,horzcat(cheby(4,:).O),'LineWidth',1.4)
        
        xlabel('$ t $','interpreter','latex','fontsize',15)
        ylabel('$ \mathcal{O} (T(\tilde{\theta})) $','interpreter','latex','fontsize',15)
        
        lgd2                = legend('$\varepsilon = 5\times 10^{-2}$','$\varepsilon = 1\times 10^{-2}$','$\varepsilon = 1\times 10^{-3}$','$\varepsilon = 1\times 10^{-6}$');
        lgd2.Interpreter    = 'latex';
        lgd2.FontSize       = 16;
        lgd2.Orientation    = 'horizontal';
        lgd2.Location       = 'southoutside';
        lgd2.EdgeColor      = 'none';
        
        % Coefficient box plot
        set(figure(3),'Position',[640 1400 878 415])
        
        for k = 1:length(epsvec)
            [minval(k),minidx(k)] = min(horzcat(cheby(k,:).ncoeffs));
            [maxval(k),maxidx(k)] = max(horzcat(cheby(k,:).ncoeffs));
            for i = 1:Ntps
                if cheby(k,i).ncoeffs < maxval(k)
                    nzeros              = maxval(k)-cheby(k,i).ncoeffs; 
                    cheby(k,i).coeffs   = [cheby(k,i).coeffs; zeros(nzeros,1)];
                end
            end
            coeffs(k).mat = horzcat(cheby(k,:).coeffs);
            ConvergenceBoxPlot(tps,1:maxval(k),abs(coeffs(k).mat),'normal')
            figure(k+3);
            set(gcf,'Position',[255 540 420 285])
        end
end
end

end % function


%% Extra functions:
%  - GetAxes (computes dynamic figure axes)
%  - GetChebInterpolant
%  - GetTangPhaseData

function [chebf,avth] =  GetChebInterpolant(filn,Q,eps)

val     = mean(filn.th);
base    = [filn.x(1);filn.y(1);val*ones(Q,1)];
avth    = filn.input(3:end) - base(3:end);
avY     = [filn.x(1);filn.y(1); avth];

% Calculate the order of Chebyshev polynomial required to
% capture the buckling shape theta - theta_av. We use
% chebfun with the 'equi' control (as thetas are equispaced
% data)
chebf   = chebfun(avth,[filn.sm(1),filn.sm(end)],'equi','eps',eps);

end

function [S,T,tangle] = GetTangPhaseData(sol,tps,Nfil,Q)

Ntps = length(tps);
sol  = reshape(sol,Nfil*(Q+2),Ntps,[]);
for n = 1:Ntps
    for i = 1:Nfil
        sol_t                   = reshape(sol(:,n),Q+2,Nfil);
        fil(i).data(:,n)        = sol_t(:,i);
        fil(i).curv(:,n)        = diff(fil(i).data(3:end,n))/(1/Q);
        [~,~,fil(i).tang(:,n)]  = GetFilamentCoordinates(fil(i).data(:,n), 1/Q);
    end
end

s       = (1:Q)/Q;
[S,T]   = meshgrid(s,tps);
tangle  = fil(i).tang';

end % function

