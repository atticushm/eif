clear all; close all

Q   = [5,10,20,40,80,160];
NQ  = length(Q);

G   = [2500,3000,3500];
NG  = length(G);

arr = [1,1];
sep = [0,0];

if ~exist('./workspaces/figureS5-data.mat','file')
    for q = 1:NQ
        for g = 1:NG
            fprintf('[se,%g/%g,%g/%g]',q,NQ,g,NG)
            model = CreateModelStruct(Q(q),arr,sep,'sedimenting',G(g));
            model_ps = CreateModelStruct(10,arr,sep,'sedimenting',G(g));
            Y0 = GetParabolicIntCond(1e-7, model_ps);
            [sol(q).se(g).data,tps(q).se(g).data,crds(q).se(g).data] = FilamentsSedimentingFunction(model, Y0, 9e-3);
        end
    end
    save('./workspaces/figureS5-data.mat','Q','sol','tps','crds')
else
    load('./workspaces/figureS5-data.mat','Q','sol','tps','crds')
end
Npair   = NQ-1;
t       = tps(1).se(1).data;
Ntps    = length(t);
for g = 1:NG
    for k = 1:Npair

        X   = crds(k).se(g).data;
        Y   = crds(k+1).se(g).data; 
        
        Xth = sol(k).se(g).data(3:end,:);
        Yth = sol(k+1).se(g).data(3:end,:);

        for n = 1:Ntps
            Xp      = [X(n).x(:); X(n).y(:)];
            Yp      = [Y(n).x(1:2:end); Y(n).y(1:2:end)];

            Xthp    = 0.5*(Xth(2:end,n)+Xth(1:end-1,n)); 
            Ythm    = 0.5*(Yth(2:end,n)+Yth(1:end-1,n));

            step    = Q(k+1)/Q(k);
            idx     = (step):step:Q(k+1)-1;
            Ythp    = Ythm(idx);

            E(k).se(n,g)    = sqrt((1/Q(k))*sum(abs(Xp-Yp).^2));
            Eth(k).se(n,g)  = sqrt((1/Q(k))*sum(abs(Xthp-Ythp).^2));
        end
    end
end

for g = 1:NG
    
    % Coordinate error
    figure(1); 
    subplot(1,NG,g); hold on; box on;
    xlabel('$t$','Interpreter','latex','FontSize',16)
    ylabel('RMSE','Interpreter','latex','FontSize',16)
    title(sprintf('$\\mathcal{G}=%g$',G(g)),'Interpreter','latex','FontSize',16)
    cmap    = parula(NQ);
    for q = 1:NQ-1
        plot(t,E(q).se(:,g),'LineWidth',1.5,'Color',cmap(q,:))
    end
    legend('5v10','10v20','20v40','40v80','80v160','Location','southeast','NumColumns',2,'Interpreter','latex','FontSize',15,'EdgeColor','none','Color','none')    
    ax1 = gca;
    ax1.Layer                   = 'Top';
    ax1.YScale                  = 'log';
    ax1.TickLabelInterpreter    = 'latex';
    ax1.LineWidth               = 1.1;
    ax1.FontSize                = 15;
    ax1.XLim                    = [0 0.00901];
    ax1.XAxis.Exponent          = -3;
    ax1.YLim                    = [10^-6 10^1];
    xticks(1e-3*[0 3 6 9])
    yticks([10^-6 10^-3 10^0])
    axis square
    
    % Tangent angle error
    %{
    figure(2);
    subplot(1,NG,g); hold on; box on;
    xlabel('$t$','Interpreter','latex','FontSize',16)
    ylabel('Angle RMS','Interpreter','latex','FontSize',16)
    title(sprintf('$\\mathcal{G}=%g$',G(g)),'Interpreter','latex','FontSize',16)
    for q = 2:NQ-1
        plot(t,Eth(q).se(:,g),'LineWidth',1.5)
    end
    legend('10v20','20v40','40v80','80v160',...
        'Location','southoutside','NumColumns',3,'Interpreter','latex','FontSize',15,'EdgeColor','none')
    xticks(1e-3*[0 3 6 9])
    set(gca,'TickLabelInterpreter','latex','LineWidth',1.3,'FontSize',16)
    ax2 = gca;
    ax2.XLim = [0 0.00901];
    ax2.XAxis.Exponent = -3;
    if g == 1
        ax2.YLim = [0 0.07];
        yticks([0 0.035 0.07]);
        ax2.YAxis.Exponent = -2;
    elseif g == 2
        ax2.YLim = [0 1.5];
        yticks([0 0.75 1.5]);
    elseif g == 3
        ax2.YLim = [0 0.4];
        yticks([0 0.2 0.4]);
        ax2.YAxis.Exponent = -1;
    end
    %}
end

figure(1); 
% set(gcf,'Position',[228 45 1519 403])
set(gcf,'Position',[274 586 1239 358])
save2pdf('./figures/figureS5a')

% figure(2); 
% set(gcf,'Position',[228 45 1519 403])
% save2pdf('./figures/sedimenting-th-error')

% Config plot

sol20   = sol(3).se;
sol40   = sol(4).se;
sol80   = sol(5).se;
sol160  = sol(6).se;

crds20  = crds(3).se;
crds40  = crds(4).se;
crds80  = crds(5).se;
crds160 = crds(6).se;

for g = 2:3
    Gp = G(g);
    for i = 1:Ntps
        tsave       = [4e-3 6e-3 8e-3];
        [~,~,fil,~] = GetFilamentData(sol80(g).data(:,i),1,Gp);
        [check,idx] = max(abs(t(i)-tsave)<4e-5);
        if check == 1
            figure(3);
            subplot(1,length(tsave),idx); hold on; box on;
            ax = gca;
            ax.TickLabelInterpreter     = 'latex';
            ax.LineWidth                = 1.1;
            ax.FontSize                 = 15;
            ax.Layer                    = 'Top';
            axSize = GetAxes(fil,arr,0.5);
            axis equal
            if g == 2
               if idx == 1
                   ax       = gca;
                   ax.XLim  = [0 1];
                   ax.YLim  = [-6.5 -5.5];
                   xticks(ax.XLim)
                   yticks(ax.YLim)
               elseif idx == 2
                   ax       = gca;
                   ax.XLim  = [0 1];
                   ax.YLim  = [-9.8 -8.8];
                   xticks(ax.XLim)
                   yticks(ax.YLim)
               elseif idx == 3
                   ax       = gca;
                   ax.XLim  = [-0.5 1.5];
                   ax.YLim  = [-13.7 -11.5];
                   xticks(ax.XLim)
                   yticks(ax.YLim)
               end
            end
            if g == 3
                axis(axSize) 
               if idx == 1
                   ax       = gca;
                   ax.XLim  = [0 1];
                   ax.YLim  = [-7.4 -6.4];
                   xticks(ax.XLim)
                   yticks(ax.YLim)
               elseif idx == 2
                   ax       = gca;
                   ax.XLim  = [0 1];
                   ax.YLim  = [-10.9 -9.9];
                   xticks(ax.XLim)
                   yticks(ax.YLim)
               elseif idx == 3
                   ax       = gca;
                   ax.XLim  = [0.3 1.3];
                   ax.YLim  = [-15.2 -14.2];
                   xticks(ax.XLim)
                   yticks(ax.YLim)
               end
            end
            axis square
            if g == 3
                if idx == 1
                    
                elseif idx == 2
                    
                elseif idx == 3
                
                end
            end
            xlabel('$x$','Interpreter','latex')
            ylabel('$y$','Interpreter','latex')
            
            plot(crds20(g).data(i).x,crds20(g).data(i).y,'LineWidth',2,'Color',cmap(2,:))
            plot(crds40(g).data(i).x,crds40(g).data(i).y,'LineWidth',2,'Color',cmap(3,:))
            plot(crds80(g).data(i).x,crds80(g).data(i).y,'LineWidth',2,'Color',cmap(4,:))
            plot(crds160(g).data(i).x,crds160(g).data(i).y,'LineWidth',2,'Color',cmap(5,:))
            
            title(sprintf('$\\mathcal{G}=%g,\\,t= %g$',Gp,round(t(i),4)),'Interpreter','latex','FontSize',16)
            if idx == 1 
                legend( '$Q=20$','$Q=40$','$Q=80$','$Q=160$','Location','northwest','NumColumns',3,'Interpreter','latex','FontSize',15,'EdgeColor','none')
            end
            
            % Inlays   
            if g == 3
                figure(4);
                subplot(1,length(tsave),idx); hold on; box on;
                ax = gca;
                ax.TickLabelInterpreter     = 'latex';
                ax.LineWidth                = 1.3;
                ax.Layer                    = 'Top';

                plot(crds20(g).data(i).x,crds20(g).data(i).y,'LineWidth',2,'Color',cmap(2,:))
                plot(crds40(g).data(i).x,crds40(g).data(i).y,'LineWidth',2,'Color',cmap(3,:))
                plot(crds80(g).data(i).x,crds80(g).data(i).y,'LineWidth',2,'Color',cmap(4,:))
                plot(crds160(g).data(i).x,crds160(g).data(i).y,'LineWidth',2,'Color',cmap(5,:))

                axis square
                xticks([]);
                yticks([]);
                if idx == 1
                    axis([0.0310 0.1945 -6.9089 -6.7454])
                elseif idx == 2
                    axis([0.6630 0.8807 -10.7095 -10.4919])
                elseif idx == 3
                    if g == 3
                        axis([0.6554 0.9451 -14.7873 -14.4977])
                    elseif g == 2
                        axis auto
                    end
                end
            end
        end
    end
    if g == 2
        figure(3);
        set(gcf,'Position',[274 586 1239 358])
        save2pdf('./figures/figureS5b')
        close all
    elseif g == 3
        figure(3);
        set(gcf,'Position',[274 586 1239 358])
        save2pdf('./figures/figureS5c')

        figure(4);
        set(gcf,'Position',[1151 374 686 474])
        save2pdf('./figures/figureS5c-inlay')
        close all
    end
end

 close all

% END OF SCRIPT