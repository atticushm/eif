function BLMConvergenceTesting(source)

% BLM Convergence Testing.
% Here we compare the BLM for increasing number of points N against the
% hyperdiffusion equation for a low amplitude relaxing filament, solved
% used a finite difference scheme.

close all

switch source
    case 'local'
        disp('Using local data...')
    case 'online'
        disp('Downloading online data...')
        url = 'http://edata.bham.ac.uk/409/7/figureS9-data.zip';
        websave('./workspaces/figureS9-data.zip',url);
        unzip('./workspaces/figureS9-data.zip','./workspaces')
        
        disp('Download complete!')
end

%% Simulations

tmax  = 5e-2;

if ~exist('./workspaces/figureS9-data-fdm.mat','file')==1
    
    % FDM solutions:
    xMin    = 0;
    xMax    = 1;
    dx      = 2e-3;
    x       = xMin:dx:xMax;
    Nx      = length(x);

    dt      = 1e-6;
    t       = 0:dt:tmax;
    Nt      = length(t);
    
    % Parabolic initial condition:
    y(:,1)  = 0.5 * (x-0.5).^2;

    % Setup finite difference scheme matrix of coefficients:
    M   = zeros(Nx);
    qq  = 1/2;
    sig = (2*log(2*qq/0.01) + 0.5)*(dt/(dx^4)); 

    R1  = horzcat((1+2*sig), -4*sig, +2*sig, zeros(1,Nx-3));
    R2  = horzcat(-2*sig, (1+5*sig), -4*sig, +1*sig, zeros(1,Nx-4));
    RN1 = horzcat(zeros(1,Nx-4), 1*sig, -4*sig, (1+5*sig), -2*sig);
    RN  = horzcat(zeros(1,Nx-3), 2*sig, -4*sig, (1+2*sig));

    M   = diag((1+6*sig).*ones(1,Nx)) + diag(-4*sig.*ones(1,Nx-1),-1) + diag(1*sig.*ones(1,Nx-2),-2) + diag(-4*sig.*ones(1,Nx-1),1) + diag(1*sig.*ones(1,Nx-2),2);

    M(1,:)      = R1;
    M(2,:)      = R2;
    M(Nx-1,:)   = RN1;
    M(Nx,:)     = RN;
    M           = sparse(M);
    
    fprintf('Computing hyperdiff eq solutions... ')
    for n = 1:Nt
        fprintf('[%g/%g]\n',n,Nt)
        
        % Solve system iteratively using the backslash command:
        y(:,n+1) = M\y(:,n);
    end
    fprintf('complete!')
    save('./workspaces/figureS9-data-fdm.mat')
else
    load('./workspaces/figureS9-data-fdm','x','y','t')
end

if ~exist('./workspaces/figureS9-data-blm.mat','file')==1
    
    % BLM solutions
    Q       = ceil(logspace(1,2.2,20));
    NQ      = length(Q);
    grid    = [1,1];
    sep     = [0,0];
    
    for q = 1:NQ
        fprintf('[%g,%g]',q,NQ)
        model = CreateModelStruct(Q(q),grid,sep,'relaxing',5e4,1);
        Y0 = GetParabolicIntCond(0.5, model);
        [blm(q).sol,blm(q).tps,blm(q).crds] = FilamentsRelaxingFunctionBLM(model, Y0, tmax);
        Ntps    = length(blm(q).tps);
        for n = 1:Ntps
            blm(q).com(n).x = mean(blm(q).crds(n).x);
            blm(q).com(n).y = mean(blm(q).crds(n).y);
        end
    end
    save('./workspaces/figureS9-data-blm.mat')
else
    load('./workspaces/figureS9-data-blm.mat','blm','Q')
end

%% Calculate CoM errors

% Find FDM solutions at tps points
tps     = blm(1).tps;
Ntps    = length(tps);
for n = 1:Ntps
    tp      = tps(n);
    idx(n)  = find(abs(t-tp)<1e-10);
    Tp(n)   = t(idx(n));
end

N   = Q+1;
NN  = length(N);
NQ  = length(Q);
for q = 1:NQ
    for n = 1:Ntps
        yy      = y(:,idx(n));
        X       = [blm(q).com(n).x;blm(q).com(n).y];
        Y       = [0.5;mean(yy)];
        E(q,n)  = sqrt((1/N(q))*sum(abs(X-Y).^2));
    end
end

%% Plots


figure;

q = 5;
cmap = parula(6);

% (a) Config plot
% BLM w/ Q=40
subplot(2,4,1);
box on; hold on;
plot(blm(q).crds(1).x,blm(q).crds(1).y,'b','LineWidth',2,'Color',cmap(2,:));
plot(blm(q).crds(end).x,blm(q).crds(end).y,'r','LineWidth',2,'Color',cmap(4,:));
ax = gca;
ax.FontSize             = 16;
ax.TickLabelInterpreter = 'latex';
ax.LineWidth            = 1.5;
ax.YAxis.Exponent       = -2;
ax.Layer                = 'top';
xticks([0 1]);
yticks([0 15e-2]);
xlabel('$x$','Interpreter','latex','FontSize',16')
ylabel('$y$','Interpreter','latex','FontSize',16')
title('BLM','Interpreter','latex','FontSize',16')
% axis([-0.6 0.6 -1e-2 15e-2])
axis([-0.1 1.1 -1e-2 15e-2])
lgd = legend('$t=0$','$t=0.05$');
lgd.Interpreter = 'latex';
lgd.Location    = 'north';
lgd.EdgeColor   = 'none';

% (b) PDE
subplot(2,4,2);
box on; hold on;
plot(x,y(:,1),'b','LineWidth',2,'Color',cmap(2,:));
plot(x,y(:,end),'r','LineWidth',2,'Color',cmap(4,:));
ax = gca;
ax.FontSize             = 16;
ax.TickLabelInterpreter = 'latex';
ax.LineWidth            = 1.5;
ax.YAxis.Exponent       = -2;
ax.Layer                = 'top';
xticks([0 1]);
yticks([0 15e-2]);
xlabel('$x$','Interpreter','latex','FontSize',16')
ylabel('$y$','Interpreter','latex','FontSize',16')
title('PDE','Interpreter','latex','FontSize',16')
% axis([-0.6 0.6 -1e-2 15e-2])
axis([-0.1 1.1 -1e-2 15e-2])
lgd = legend('$t=0$','$t=0.05$');
lgd.Interpreter = 'latex';
lgd.Location    = 'north';
lgd.EdgeColor   = 'none';

% (c) CoM error over t
subplot(2,4,[5 6])
box on; hold on;
ax = gca;
ax.FontSize             = 16;
ax.TickLabelInterpreter = 'latex';
ax.LineWidth            = 1.5;
ax.XAxis.Exponent       = -2;
ax.Layer                = 'top';
xticks(1e-2*[0 2.5 5])
xlabel('$t$','Interpreter','latex','FontSize',16')
ylabel('$\textup{RMSE}_c$','Interpreter','latex','FontSize',16)
plot(tps,E(q,:),'r','LineWidth',1.8)

% (d) CoM RMS plot
subplot(2,4,[3 4 7 8])
box on; hold on;
plot(N,E(:,end),'rs-','LineWidth',1.8)

xlabel('$N$','Interpreter','latex','FontSize',16)
ylabel('$\textup{RMSE}_c \textup{ at } t=0.05$','Interpreter','latex','FontSize',16)
ax = gca;
ax.FontSize             = 16;
ax.TickLabelInterpreter = 'latex';
ax.LineWidth            = 1.5;
ax.XScale               = 'log';
ax.XLim                 = [10^1 10^2.3];
ax.YAxisLocation        = 'right';
ax.YScale               = 'log';
ax.YLim                 = [10^-4 10^-2];
ax.XLim                 = [8.1392 219.0685];
ax.YLim                 = [8.1350e-05 0.0129];

set(gcf,'Position',[422 263 1218 563])
save2pdf('./figures/figureS9.pdf')
close all


end