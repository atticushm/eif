% Sedimenting filament
% More detailed convergence testing
% * We previously have found that convergence testing for the sedimenting
% filament experiment is troublesome due to the nature of the instability
% that develops as the fibre sediments, which is tied to the level of
% discretisation. The filament can `fall' left or right.
% * In this function we force the direction of falling by initialising the
% filament at a non-zero angle to the horizontal.

close all;

Q   = [5,10,20,40,80,160];
NQ  = length(Q);

G   = [2500,3000,3500];
NG  = length(G);

arr = [1,1];
sep = [0,0];

cmap    = parula(NQ);

%% Simulations

clear sol tps crds

if ~exist('./workspaces/figureS6-data.mat','file')
    for q = 1:NQ
        for g = 1:NG
            fprintf('[se,%g/%g,%g/%g]',q,NQ,g,NG)
            model = CreateModelStruct(Q(q),arr,sep,'sedimenting',G(g));
            model_ps = CreateModelStruct(10,arr,sep,'sedimenting',G(g));
            model.phi = 0.001*pi;
            model_ps.phi = 0.001*pi;
            Y0 = GetParabolicIntCond(1e-7, model_ps);
            [sol(q).se(g).data,tps(q).se(g).data,crds(q).se(g).data] = FilamentsSedimentingFunction(model, Y0, 9e-3);
        end
    end
    save('./workspaces/figureS6-data.mat')
else
    load('./workspaces/figureS6-data.mat')
end
t       = tps(1).se(1).data;
Ntps    = length(t);

for g = 1:NG
    for q = 1:NQ-1

        X   = crds(q).se(g).data;
        Y   = crds(q+1).se(g).data; 
        
        Xth = sol(q).se(g).data(3:end,:);
        Yth = sol(q+1).se(g).data(3:end,:);

        for n = 1:Ntps
            Xp      = [X(n).x(:); X(n).y(:)];
            Yp      = [Y(n).x(1:2:end); Y(n).y(1:2:end)];
            
            Xthp    = 0.5*(Xth(2:end,n)+Xth(1:end-1,n)); % interior point angles
            Ythm    = 0.5*(Yth(2:end,n)+Yth(1:end-1,n));
            
            step    = Q(q+1)/Q(q);
            idx     = (step):step:Q(q+1)-1;
            Ythp    = Ythm(idx);

            E(q).se(n,g)    = sqrt((1/Q(q))*sum(abs(Xp-Yp).^2));
            Eth(q).se(n,g)  = sqrt((1/Q(q))*sum(abs(Xthp-Ythp).^2));
        end
    end
end

%% Plots

figure; 
for g = 1:NG
    
    % Coordinate error
    figure(1); 
    subplot(1,NG,g); hold on; box on;
    xlabel('$t$','Interpreter','latex','FontSize',16)
    ylabel('RMSE','Interpreter','latex','FontSize',16)
    title(sprintf('$\\mathcal{G}=%g$',G(g)),'Interpreter','latex','FontSize',16)
    for q = 1:NQ-1
        plot(t,E(q).se(:,g),'LineWidth',1.5,'Color',cmap(q,:))
    end
    legend('5v10','10v20','20v40','40v80','80v160','Location','southeast','NumColumns',2,'Interpreter','latex','FontSize',15,'EdgeColor','none','Color','none') 
    set(gca,'TickLabelInterpreter','latex','LineWidth',1.1,'FontSize',16)
    ax1 = gca;
    ax1.Layer                   = 'Top';
    ax1.YScale                  = 'log';
    ax1.TickLabelInterpreter    = 'latex';
    ax1.LineWidth               = 1.1;
    ax1.FontSize                = 15;
    ax1.XLim                    = [0 0.00901];
    ax1.XAxis.Exponent          = -3;
    ax1.YLim                    = [10^-6 10^1];
    xticks(1e-3*[0 3 6 9])
    yticks([10^-6 10^-3 10^0])
    axis square
    
    % Tangent angle error
    %{
    figure(2);
    subplot(1,NG,g); hold on; box on;
    xlabel('$t$','Interpreter','latex','FontSize',16)
    ylabel('Angle RMS','Interpreter','latex','FontSize',16)
    title(sprintf('$\\mathcal{G}=%g$',G(g)),'Interpreter','latex','FontSize',16)
    for q = 2:NQ-1
        plot(t,Eth(q).se(:,g),'LineWidth',1.5)
    end
    legend('10v20','20v40','40v80','80v160',...
        'Location','southoutside','NumColumns',3,'Interpreter','latex','FontSize',15,'EdgeColor','none')
    set(gca,'TickLabelInterpreter','latex','LineWidth',1.1,'FontSize',16)
    ax2 = gca;
    ax2.XLim = [0 0.00901];
    ax2.XAxis.Exponent = -3;
    if g == 1
        ax2.YLim = [0 0.08];
        yticks([0 0.04 0.08]);
        ax2.YAxis.Exponent = -2;
    elseif g == 2 
        ax2.YLim = [0 0.4];
        yticks([0 0.2 0.4]);
        ax2.YAxis.Exponent = -1;
    elseif g == 3
        ax2.YLim = [0 0.3];
        yticks([0 0.15 0.3]);
        ax2.YAxis.Exponent = -1;
    end
    %}
end

figure(1); 
set(gcf,'Position',[274 574 1204 363])
save2pdf('./figures/figureS6a')

% figure(2); 
% set(gcf,'Position',[228 45 1519 403])
% save2pdf('./figures/sedimenting-bias-th-error')

% Config plot

sol20   = sol(3).se;
sol40   = sol(4).se;
sol80   = sol(5).se;
sol160  = sol(6).se;

crds20  = crds(3).se;
crds40  = crds(4).se;
crds80  = crds(5).se;
crds160 = crds(6).se;

for g = 3
    Gp = G(g);
    for i = 1:Ntps
%         tsave       = [1.5e-3 2.5e-3 3.5e-3];
        tsave = [1.5e-3, 5.5e-3, 8.5e-3];
        [~,~,fil,~] = GetFilamentData(sol80(g).data(:,i),1,Gp);
        [check,idx] = max(abs(t(i)-tsave)<4e-5);
        if check == 1
            figure(3);
            subplot(1,length(tsave),idx); hold on; box on;
            ax      = gca;
            ax.TickLabelInterpreter = 'latex';
            ax.LineWidth            = 1.1;
            ax.FontSize             = 15;
            ax.Layer                = 'top';
            
            axis equal
            if idx == 1
                ax.XLim = [0 1];
                ax.YLim = [-3.1 -2.1];
                xticks(ax.XLim);
                yticks(ax.YLim);
            elseif idx == 2
                ax.XLim = [0 1];
                ax.YLim = [-10 -9];
                xticks(ax.XLim);
                yticks(ax.YLim);
            elseif idx == 3
                ax.XLim = [0.2 1.2];
                ax.YLim = [-16.4 -15.4];
                xticks(ax.XLim);
                yticks(ax.YLim);
            end
            axis square
            xlabel('$x$','Interpreter','latex')
            ylabel('$y$','Interpreter','latex')
            
            plot(crds20(g).data(i).x,crds20(g).data(i).y,'LineWidth',2,'Color',cmap(2,:))
            plot(crds40(g).data(i).x,crds40(g).data(i).y,'LineWidth',2,'Color',cmap(3,:))
            plot(crds80(g).data(i).x,crds80(g).data(i).y,'LineWidth',2,'Color',cmap(4,:))
            plot(crds160(g).data(i).x,crds160(g).data(i).y,'LineWidth',2,'Color',cmap(5,:))
            
            title(sprintf('$\\mathcal{G}=%g,\\,t= %g$',Gp,round(t(i),4)),'Interpreter','latex','FontSize',16)
            if  idx == 1
                legend( '$Q=20$','$Q=40$','$Q=80$','$Q=160$','Location','northwest','NumColumns',3,'Interpreter','latex','FontSize',15,'EdgeColor','none')
            end
            
            % Inlays
            figure(4);
            subplot(1,length(tsave),idx); hold on; box on;
            ax = gca;
            ax.TickLabelInterpreter = 'latex';
            ax.LineWidth            = 1.3;
            ax.Layer                = 'top';
            
            plot(crds20(g).data(i).x,crds20(g).data(i).y,'LineWidth',2,'Color',cmap(2,:))
            plot(crds40(g).data(i).x,crds40(g).data(i).y,'LineWidth',2,'Color',cmap(3,:))
            plot(crds80(g).data(i).x,crds80(g).data(i).y,'LineWidth',2,'Color',cmap(4,:))
            plot(crds160(g).data(i).x,crds160(g).data(i).y,'LineWidth',2,'Color',cmap(5,:))
            
            axis square
            xticks([]);
            yticks([]);
            if idx == 1
                axis([0.748085046853288 0.791141811166712 -2.491191641038688 -2.448134876725264])
            elseif idx == 2
                axis([0.607561632411535 0.825190768201683 -9.732521770527953 -9.514892634737805])
            elseif idx == 3
                axis([0.554561239172856 0.873192056883212 -16.166879358120646 -15.848248540410289])
            end
            
        end
    end
end

figure(3);
set(gcf,'Position',[274 574 1204 363])
save2pdf('./figures/figureS6b')

figure(4);
set(gcf,'Position',[1151 374 686 474])
save2pdf('./figures/figureS6b-inlay')

close all; 

% END OF SCRIPT