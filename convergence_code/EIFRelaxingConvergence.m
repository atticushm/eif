close all; clear variables

Q   = [5,10,20,40,80,160];
NQ  = length(Q);

arr = [1,1];
sep = [0,0];

if ~exist('./workspaces/figureS1-data.mat','file')
    for q = 1:NQ
        fprintf('[re,%g/%g]',q,NQ)
        model                                           = CreateModelStruct(Q(q),arr,sep,'relaxing',5e4);
        Y0                                              = GetParabolicIntCond(0.5, model, 'scale');
        [sol(q).re.data,tps(q).re.data,crds(q).re.data] = FilamentsRelaxingFunction(model, Y0, 2e-2);      
    end
    save('./workspaces/figureS1-data.mat','Q','sol','tps','crds')
else
    load('./workspaces/figureS1-data.mat','Q','sol','tps','crds')
end
Npair   = NQ-1;
t       = tps(1).re.data;
Ntps    = length(t);

for k = 1:Npair

    X   = crds(k).re.data;          % lower resolution coordinate data.
    Y   = crds(k+1).re.data;        % higher reoslution coordinate data.
    
    Xth = sol(k).re.data(3:end,:);
    Yth = sol(k+1).re.data(3:end,:);

    for n = 1:Ntps
        Xp  = [X(n).x(:); X(n).y(:)];
        Yp  = [Y(n).x(1:2:end); Y(n).y(1:2:end)];

        Xthp    = 0.5*(Xth(2:end,n)+Xth(1:end-1,n)); 
        Ythm    = 0.5*(Yth(2:end,n)+Yth(1:end-1,n));

        step    = Q(k+1)/Q(k);
        idx     = (step):step:Q(k+1)-1;
        Ythp    = Ythm(idx);
        
        E(k).re(1,n)    = sqrt((1/Q(k))*sum(abs(Xp-Yp).^2)); 
        Eth(k).re(1,n)  = sqrt((1/Q(k))*sum(abs(Xthp-Ythp).^2));
    end
end

% Coordinate error

figure(1); 
hold on; 
box on;
xlabel('$t$','Interpreter','latex','FontSize',16)
ylabel('RMSE','Interpreter','latex','FontSize',16)
cmap = parula(NQ);
for q = 1:NQ-1
    plot(t,E(q).re(:),'LineWidth',1.5,'Color',cmap(q,:))
end
lgd = legend('5v10','10v20','20v40','40v80','80v160','Location','southeast','NumColumns',3,'Interpreter','latex','FontSize',15,'EdgeColor','none','Color','none');
xticks([0 0.01 0.02])
set(gca,'TickLabelInterpreter','latex','LineWidth',1.1,'YScale','log','Layer','Top')
ax                  = gca;
ax.XAxis.Exponent   = -2;
ax.FontSize         = 16;

figure(1); 
set(gcf,'Position',[225 609 452 405])
save2pdf('./figures/figureS1')

close all