%% EIF FILAMENT IN SHEAR CONVERGENCE

clear variables; close all;

Q   = [5,10,20,40,80,160];
NQ  = length(Q);

grid = [1,1];
sep  = [0,0];

%% LOW V TESTS
%  Simulation and error calcualtion

V   = [4e3 8e3 12e3 16e3];
NV  = length(V);

if ~exist('./workspaces/figureS234-data-a.mat','file')
    for q = 1:NQ
        for v = 1:NV
            fprintf('[sh,%g/%g,%g/%g]',q,NQ,v,NV)
            model = CreateModelStruct(Q(q),grid,sep,'shear',V(v),0.9*pi);
            dth0 = 0.1;
            Y0 = GetPerturbedIntCondYoung(model, dth0);
            [sol(q).sh(v).data,tps(q).sh(v).data,crds(q).sh(v).data] = FilamentsShearFunction(model, Y0, 5);
        end
    end
    save('./workspaces/figureS234-data-a.mat','Q','sol','tps','crds','V')
else
    load('./workspaces/figureS234-data-a.mat','Q','sol','tps','crds','V')
end
Npair   = NQ-1;
t       = tps(1).sh.data;
Ntps    = length(t);

for v = 1:NV
    for k = 1:Npair

        X   = crds(k).sh(v).data; 
        Y   = crds(k+1).sh(v).data; 
        
        Xth = sol(k).sh(v).data(3:end,:);
        Yth = sol(k+1).sh(v).data(3:end,:);

        for n = 1:Ntps
            Xp      = [X(n).x(:); X(n).y(:)];
            Yp      = [Y(n).x(1:2:end); Y(n).y(1:2:end)];

            Xthp    = 0.5*(Xth(2:end,n)+Xth(1:end-1,n)); 
            Ythm    = 0.5*(Yth(2:end,n)+Yth(1:end-1,n));

            step    = Q(k+1)/Q(k);
            idx     = (step):step:Q(k+1)-1;
            Ythp    = Ythm(idx);

            E(k).sh(n,v)    = sqrt((1/Q(k))*sum(abs(Xp-Yp).^2));
            Eth(k).sh(n,v)  = sqrt((1/Q(k))*sum(abs(Xthp-Ythp).^2));
        end
    end
end

% Plots
 
for v = 1:NV
    % Coordinate error
    figure(1); 
    subplot(1,NV,v); hold on; box on;
    xlabel('$t$','Interpreter','latex','FontSize',16)
    ylabel('RMSE','Interpreter','latex','FontSize',16)
    title(sprintf('$\\mathcal{V}=%g$',V(v)),'Interpreter','latex','FontSize',16)
    cmap = parula(NQ);
    for q = 1:NQ-1
        plot(t,E(q).sh(:,v),'LineWidth',1.5,'Color',cmap(q,:))
    end
    axis([0 t(end) 10^(-6) 10^0])
    legend('5v10','10v20','20v40','40v80','80v160','160v320','Location','southeast','NumColumns',2,'Interpreter','latex','FontSize',15,'EdgeColor','none','Color','none')
    ax = gca;
	ax.TickLabelInterpreter = 'latex';
    ax.LineWidth            = 1.1;
    ax.FontSize             = 16;
    ax.YScale               = 'log';
    ax.Layer                = 'Top';
    xticks([0 2.5 5])
    yticks([10^(-6) 10^(-3) 10^0])
    
    % Tangent angle error
    %{
    figure(2);
    subplot(1,NV,v); hold on; box on;
    xlabel('$t$','Interpreter','latex','FontSize',16)
    ylabel('Angle RMS','Interpreter','latex','FontSize',16)
    title(sprintf('$\\mathcal{V}=%g$',V(v)),'Interpreter','latex','FontSize',15)
    for q = 1:NQ-1
        plot(t,Eth(q).sh(:,v),'LineWidth',1.5)
    end
    axis([0 5 0 1.6])
    yticks([0 0.8 1.6])
    set(gca,'TickLabelInterpreter','latex','LineWidth',1.1,'FontSize',16)
    legend('5v10','10v20','20v40','40v80','80v160','160v320',...
       'Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',15,'EdgeColor','none')
    %}
end

figure(1); 
set(gcf,'Position',[268 480 1.7230e+03 400])
save2pdf('./figures/figureS2')

% figure(2); 
% set(gcf,'Position',[268 480 1474 431])
% save2pdf('./figures/supp-shear-th-error')

sol20   = sol(3).sh;    crds20  = crds(3).sh;
sol40   = sol(4).sh;    crds40  = crds(4).sh;
sol80   = sol(5).sh;    crds80  = crds(5).sh;
sol160  = sol(6).sh;    crds160 = crds(6).sh;

% Overlaid shape plots
figure(3);
% figure(4);

k = 1;
for v = 1:2
    if v == 1
        Vp = V(1);
    elseif v == 2
        Vp = V(4);
    end
    for i = 1:Ntps
        tsave       = [2.2, 3.3];
        [~,~,fil,~] = GetFilamentData(sol40(v).data(:,i),1,Vp);
        [check,idx] = max(abs(t(i)-tsave)<5e-5);
        if check == 1
            
            % Shape comparison
            figure(3);
            if v == 1
                subplot(3,2,idx)
            elseif v == 2
                subplot(3,2,idx+2)
            end
            hold on; box on;
            axSize      = GetAxes(fil,grid,0.9);
            axis(axSize)
            
            plot(crds20(v).data(i).x,crds20(v).data(i).y,'LineWidth',2,'Color',cmap(2,:));
            plot(crds40(v).data(i).x,crds40(v).data(i).y,'LineWidth',2,'Color',cmap(3,:));
            plot(crds80(v).data(i).x,crds80(v).data(i).y,'LineWidth',2,'Color',cmap(4,:));
            uistack(plot(crds160(v).data(i).x,crds160(v).data(i).y,'LineWidth',2,'Color',cmap(5,:)),'bottom');
            
            xlabel('$x$','Interpreter','latex','FontSize',16)
            ylabel('$y$','Interpreter','latex','FontSize',16)
            
            xticks([-0.5 0.5])
            yticks([-0.5 0.5]) 
            
            title(sprintf('$\\mathcal{V}=%g,\\, t=%g$',Vp,t(i)),'Interpreter','latex','FontSize',16)
            ax  = gca;
            ax.TickLabelInterpreter = 'latex';
            ax.LineWidth            = 1.1;
            ax.FontSize             = 15;
            ax.Layer                = 'Top';
            axis([-0.5 0.5 -0.5 0.5])
            axis square
            
            if idx == 1 && v == 2 % inlay 1
                figure(3);
                subplot(3,2,5); box on; hold on;
                p1 = plot(crds20(v).data(i).x,crds20(v).data(i).y,'LineWidth',2,'Color',cmap(2,:));
                p2 = plot(crds40(v).data(i).x,crds40(v).data(i).y,'LineWidth',2,'Color',cmap(3,:));
                p3 = plot(crds80(v).data(i).x,crds80(v).data(i).y,'LineWidth',2,'Color',cmap(4,:));
                p4 = plot(crds160(v).data(i).x,crds160(v).data(i).y,'LineWidth',2,'Color',cmap(5,:));
                uistack(p4,'bottom')
                
                xticks([])
                yticks([])
                axis equal
                ax              = gca;
                ax.LineWidth    = 1.4;
                ax.Layer        = 'Top';
                axis([-0.0308 0.0773 -0.0402 0.0679])
                axis square
                lgd  = legend([p1 p2 p3 p4],{'$Q=20$','$Q=40$','$Q=80$','$Q=160$'},'Interpreter','latex','FontSize',15);
                lgd.Location = 'southoutside';
                lgd.NumColumns = 2;
                lgd.EdgeColor = 'none';

            elseif idx == 2 && v ==2 % inlay 2
                figure(3);
                subplot(3,2,6); box on; hold on;
                p1 = plot(crds20(v).data(i).x,crds20(v).data(i).y,'LineWidth',2,'Color',cmap(2,:));
                p2 = plot(crds40(v).data(i).x,crds40(v).data(i).y,'LineWidth',2,'Color',cmap(3,:));
                p3 = plot(crds80(v).data(i).x,crds80(v).data(i).y,'LineWidth',2,'Color',cmap(4,:));
                p4 = plot(crds160(v).data(i).x,crds160(v).data(i).y,'LineWidth',2,'Color',cmap(5,:));
                uistack(p4,'bottom')

                xticks([])
                yticks([])
                axis equal
                ax              = gca;
                ax.LineWidth    = 1.4;
                ax.Layer        = 'Top';
                axis([0.0572 0.1145 0.3956 0.4529])
                axis square
                lgd  = legend([p1 p2 p3 p4],{'$Q=20$','$Q=40$','$Q=80$','$Q=160$'},'Interpreter','latex','FontSize',15);
                lgd.Location = 'southoutside';
                lgd.NumColumns = 2;
                lgd.EdgeColor = 'none';
            end
            
            % Angle comparison   
            %{
            figure(4);
            if v == 1
                subplot(2,2,idx)
            elseif v == 2
                subplot(2,2,idx+2)
            end
            hold on; box on;
            
            s20 = 0:(1/20):1; sm20 = 0.5.*(s20(1:end-1)+s20(2:end)); 
            s40 = 0:(1/40):1; sm40 = 0.5.*(s40(1:end-1)+s40(2:end)); 
            s80 = 0:(1/80):1; sm80 = 0.5.*(s80(1:end-1)+s80(2:end)); 
            s160= 0:(1/160):1;sm160= 0.5.*(s160(1:end-1)+s160(2:end));
            
            thb20   = sol20(v).data(3:end,i)-mean(sol20(v).data(3:end,i));
            thb40   = sol40(v).data(3:end,i)-mean(sol40(v).data(3:end,i));
            thb80   = sol80(v).data(3:end,i)-mean(sol80(v).data(3:end,i));
            thb160  = sol160(v).data(3:end,i)-mean(sol160(v).data(3:end,i));
           
            plot(sm20,thb20,'LineWidth',1.5);
            plot(sm40,thb40,'.-','LineWidth',1.5);
            plot(sm80,thb80,'--','LineWidth',1.5);
            plot(sm160,thb160,':','LineWidth',1.5);
            
            if v == 2
                xlabel('$\tilde{s}$','Interpreter','latex','FontSize',16)
            end
            if idx == 1    
                ylabel('$\tilde{\theta}_\textup{body}$','Interpreter','latex','FontSize',16)
            end
            
            xticks([0 1])
           
            lgd  = legend('$Q=20$','$Q=40$','$Q=80$','$Q=160$','Interpreter','latex','FontSize',15);
            lgd.Location = 'south';
            lgd.NumColumns = 2;
            lgd.EdgeColor = 'none';
            title(sprintf('$\\mathcal{V}=%g, \\, t=%g$',Vp,t(i)),'Interpreter','latex','FontSize',16)
            set(gca,'TickLabelInterpreter','Latex','LineWidth',1.5,'FontSize',15)
            axis([0 1  -pi pi])
            %}
        end
    end
    k = k+1;
end

figure(3);
pos = gcf;
set(gcf,'Position',[1145 40 586 919])
save2pdf('./figures/figureS3')

% figure(4);
% pos = gcf;
% set(gcf,'Position',[350 327 783 580])
% save2pdf('./figures/supp-shear-th')

close all

% END OF SCRIPT

%% HIGH V TESTS
%  Simulation and error calcualtion

Q   = [5,10,20,40,80,160,320];
NQ  = length(Q);

grid = [1,1];
sep  = [0,0];

V   = [4e4];
NV  = length(V);

if ~exist('./workspaces/figureS234-data-b.mat','file')
    for q = 1:NQ
        for v = 1:NV
            fprintf('[sh,%g/%g,%g/%g]',q,NQ,v,NV)
            model = CreateModelStruct(Q(q),grid,sep,'shear',V(v),0.9*pi);
            dth0 = 0.1;
            Y0 = GetPerturbedIntCondYoung(model, dth0);
            [sol(q).sh(v).data,tps(q).sh(v).data,crds(q).sh(v).data] = FilamentsShearFunction(model, Y0, 5);
        end
    end
    save('./workspaces/figureS234-data-b.mat','Q','sol','tps','crds','V')
else
    load('./workspaces/figureS234-data-b.mat','Q','sol','tps','crds','V')
end
Npair   = NQ-1;
t       = tps(1).sh.data;
Ntps    = length(t);

for v = 1:NV
    for k = 1:Npair

        X   = crds(k).sh(v).data; 
        Y   = crds(k+1).sh(v).data; 
        
        Xth = sol(k).sh(v).data(3:end,:);
        Yth = sol(k+1).sh(v).data(3:end,:);

        for n = 1:Ntps
            Xp      = [X(n).x(:); X(n).y(:)];
            Yp      = [Y(n).x(1:2:end); Y(n).y(1:2:end)];

            Xthp    = 0.5*(Xth(2:end,n)+Xth(1:end-1,n)); 
            Ythm    = 0.5*(Yth(2:end,n)+Yth(1:end-1,n));

            step    = Q(k+1)/Q(k);
            idx     = (step):step:Q(k+1)-1;
            Ythp    = Ythm(idx);

            E(k).sh(n,v)    = sqrt((1/Q(k))*sum(abs(Xp-Yp).^2));
            Eth(k).sh(n,v)  = sqrt((1/Q(k))*sum(abs(Xthp-Ythp).^2));
        end
    end
end

% Plots
 
for v = 1:NV
    %{
    % Coordinate error
    figure(1); 
    subplot(1,NV,v); hold on; box on;
    xlabel('$t$','Interpreter','latex','FontSize',16)
    ylabel('Coordinate RMS','Interpreter','latex','FontSize',16)
    title(sprintf('$\\mathcal{V}=%g$',V(v)),'Interpreter','latex','FontSize',16)
    for q = 1:NQ-1
        plot(t,E(q).sh(:,v),'LineWidth',1.5)
    end
    axis([0 t(end) 0 0.21])
    set(gca,'TickLabelInterpreter','latex','LineWidth',1.1,'FontSize',16)
    legend('5v10','10v20','20v40','40v80','80v160','160v320',...
       'Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',15,'EdgeColor','none')
    ax = gca;
    ax.YAxis.Exponent = -1;
    
    % Tangent angle error
    figure(2);
    subplot(1,NV,v); hold on; box on;
    xlabel('$t$','Interpreter','latex','FontSize',16)
    ylabel('Angle RMS','Interpreter','latex','FontSize',16)
    title(sprintf('$\\mathcal{V}=%g$',V(v)),'Interpreter','latex','FontSize',15)
    for q = 1:NQ-1
        plot(t,Eth(q).sh(:,v),'LineWidth',1.5)
    end
    axis([0 5 0 1.6])
    yticks([0 0.8 1.6])
    set(gca,'TickLabelInterpreter','latex','LineWidth',1.1,'FontSize',16)
    legend('5v10','10v20','20v40','40v80','80v160','160v320',...
       'Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',15,'EdgeColor','none')
    %}
end

% figure(1); 
% set(gcf,'Position',[268 480 1474 431])
% save2pdf('./figures/supp-shear-crd-error')
% 
% figure(2); 
% set(gcf,'Position',[268 480 1474 431])
% save2pdf('./figures/supp-shear-th-error')

sol20   = sol(3).sh;    crds20  = crds(3).sh;
sol40   = sol(4).sh;    crds40  = crds(4).sh;
sol80   = sol(5).sh;    crds80  = crds(5).sh;
sol160  = sol(6).sh;    crds160 = crds(6).sh;
sol320  = sol(7).sh;    crds320 = crds(7).sh;

% Overlaid shape plots
k = 1;
for v = 1
    Vp = V(v);
    for i = 1:Ntps
        tsave       = [2.2, 3.3];
        [~,~,fil,~] = GetFilamentData(sol40(v).data(:,i),1,Vp);
        [check,idx] = max(abs(t(i)-tsave)<5e-5);
        if check == 1
            
            % Shape comparison
            figure(3);
            subplot(2,2,idx)
            hold on; box on;
            axSize      = GetAxes(fil,grid,0.9);
            axis(axSize)
            
            plot(crds40(v).data(i).x,crds40(v).data(i).y,'LineWidth',2,'Color',cmap(3,:));
            plot(crds80(v).data(i).x,crds80(v).data(i).y,'LineWidth',2,'Color',cmap(4,:));
            plot(crds160(v).data(i).x,crds160(v).data(i).y,'LineWidth',2,'Color',cmap(5,:));
            uistack(plot(crds320(v).data(i).x,crds320(v).data(i).y,'LineWidth',2,'Color',cmap(1,:)),'bottom');
            
            xlabel('$x$','Interpreter','latex','FontSize',16)
            ylabel('$y$','Interpreter','latex','FontSize',16)
            
            xticks([-0.5 0.5])
            yticks([-0.5 0.5]) 
            
            title(sprintf('$\\mathcal{V}=%g,\\, t=%g$',Vp,t(i)),'Interpreter','latex','FontSize',16)
            ax  = gca;
            ax.TickLabelInterpreter = 'latex';
            ax.LineWidth            = 1.1;
            ax.FontSize             = 15;
            ax.Layer                = 'Top';
            axis([-0.5 0.5 -0.5 0.5])
            axis square
            
            % Shape inlays
            if idx == 1
                figure(4);
                subplot(1,2,1); hold on; box on;
                plot(crds40(v).data(i).x,crds40(v).data(i).y,'LineWidth',2,'Color',cmap(3,:));
                plot(crds80(v).data(i).x,crds80(v).data(i).y,'LineWidth',2,'Color',cmap(4,:));
                plot(crds160(v).data(i).x,crds160(v).data(i).y,'LineWidth',2,'Color',cmap(5,:));
                uistack(plot(crds320(v).data(i).x,crds320(v).data(i).y,'LineWidth',2,'Color',cmap(1,:)),'bottom');
                
                xticks([]); yticks([])
                axis([-0.0581 0.0647 -0.0579 0.0650])
                ax  = gca;
                ax.LineWidth    = 1.3;
                ax.Layer        = 'Top';
            elseif idx == 2
                figure(4);
                subplot(1,2,2); hold on; box on;
                plot(crds40(v).data(i).x,crds40(v).data(i).y,'LineWidth',2,'Color',cmap(3,:));
                plot(crds80(v).data(i).x,crds80(v).data(i).y,'LineWidth',2,'Color',cmap(4,:));
                plot(crds160(v).data(i).x,crds160(v).data(i).y,'LineWidth',2,'Color',cmap(5,:));
                uistack(plot(crds320(v).data(i).x,crds320(v).data(i).y,'-','LineWidth',2,'Color',cmap(1,:)),'bottom');
                
                xticks([]); yticks([])
                axis([-0.0591 0.0761 -0.0621 0.0730])
                ax  = gca;
                ax.LineWidth    = 1.3;
                ax.Layer        = 'Top';
            end
            
            % Angle comparson 
            figure(3);
            subplot(2,2,idx+2)
            hold on; box on;
            
            s20 = 0:(1/20):1; sm20 = 0.5.*(s20(1:end-1)+s20(2:end)); 
            s40 = 0:(1/40):1; sm40 = 0.5.*(s40(1:end-1)+s40(2:end)); 
            s80 = 0:(1/80):1; sm80 = 0.5.*(s80(1:end-1)+s80(2:end)); 
            s160= 0:(1/160):1;sm160= 0.5.*(s160(1:end-1)+s160(2:end));
            s320= 0:(1/320):1;sm320= 0.5.*(s320(1:end-1)+s320(2:end));
            
            thb20   = sol20(v).data(3:end,i)-mean(sol20(v).data(3:end,i));
            thb40   = sol40(v).data(3:end,i)-mean(sol40(v).data(3:end,i));
            thb80   = sol80(v).data(3:end,i)-mean(sol80(v).data(3:end,i));
            thb160  = sol160(v).data(3:end,i)-mean(sol160(v).data(3:end,i));
            thb320  = sol320(v).data(3:end,i)-mean(sol320(v).data(3:end,i));
            
            plot(sm40,thb40,'LineWidth',1.5,'Color',cmap(3,:));
            plot(sm80,thb80,'LineWidth',1.5,'Color',cmap(4,:));
            plot(sm160,thb160,'LineWidth',1.5,'Color',cmap(5,:));
            plot(sm320,thb320,'LineWidth',1.5,'Color',cmap(1,:));
            
            xlabel('$\tilde{s}$','Interpreter','latex','FontSize',16)    
            ylabel('$\tilde{\theta}_\textup{body}$','Interpreter','latex','FontSize',16)
            
            xticks([0 1])
           
            lgd  = legend('$Q=40$','$Q=80$','$Q=160$','$Q=320$','Interpreter','latex','FontSize',15);
            lgd.Location = 'south';
            lgd.NumColumns = 2;
            lgd.EdgeColor = 'none';
            title(sprintf('$\\mathcal{V}=%g, \\, t=%g$',Vp,t(i)),'Interpreter','latex','FontSize',16)
            ax  = gca;
            ax.TickLabelInterpreter = 'latex';
            ax.LineWidth            = 1.1;
            ax.FontSize             = 15;
            ax.Layer                = 'Top';
            axis([0 1  -pi pi])
            axis square
            
        end
    end
    k = k+1;
end

figure(3);
set(gcf,'Position',[680 175 920 803])
save2pdf('./figures/figureS4')

figure(4);
set(gcf,'Position',[680 428 365 147])
save2pdf('./figures/figureS4-inlay')

close all