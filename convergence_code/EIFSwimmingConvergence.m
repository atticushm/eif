% EIFSwimmingConvergence

close all; clear variables

Q   = [5,10,20,40,80,160];
NQ  = length(Q);

S   = [8,10,12];
NS  = length(S);

arr = [1,1];
sep = [0,0];

cmap    = parula(NQ);

if ~exist('./workspaces/figureS7-data.mat','file')
    for q = 1:NQ
        for s = 1:NS
            fprintf('[sw,%g/%g,%g/%g]',q,NQ,s,NS)
            model = CreateModelStruct(Q(q), arr, sep, 'swimming', S(s), 4*pi, 0.016);
            Y0 = GetParabolicIntCond(1e-3, model, 'scale');
            [data(q,s).sol, data(q,s).tps, data(q,s).crds] = FilamentsSwimmingFunction(model, Y0, 4*pi, 'sperm');
        end
    end
    save('./workspaces/figureS7-data.mat','Q','data','S')
else
    load('./workspaces/figureS7-data.mat','Q','data','S')
end
Npair   = NQ-1;
t       = data(1,1).tps;
Ntps    = length(t);
for s= 1:NS
    for k = 1:Npair
        
        X = data(k,s).crds;
        Y = data(k+1,s).crds;
        
        Xth = data(k,s).sol(3:end);
        Yth = data(k+1,s).sol(3:end);
        
        for n = 1:Ntps

            Xp      = [X(n).x(:); X(n).y(:)];
            Yp      = [Y(n).x(1:2:end); Y(n).y(1:2:end)];

            E(k).sw(n,s)    = sqrt((1/Q(k))*sum(abs(Xp-Yp).^2));

        end
    end
end

figure; 

for s = 1:NS
    % Coordinate error
    figure(1); 
    subplot(1,NS,s); hold on; box on;
    xlabel('$t$','Interpreter','latex','FontSize',16)
    ylabel('RMSE','Interpreter','latex','FontSize',16)
    title(sprintf('$\\mathcal{S}=%g$',S(s)),'Interpreter','latex','FontSize',16)
    for q = 1:NQ-1
        plot(t,E(q).sw(:,s),'LineWidth',1.5,'Color',cmap(q,:))
    end
    legend('5v10','10v20','20v40','40v80','80v160','Location','north','NumColumns',2,'Interpreter','latex','FontSize',15,'EdgeColor','none','Color','none')
    ax = gca;
    ax.XLim                 = [0 4*pi];
    ax.XTickLabels          = {'0','$2\pi$','$4\pi$'};
    ax.TickLabelInterpreter = 'latex';
    ax.LineWidth            = 1.1;
    ax.FontSize             = 15;
    ax.Layer                = 'Top';
    ax.YScale               = 'log';
    ax.YLim                 = [10^-4 10^0];
    xticks([0 2*pi 4*pi])
    yticks([10^-4 10^-2 10^0])
end

figure(1); 
set(gcf,'Position',[274 574 1204 363])
save2pdf('./figures/figureS7')

close all

% END OF SCRIPT