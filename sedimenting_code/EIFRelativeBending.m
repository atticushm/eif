function EIFRelativeBending(source)

% Sedimenting A/L vs G 

switch source
    case 'local'
        disp('Using local data...')
    case 'online'
        disp('Downloading online data...')
        url = 'http://edata.bham.ac.uk/409/10/figureS10-data.zip';
        websave('./workspaces/figureS10-data.zip',url);
        unzip('./workspaces/figureS10-data.zip','./workspaces')

        disp('Download complete!')
end

close all
G = round(logspace(0,4,20),4);
Q = 40;

%% EIF 

if ~exist('./workspaces/figureS10-data-eif.mat','file') == 1
    for g = 1:length(G)
        fprintf('[%g,%g]',g,length(G))
        arr     = [1,1];
        sep     = [0,0];
        model   = CreateModelStruct(Q,arr,sep,'sedimenting',G(g));
        model_ps = CreateModelStruct(10,arr,sep,'sedimenting',G(g));
        model.tol = 0.005;
        model.eps = 1/(Q*(100/Q));
        model_ps.tol = model.tol;
        model_ps.eps = model.eps;
        Y0 = GetParabolicIntCond(1e-3,model_ps);

        [~,~,crds]  = FilamentsSedimentingFunction(model, Y0, 20e-3);
        deflection_EIF(g)= max(crds(end).y)-min(crds(end).y);
    end
    save('./workspaces/figureS10-data-eif.mat')
else
    load('./workspaces/figureS10-data-eif.mat')
end


%% Schoeller et al. (2019)

if ~exist('./workspaces/figureS10-data-bmi.mat','file') == 1
    for g = 1:length(G)
        deflection_BMI(g)= BeadModelImperialMain(G(g),Q,false);
    end
    save('./workspaces/figureS10-data-bmi.mat')
else
    load('./workspaces/figureS10-data-bmi.mat')
end

%% Plots

fig = figure; 
hold on; box on; grid off;
ax                      = gca;
ax.LineWidth            = 1.1;
ax.TickLabelInterpreter = 'latex';
ax.FontSize             = 16;
ax.XScale               = 'log';
ax.YScale               = 'log';
ax.XAxisLocation        = 'origin';
ax.YAxisLocation        = 'origin';
ax.Layer                = 'top';

cmap = parula(6);

uistack(loglog(G,deflection_EIF,'LineWidth',2,'Color',cmap(2,:)),'top')
loglog(G,deflection_BMI,'--','LineWidth',2,'Color',cmap(5,:))

xlabel('$\mathcal{G}$','Interpreter','latex','FontSize',16)
ylabel('$A/L$','Interpreter','latex','FontSize',16)

xticks([10^0,10^2,10^4])
yticks([10^(-3),10^(-2),10^(-1),10^0])

lgd = legend('EIF','STWK');
lgd.Interpreter     = 'latex';
lgd.FontSize        = 15;
lgd.Location        = 'southeast';
lgd.EdgeColor       = 'none';

fig.Position        = [2717 396 388 281];

save2pdf('./figures/figureS10.pdf')
close all

end