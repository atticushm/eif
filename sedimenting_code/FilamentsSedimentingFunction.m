function [solutions,tps,coords] = FilamentsSedimentingFunction(model,Y0,tmax)

%% Filaments sedimenting

% Code by A.L. Hall-McNair, University of Birmingham (2018).
% Nfil filaments sedimenting using the method of lines w/ the method of
% regularized stokeslets, accounting for elastodynamics of the filaments
% and non-local hydrodynamic interactions between filaments.

% Inputs
% - model:      Structure created by CreateModelStruct() containing problem data, including G, the elasto-gravitational parameter.
% - Y0:         Initial condition (before pre-solve).
% - tmax:       Time at which to stop the simulation.

% Outputs
% - solutions: 	Solution data at time points tps.
% - tps:        Vector of time points at which solution is given.
% - coords:     Coordinate data of filament positions.

%% Global parameter setup

fprintf('\nSEDIMENTING FILAMENTS\n')
warning off
Q = model.Q; 

%% Pre-solve at low Q for better initial condition
%  High Q results in a very stiff initial system with the above
%  configuration.
%  To combat this we solve the problem with Q=10 and then interpolate to
%  get a better (and less stiff) initial condition for the higher Q
%  problem.

Y0_2 = ConstructInitialCondition(model,Y0);
    
%% Solve
%  An ode15s option "CheckFilOverlap" is created, which detects when two or
%  more filaments get close to each other. The current EIF method
%  implementation does not account for physical filament interactions, and
%  so the solve is stopped when this happens.
%  This option requires manually tweaking when Nfil changes, so is disabled
%  (commented out) by default.

fprintf('Solve started...\n')

options = odeset('Events',@CheckFilOverlap,'OutputFcn',@odetpbar);
dY_RSM  = @(t,Y) RatesRSMSedimenting(t,Y,model);
RSM_sol = ode15s(dY_RSM, [0 tmax], Y0_2, options);

roundval    = round(RSM_sol.x(end),3);
if roundval > RSM_sol.x(end)
    roundval = round(RSM_sol.x(end)-0.0005,2);
end
tps         = linspace(0,roundval,107);
solutions   = deval(RSM_sol, tps);

fprintf(' complete!\n')

%% Get filament coordinates.

Ntps = length(tps);
for i = 1:Ntps
    soldata = solutions(:,i);
    soldata = reshape(soldata,[],model.Nfil)';
    for n = 1:model.Nfil
        fil(n).sol                          = soldata(n,:);
        [coords(n,i).x, coords(n,i).y,~]    = GetFilamentCoordinates(fil(n).sol,1/Q);
    end
end

fprintf('Complete!\n\n')

end % function
