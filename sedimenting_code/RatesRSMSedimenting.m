function [dY] = RatesRSMSedimenting(t,Y,model)

% This code calculates the rate of movement of N filaments using
% the EIF model with forces decribed by the method of
% regularised stokeslets. Code by Smith (2018).

% Inputs:
% - t:      time point to evaluate rates at.
% - Y:      vector [x0,y0,theta] defining the geometry of the filament at time t.
% - model:  struct containing modelling parameters.

% Outputs:
% - dY:     rates vector of d/dt(x0,y0,theta).

%% Get filament data
%  Using input data, the joint coordinates and segment midpoints are
%  calculated.

[Q,ds,fil,all]  = GetFilamentData(Y,model.Nfil,model.G);
Nfil            = model.Nfil;
G               = model.G;

%% Hydrodynamics: the method of regularized stokeslets
% Compute using a semi-analytic method using the analytic solutions of the
% stokeslet integral along a straight line segment (Smith 2009).
  
R  = [ cos(all.th)     -sin(all.th)      zeros(1,Nfil*Q)   ;
       sin(all.th)      cos(all.th)      zeros(1,Nfil*Q)   ;
       zeros(1,Nfil*Q)  zeros(1,Nfil*Q)  ones(1,Nfil*Q)   ];

AH  = RegStokesletAnalyticIntegrals(all.Xm,all.Xm,ds/2,R,model.eps);
D   = size(AH,2)/3;
AH  = AH(1:2*D,1:2*D);

%% Kinematics
%  Calculates the kinematic filament velocity from the X0,theta
%  configuration.

for n = 1:Nfil
    ds        = fil(n).ds;
    fil(n).K  = [ ones(Q,1)  zeros(Q,1) tril(repmat(-ds*sin(fil(n).th(:))',Q,1),-1)+diag(-ds/2*sin(fil(n).th(:))')    ;
                  zeros(Q,1) ones(Q,1)  tril(repmat( ds*cos(fil(n).th(:))',Q,1),-1)+diag( ds/2*cos(fil(n).th(:))')   ];
    fil(n).Kx = fil(n).K(1:Q,:);
    fil(n).Ky = fil(n).K(Q+1:2*Q,:);
end
AK = [blkdiag(fil(1:Nfil).Kx); blkdiag(fil(1:Nfil).Ky)];

%% Elastodynamics
%  Moment balance across each segment and the entire filament, as well as
%  total force balance.
%  Introduction of body force means we have to take total moment around the
%  centre of mass of the filament.
%{x
for n = 1:Nfil
    
    xc  = fil(n).comx;
    yc  = fil(n).comy;
    x   = fil(n).x(1:Q);
    y   = fil(n).y(1:Q);
    xm  = fil(n).xm;
    ym  = fil(n).ym;
    
    for i = 1:Q-1
        for j = i:Q-1
            M(i,j)   = -(-ds*(ym(j+1)-y(i+1)));
            M(i,j+Q) = -( ds*(xm(j+1)-x(i+1)));
        end
    end
    M           = [zeros(Q-1,1),M];
    M           = [ds*(-ym+yc),ds*(xm-xc); M];
    M           = [M; ds*ones(1,Q),zeros(1,Q);zeros(1,Q),ds*ones(1,Q)];
    fil(n).M    = M;
    fil(n).Mx   = M(:,1:Q);
    fil(n).My   = M(:,Q+1:2*Q);
    
    clear M
end
%}
AE = [blkdiag(fil.Mx), blkdiag(fil.My)];

%{
for n = 1:Nfil
    
    % First row is total moment balance:
    Dx     = ds * (fil(n).xm(1:Q) - fil(n).comx);
    Dy     = ds * (fil(n).ym(1:Q) - fil(n).comy);
    
    % Compute elastics equations rows:
    M = -[ triu(repmat( ds*fil(n).y(1:Q)',1,Q)) + triu(repmat(-ds*fil(n).ym,Q,1))  ...
           triu(repmat(-ds*fil(n).x(1:Q)',1,Q)) + triu(repmat( ds*fil(n).xm,Q,1))  ;
           -ds*ones(1,Q)                           zeros(1,Q)                       ;
           zeros(1,Q)                             -ds*ones(1,Q)                ]    ;
    
    % Construct the elastics matrix:
    fil(n).M   = [ -Dy    Dx;    M(2:end,:) ];
    fil(n).Mx  = fil(n).M(:,1:Q);
    fil(n).My  = fil(n).M(:,Q+1:2*Q);
    
    clear Dx Dy M
end

AE = [blkdiag(fil.Mx), blkdiag(fil.My)];
%}


%% Construct linear system
%  Due to introduction of body force, extra terms involving the
%  gravitational parameter G and unit vector ey are present in the RHS
%  vector b.
%  In mbal, extra terms are from: total moment balance; elastodynamic eqn.
%  fbal is the RHS of the force balance eqn.

ey  = [zeros(Q,1);ones(Q,1)];

for n = 1:Nfil
    fil(n).mbal     = [-G*sum(ds*(fil(n).xm-fil(n).comx)); diff(fil(n).th(:))/ds] - G*[zeros(1,2*Q);fil(n).M(2:Q,:)]*ey;
%     fil(n).mbal     = [0; diff(fil(n).th(:))/ds] -G * fil(n).M(1:Q,:) * ey;
    fil(n).fbal     = -G * [0;1];    
    fil(n).RHS      = [fil(n).mbal; fil(n).fbal];
end

b   = [vertcat(fil(1:Nfil).RHS); zeros(Nfil*Q*2,1)];

A   = [zeros(Nfil*(Q+2)) AE; AK -AH];
dZ  = A\b;
dY  = dZ(1:Nfil*(Q+2));

end % function
