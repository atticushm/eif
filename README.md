# Elastohydrodynamic Integral Formulation (EIF)

This repository contains all the MATLAB<sup>&reg;</sup> code used to produce the results in the 2019 paper "Efficient Implementation of Elastohydodynamic Integral Operators" by A.L. Hall-McNair *et al.*, published in Phys. Rev. Fluids 2019 (DOI: https://doi.org/10.1103/PhysRevFluids.4.113101).

## Usage

The folders in this repository each contain code relevant to each of the problems presented in the above manuscript, as well as the associated Supplemental Material.

* `convergence_code` contains code to generate figures related to the convergence results in the Supplemental Material.
* `relaxing_code` contains code for modelling passively relaxing filaments.
* `sedimenting_code` contains code for modelling filaments sedimenting under gravity.
* `shear_code` contains code for modelling filaments in a linear shear flow.
* `swimming_code` contains code for modelling moment-driven active swimmers.
* `supplemental_code` contains supplemental functions used by all other codes.
* `pregen_figures` contains all the pregenerated figures that can be produced by the code in this repository.

In the root folder is `MAINGenerateFigures.m`, which will generate all the figures presented in the manuscript and save them as pdfs in a new `figures` directory. Some of the figures require a large number of simulations to be ran, which can take a sizeable amount of time. In these cases, the script will prompt the user whether or not they would like to download the necessary workspaces from an online repository, or compute them locally.

Other functions will save workspaces they create in a new `workspaces` folder, to avoid the need for recomputation.

## Example

We encourage others to use the EIF in their own projects to model inextensible elastic filaments, and build upon the provided code to model problems of their own. To this end, here we briefly outline the basic steps required to simulate a swimming sperm-like swimmer.

We begin my creating the `model` structure, which contains all the problem setup and relevant parameters. Essential information includes the discretisation parameter *Q*, a 1x2 vector indicating the number of filaments in an *xy* grid, and a 1x2 vector giving the *x* and *y* separation of filaments (if more than one filament is being modelled). Next, parameters specific to the problem are given - for a swimmer, this is the swimming parameter, wave number, and moment amplitude. This structure is created using the `CreateModelStruct.m` function, as follows
```matlab
Q     = 40;     % discretisation parameter
arr   = [1,1];  % filament array dimensions
sep   = [0,0];  % filament separations
S     = 10;     % swimming parameter
k     = 4*pi;   % wave number
m0    = 0.03;   % wave amplitude

model = CreateModelStruct(Q, arr, sep, 'swimming', S, k, m0);
```
The `model` structure also sets the regularisation parameter for use in the method of regularised stokeslets, which defaults to 0.01. 

Next, we specify the initial condition for the problem. Many of the problems in the paper use a parabolic initial condition `Y0`, which is constructed using the function `GetParabolicIntCondition.m` as follows
```matlab
coeff = 0.5;  % parabolic coefficient
Y0 = GetParabolicIntCondition(coeff, model);
```

To begin the simulation, call the function associated to the problem being solved, giving inputs `model`, `Y0` and `tmax`, the upper bound of the time interval over which to solve. In our example:
```matlab
type  = 'sperm' % type of swimmer to model; sperm or worm
[sol, tps, crd] = FilamentsSwimmingFunction(model, Y0, tmax, type);
```
Outputs are solution data `sol`, time points `tps` at which solutions are given, and filament coordinates `crd` at each time point. To view a video of the simulation, frame data can be calculated and played as follows:
```matlab
framedata = GetFrames(model, sol, tps);
fps = 15;     % frame rate at which to play video
PlayMovie(framedata, fps)
```

## Important note

After publication, a few errors were identified in the codes used to produce the figures in the paper. Briefly, these are:

* The initial condition for many of the problems is described in the paper as being sampled from a parabola `y=a*x^2`. In actuality, the figures in the paper were generated using an initial condition which sampled the parabolas mentioned but then scaled to obtain an initial condition of unit arclength. This scaling produces an initial condition which is essentially sampled from a different (steeper) parabola.
* The calculation of the bending moments in the swimming filament code (`RatesRSMSwimming.m`) contained an error. 

The code in this repository has been fixed so as to correct these problems. In the case of the parabolic initial condition problem, the user may select which construction type to use (see code comments for more details). As a result of these fixes, **some of the figures produced by this code differ slightly from figures presented in the paper**.

Additionally, changes to the sedimenting filament code in `RatesRSMSedimenting.m` to improve robustness have slightly affected the recorded dynamics of these simulations in the case of a high elasto-gravitational parameter choice.

Aside from one of the results in Figure 16 (in which increased filament separation is now found to increase average swimming speed), **qualilative behaviour is the same, and the conclusions drawn in each affected case are unchanged**. For the impacted figures and codes, the script `MAINGenerateFigures.m` has been commented in detail describing any differences between the produced figures with those in the paper. For reference, all the figures this code produces, including those affected, are included in the folder `pregen_figures`.

## Authors

* **Atticus Hall-McNair**, University of Birmingham.
* **Meurig Gallagher**, University of Birmingham.
* **David Smith**, University of Birmingham.

## Collaborators

* **Thomas Montenegro-Johnson**, University of Birmingham.
* **Hermes Gadêlha**, University of Bristol.

## Acknowledgements

The code given employs a number of functions by other authors, which have been very useful. Thanks go to
* Douglas M. Schwarz for the MATLAB<sup>&reg;</sup> add-on "Robust and Fast Curve Intersections",
* Ben Mitch for the MATLAB<sup>&reg;</sup> "Panel" toolkit,
* David Romero-Antequera & Paul Proteus for the MATLAB<sup>&reg;</sup> add-on "Text Progress Bar for ODEs",
* The Chebfun team, for the MATLAB<sup>&reg;</sup> package "Chebfun".
