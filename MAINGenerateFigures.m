%% Efficient Implementation of Elastohydrodynamic Integral Formulations
%  Code by Atticus L. Hall-McNair, Meurig T. Gallagher and David J. Smith,
%  University of Birmingham (2019).

% This script generates all the figures in the above manuscript.
% It can be ran in parts, or all at once, and saves pdfs of each figure to
% a ./figure directory.

% ~~~~ PRE-REQUISITES ~~~~

% You will also need to download and install the following MATLAB addons:
% -- Panel, by Douglas Schwarz (included in this repo),
% -- Robust and Fast Curve Intersections, by Ben Mitch (included in this repo),
% -- Text progress bar for ODEs, by David Romero-Antequera (included in this repo),
% -- Deep Learning Toolbox, by MATLAB,
% -- Chebfun, by Chebfun Team (for Figure 5),
% -- Curve Fitting Toolboxes, by MATLAB (for Figure 5),
% -- Statistics and Machine Learning Toolboxes, by MATLAB (for Figure 13).

% ~~~~ IMPORTANT NOTE ~~~~

% In the paper, the initial condition in many of the problems
% is described as being sampled from a parabola of the form y = a*x^2, with
% a chosen depending on the problem. 
% In actuality, the Figures in the paper were generated using an initial
% condition which sampled the parabolas mentioned but then scaled to obtain
% an initial condition of unit arclength. This scaling produces an initial
% condition which is essentially sampled from a different (steeper)
% parabola. 
% To address this, the GetParabolicIntCondition M-file has been altered
% accordingly to also enable the construction of initial conditions which
% better match the language of the paper. Figures generated using this
% initial condition produce the same qualilative results.
% By default, the `scaled' method is used so that this script will produce
% the exact Figures in the paper. The alternative `sampled' is available as
% a function input should the user wish to experiment with this.

%% Make directories in which to save figures and workspaces
%  THIS SECTION MUST BE RUN FIRST!
%  Be sure to run this section first to create the necessary directories.

close all; clc

if exist('/figures','dir') == 7
    mkdir('/figures-backup')
    if isempty(dir('./figures/*figure*')) == 0
        movefile figures/* figures_backup
    end
    rmdir('/figures','s')
end
mkdir('./figures')
mkdir('./workspaces')

addpath(genpath('./'))

% Choose whether to use scaled or sampled parabolic initial condition.
% Figures in the paper use the scaled method (see code header).
global parab_type
parab_type  = 'scale';

%% Figure 2
%  Error convergence of the EIF against a high-resolution BLM.
%  Figure 2 has 3 panels, and is saved as 1 figure.

%  Note that optimisations to the code since publications may yield faster
%  walltimes that those presented in the paper. The general trends and
%  scalability of the methods remains the same however. We felt it best to
%  present the more optimised codes in this repository for a more enjoyable
%  (!) user experience.

clear all
disp('FIGURE 2')

disp('This script can compute solution data for Figure 1,')
disp('or download pre-computed online data')
prompt  = 'Compute solutions locally?\n([Y]es / [N]o):\n';
resp    = input(prompt,'s');

HR      = 200;                                  % Resolution of BLM against which to compare.
Q       = unique(round(logspace(0,2,60)));      % choose Q from a logscale.
Q(Q<4)  = [];                                   % Ensure Q > 4 for meaningful simulations.
if      max(strcmp(resp,{'Y','y','yes','Yes'})) == 1
    if ~exist('./workspaces/figure2-data.mat','file')==1
        ModelError(Q);
    end
    CreateErrorFigures(Q,HR,'local');
elseif  max(strcmp(resp,{'N','n','no','No'})) == 1
    CreateErrorFigures(Q,HR,'online');
end
save2pdf('./figures/figure2.pdf')

close all

%% Figure 3
%  Single filament in a shear flow for four choices of the viscous-elastic 
%  parameter V.
%  Figure 3 has 4 panels, and is saved as 4 figures.

clear all
disp('FIGURE 3')

Q       = 40;       % discretisation parameter.
grid    = [1,1];    % dimensions of filament `grid'.
sep     = [0,0];    % filament separation distances.
th_0    = 0.9*pi;   % initial filament rotation.
tmax    = 5;        % max time.
dth_0   = 0.1;      % initial condition pertubation parameter.

% Figure 3a
V           = 5000;
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
FigShapeWithStreamlines(V,sol,tps,[1,1],'shear',0);
save2pdf('./figures/figure3a.pdf')

% Figure 3b
clear sol tps
V           = 10000;
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
FigShapeWithStreamlines(V,sol,tps,[1,1],'shear',0);
save2pdf('./figures/figure3b.pdf')

% Figure 3c
clear sol tps
V           = 34000;
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
FigShapeWithStreamlines(V,sol,tps,[1,1],'shear',0);
save2pdf('./figures/figure3c.pdf')

% Figure 3d
clear sol tps
V           = 40000;
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
FigShapeWithStreamlines(V,sol,tps,[1,1],'shear',0);
save2pdf('./figures/figure3d.pdf')

close all

%% Figure 4
%  Evolving curvature profile of a single filament in shear flow for two
%  choices of the viscous-elastic parameter V.
%  Figure 4 has two panels, and is saved as two figures.

clear all
disp('FIGURE 4')

Q       = 40;       % discretisation parameter.
grid    = [1,1];    % dimensions of filament `grid'.
sep     = [0,0];    % filament separation distances.
th_0    = 0.9*pi;   % initial filament rotation.
dth_0   = 0.1;      % initial condition pertubation parameter.
tmax    = 6;        % maximum time.

% Figure 4a
V           = 34000;
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
FigFilamentCurvature(Q,V,sol,tps);
save2pdf('./figures/figure4a.pdf')

% Figure 4b
clear sol tps
V           = 40000;
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
FigFilamentCurvature(Q,V,sol,tps);
save2pdf('./figures/figure4b.pdf')

close all

%% Figure 5
%  Interpolating a buckling filament using Chebyshev polynomials.
%  Figure 5 has 4 panels and is saved as 4 figures.

%  Note that CHEBFUN needs to be installed in order for the code in this
%  section to run successfully.

clear all
disp('FIGURE 5')

Q       = 40;       % discretisation parameter.
grid    = [1,1];    % dimensions of filament `grid'.
sep     = [0,0];    % filament separation distances.
th_0    = 0.9*pi;   % initial filament rotation.
dth_0   = 0.1;      % initial condition pertubation parameter.
tmax    = 5;        % maximum time.

% Figure 5a & 5c
V           = 5000;
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
FigChebyshevFitting(V,sol,tps,[1,1]);
figure(2); save2pdf('./figures/figure5a.pdf')
figure(3); save2pdf('./figures/figure5c.pdf')
close all

% Figure 5b & 5d
V           = 40000;
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
FigChebyshevFitting(V,sol,tps,[1,1]);
figure(2); save2pdf('./figures/figure5b.pdf')
figure(3); save2pdf('./figures/figure5d.pdf')

close all

%% Figure 6
%  Single filament sedimenting under gravity for a small parameter choice
%  G.
%  Figure 6 has 4 panels and is saved as 1 figure.

clear all
disp('FIGURE 6')

Q       = 40;       % discretisation parameter.
G       = 300;      % elasto-gravitational parameter.
tmax    = 9e-3;     % maximum time.

model       = CreateModelStruct(Q,[1,1],[0,0],'sedimenting',G);     % model struct for main problem.
model_ps    = CreateModelStruct(10,[1,1],[0,0],'sedimenting',G);    % model struct for pre-solve.
Y0          = GetParabolicIntCond(1e-7,model_ps);                   % low-resolution parabola for pre-solve.
[sol,tps,~] = FilamentsSedimentingFunction(model, Y0, tmax);
FigShapeWithStreamlines(G,sol,tps,[1,1],'sedimenting',0);
save2pdf('./figures/figure6.pdf')

close all

%% Figure 7
%  Single filament sedimenting under gravity for a high G value. 
%  Figure 7 has 4 panels and is saved as 1 figure.

% An error in the labelling of the figure in the manuscript means that the
% panel annotations did not match the time points being plotted. This has
% been remedied below. Minor alterations to the code to improve robustness
% result in very slight differences in the figures produced below.

clear all
disp('FIGURE 7')

Q       = 40;       % discretisation parameter.
G       = 3500;     % elasto-gravitational parameter.
tmax    = 9e-3;     % maximum time.

model       = CreateModelStruct(Q,[1,1],[0,0],'sedimenting',G);     % model struct for main problem.
model_ps    = CreateModelStruct(10,[1,1],[0,0],'sedimenting',G);    % model struct for pre-solve.
Y0          = GetParabolicIntCond(1e-7,model_ps);                   % low-resolution initial condition for pre-solve.
[sol,tps,~] = FilamentsSedimentingFunction(model, Y0, tmax);
FigShapeWithStreamlinesHighG(G,sol,tps,[1,1]);
save2pdf('./figures/figure7.pdf')

close all

%% Figures 8, 9
%  VAL comparison for sperm-like and worm-like swimmers.
%  Figure 8 has two panels and is saved as 1 figure.
%  Figure 9 has 4 panels and is saved as 2 figures.

%  After the paper was published an error in the moment calculation for the
%  swimming filaments was found. Correcting this produces slightly
%  different VAL plots. The authors apologise for this mistake. 
%  Additionally, the VAL has been calculated in a slightly different way (using
%  X_c, the filament centre of mass rather than X_0) to help smooth the
%  data - namely, this removes anomalous results, particularly when the
%  worm-like swimmers are close to self-intersecting.

clear all
close all
disp('FIGURES 8 & 9')

disp('This script can compute solution data for  Figures 8 or 9 (takes')
disp('approx. 24 hours), or download pre-computed online data.')
disp('Online workspaces are ~300MB.')
prompt  = 'Compute solutions locally? \n([Y]es / [N]o):\n';
resp    = input(prompt,'s');
if      max(strcmp(resp,{'Y','y','yes','Yes'})) == 1
    CreateSwimmingFilamentPlots_PAR('local');
elseif  max(strcmp(resp,{'N','n','no','No'})) == 1
    CreateSwimmingFilamentPlots_PAR('online');
else
    disp('Unrecognised response; please try again.')
end

close all

%% Figure 10
%  Two proximal filaments in shear flow at different levels of separation.
%  Figure 11 has 8 panels and is saved as 2 figures.

clear all
disp('FIGURE 10')

Q       = 40;       % discretisation parameter.
grid    = [1,2];    % dimensions of filament grid.
V       = 5000;     % viscous-elastic paramter.
th_0    = 0.9*pi;   % initial filament rotation angle.
dth_0   = 0.1;      % initial condition pertubation parameter
tmax    = 5;        % maximum time.
global sub          % used to identify subfigure annotation positions.

% Figure 11a
sep         = [0.5,0];
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
sub         = 'a';
CreateFilamentFigures(2,'shear',grid,sol,tps,V,1,1);
save2pdf('./figures/figure10a.pdf')

% Figure 11b
sep         = [1,0];
model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
sub         = 'b';
CreateFilamentFigures(2,'shear',grid,sol,tps,V,1,1);
save2pdf('./figures/figure10b.pdf')

close all

%% Figure 11
%  Two proximal segments with different values of V.
%  Figure 11 has X panels and is saved as Y figures.

clear all
disp('FIGURE 11')

Q       = 40;           % discretisation parameter.
grid    = [1,2];        % dimensions of filament array.
sep     = [1,0];        % filament separation distances.
V       = [5e3, 20e3];  % viscous-elastic parameters
th_0    = 0.9*pi;       % initial filament rotation.
dth_0   = 0.1;          % initial condition pertubation parameter.
tmax    = 5;            % maximum time.

model       = CreateModelStruct(Q,grid,sep,'shear',V,th_0);
Y0          = GetPerturbedIntCondYoung(model,dth_0);
[sol,tps,~] = FilamentsShearFunction(model, Y0, tmax);
CreateFilamentFigures(2,'shear',grid,sol,tps,V,1,1);
save2pdf('./figures/figure11.pdf')

close all

%% Figure 12
%  Procustes measure of two proximal filaments in shear flow for varying
%  separations X0 and viscous-elastic parameter V.
%  Figure 12 has X panels and is saved as Y figures.

clear all
disp('FIGURE 12')

FilamentsShearFilSepProcrustesMeasure;
save2pdf('./figures/figure12.pdf')

close all

%% Figure 13
%  Two filaments sedimenting under gravity for different choices of
%  elasto-gravitational parameter G.
%  Figure 13 has 3 panels and is saved as Y figures.

clear all 
disp('FIGURE 13')

Q       = 40;       % discretisation parameter.
sep     = [1.5,0];  % filament separation distances.
grid    = [1,2];    % dimensions of filament array.
tmax    = 3e-3;     % maximum time.

% Figure 13a
clear sol tps
G           = 300;
model       = CreateModelStruct(Q,grid,sep,'sedimenting',G); 
model_ps    = CreateModelStruct(10,grid,sep,'sedimenting',G);
Y0          = GetParabolicIntCond(1e-7,model_ps);
[sol,tps,~] = FilamentsSedimentingFunction(model, Y0, tmax);
FigShapeWithStreamlines(G,sol,tps,grid,'sedimenting',0);
save2pdf('./figures/figure13a.pdf')

% Figure 13b
clear sol tps
G           = 3500;
model       = CreateModelStruct(Q,grid,sep,'sedimenting',G);
model_ps    = CreateModelStruct(10,grid,sep,'sedimenting',G);
Y0          = GetParabolicIntCond(1e-7,model_ps);
[sol,tps,~] = FilamentsSedimentingFunction(model, Y0, tmax);
FigShapeWithStreamlines(G,sol,tps,grid,'sedimenting',0);
save2pdf('./figures/figure13b.pdf')

% Figure 13c
clear sol tps
G           = 5000;
model       = CreateModelStruct(Q,grid,sep,'sedimenting',G); 
model_ps    = CreateModelStruct(10,grid,sep,'sedimenting',G);
Y0          = GetParabolicIntCond(1e-7,model_ps);
[sol,tps,~] = FilamentsSedimentingFunction(model, Y0, tmax);
FigShapeWithStreamlines(G,sol,tps,grid,'sedimenting',0);
save2pdf('./figures/figure13c.pdf')

close all

%% Figure 14
%  Four filaments sedimenting under gravity.
%  Figure 14 has 8 panels and is saved as 2 figures.

%  In the paper we erroneously state that the problems involving more than
%  one sedimenting filament have an initial condition sampled from
%  y=1e-7x^2. However, for these probelem we instead sample and scale 
%  from a slightly steeper parabola y=1e-3x^2 to avoid stiffness issues.

clear all
disp('FIGURE 14')

G       = 3500;     % elasto-gravitational parameter.
grid    = [2,2];    % dimensions of filament array.
Q       = 40;       % discretisation parameter.
tmax    = 3e-3;     % maximum time.

% Figure 14a
clear sol tps
sep         = [1.5,0.5];
model       = CreateModelStruct(Q,grid,sep,'sedimenting',G);
model_ps    = CreateModelStruct(10,grid,sep,'sedimenting',G);
Y0          = GetParabolicIntCond(1e-3,model_ps);
[sol,tps,~] = FilamentsSedimentingFunction(model, Y0, tmax);
FigShapeWithStreamlines(G,sol,tps,grid,'sedimenting',0);
save2pdf('./figures/figure14a.pdf')

clear all
G       = 3500;
grid    = [2,2];
Q       = 40;
tmax    = 3e-3;

% Figure 14b
clear sol tps
sep         = [1.5,1.5];
model       = CreateModelStruct(Q,grid,sep,'sedimenting',G);
model_ps    = CreateModelStruct(10,grid,sep,'sedimenting',G);
Y0          = GetParabolicIntCond(1e-3,model_ps);
[sol,tps,~] = FilamentsSedimentingFunction(model, Y0, tmax);
FigShapeWithStreamlines(G,sol,tps,grid,'sedimenting',0);
save2pdf('./figures/figure14b.pdf')

close all

%% Figure 15
%  Nine filaments sedimenting under gravity.
%  Figure 15 has 8 panels and is saved as 2 figures.

%  Due to the larger number of filaments modelled in these examples, we
%  recommend downloading the workspaces when prompted in the command
%  window. If you'd like, you can run the simulations locally - though this
%  can take a very long time (~24 hours).

disp('FIGURE 15')
prompt  = 'Compute solutions locally? \n([Y]es / [N]o): \n';
resp    = input(prompt,'s');
if      max(strcmp(resp,{'Y','y','yes','Yes'})) == 1

    % Compute solutions locally:
    % Figure 15a
    clear all
    Q       = 40;       % discretisation parameter.
    grid    = [3,3];    % dimensions of filament array.
    sep     = [1.5,1];  % filament separation distances.
    tmax    = 3e-3;     % maximum time.
    G       = 3000;     % elasto-gravitational parameter.
    if ~exist('workspaces/figure15a-data.mat','file')
        model       = CreateModelStruct(Q,grid,sep,'sedimenting',G);
        model_ps    = CreateModelStruct(10,grid,sep,'sedimenting',G);
        Y0          = GetParabolicIntCond(1e-3,model_ps);
        [sol,tps,~] = FilamentsSedimentingFunction(model, Y0, tmax);
    else
        load('workspaces/figure15a-data.mat','sol','tps')
    end
    FigShapeWithStreamlines(G,sol,tps,grid,'sedimenting',0);
    save2pdf('./figures/figure15a.pdf')
    
    % Figure 15b
    clear all
    Q       = 40;
    grid    = [3,3];
    sep     = [1.5,1];
    G       = 3500;
    tmax    = 3e-3;
    if ~exist('workspaces/figure15b-data.mat','file')
        model       = CreateModelStruct(Q,grid,sep,'sedimenting',G);
        model_ps    = CreateModelStruct(10,grid,sep,'sedimenting',G);
        Y0          = GetParabolicIntCond(1e-3,model_ps);
        [sol,tps,~] = FilamentsSedimentingFunction(model,tmax);
    else
        load('workspaces/figure15b-data.mat')
    end
    FigShapeWithStreamlines(G,sol,tps,grid,'sedimenting',0);
    save2pdf('./figures/figure15b.pdf')
    
elseif max(strcmp(resp,{'N','n','no','No'})) == 1
    
    % Download solution data from online repo:
    disp('Downloading online solution data... ')
    url = 'http://edata.bham.ac.uk/409/9/figure15-data.zip';
    websave('./workspaces/figure15-data.zip',url);
    disp('Download complete!')
    unzip('./workspaces/figure15-data.zip','./workspaces')
    
    % Figure 15a
    load('./workspaces/figure15a-data.mat','sol','tps')
    FigShapeWithStreamlines(3000,sol,tps,[3,3],'sedimenting',0);
    save2pdf('./figures/figure15a.pdf')
    
    % Figure 15b
    load('workspaces/figure15b-data.mat')
    FigShapeWithStreamlines(3500,sol,tps,[3,3],'sedimenting',0);
    save2pdf('./figures/figure15b.pdf')
    
end

close all

%% Figure 16
%  Two proximal sperm-like swimmers.

%  Owing to the previously mentioned error in the moment calculation in the
%  swimming filament code, the average VAL against separation distance
%  graph presented in the paper is wrong. The code below produces the fixed
%  figure, which reports a different finding. Nevertheless, we include this
%  example in this repository to illustrate the method's ability to
%  simulate multiple active swimmers.

clear all
disp('FIGURE 16')
disp('This script can compute solution data locally, or')
disp('download the pre-computed workspaces from online (~1GB).')

prompt  = 'Compute solutions locally? \n([Y]es / [N]o): \n';
resp    = input(prompt,'s');
if      max(strcmp(resp,{'Y','y','yes','Yes'})) == 1
    CreateMultiFilamentsSwimmingFigures('local');
    save2pdf('./figures/figure16.pdf')
elseif  max(strcmp(resp,{'N','n','no','No'})) == 1
    CreateMultiFilamentsSwimmingFigures('online');
    save2pdf('./figures/figure16.pdf')
end

close all

%% SUPPLEMENTAL MATERIAL - Figure 1
% Error convergence for a relaxing filament.

clear all
disp('FIGURE S1')
EIFRelaxingConvergence;

%% SUPPLEMENTAL MATERIAL - Figures 2, 3, 4
% Error and shape convergence for a filament in shear flow.

clear all
disp('FIGURE S2, S3 & S4')
EIFShearConvergence;

%% SUPPLEMENTAL MATERIAL - Figure 5
% Error and shape convergence for a filament sedimenting due to gravity.

clear all 
disp('FIGURE S5')
EIFSedimentingConvergence;

%% SUPPLEMENTAL MATERIAL - Figure 6
% Error and shape convergence for a filament sedimenting due to gravity,
% with an initially biased initial condition.

clear all
disp('FIGURE S6')
EIFSedimentingBiasedConvergence;
 
%% SUPPLEMENTAL MATERIAL- Figure 7
% Error convergence for a swimming filament.

% Due to the previously mentioned moment error, the choice of m0 here has
% been reduced to m0=0.016. Convergence results thus look slightly
% different but communicate the same results.

clear all
disp('FIGURE S7')
EIFSwimmingConvergence;

%% SUPPLEMENTAL MATERIAL - Figure 9
% Convergence testing of the bead and link model. 

clear all
disp('FIGURE S7')

disp('This script can compute solution data locally, or')
disp('download the pre-computed workspaces from online (~120MB).')
prompt  = 'Compute solutions locally? \n([Y]es / [N]o): \n';
resp    = input(prompt,'s');
if      max(strcmp(resp,{'Y','y','yes','Yes'})) == 1
    BLMConvergenceTesting('local');
elseif  max(strcmp(resp,{'N','n','no','No'})) == 1
    BLMConvergenceTesting('online');
end
disp('Complete!')

%% SUPPLEMENTAL MATERIAL - Figure 10
% Comparison of the EIF for a sedimenting filament to the method by
% Schoeller et al (2019).

clear all
disp('FIGURE S10')

disp('This script can compute solution data locally, or')
disp('download the pre-computed workspaces from online.')
prompt  = 'Compute solutions locally? \n([Y]es / [N]o): \n';
resp    = input(prompt,'s');
if      max(strcmp(resp,{'Y','y','yes','Yes'})) == 1
    EIFRelativeBending('local');
elseif  max(strcmp(resp,{'N','n','no','No'})) == 1
    EIFRelativeBending('online');
end
disp('Complete!')

% END OF SCRIPT

