function [solutions,tps,coords] = FilamentsRelaxingFunctionRFT(model,Y0,tmax)

%% Filaments relaxing

% Code by A.L. Hall-McNair, University of Birmingham (2018).
% Nfil filaments relaxing in Stokes flow using the method of lines w/ resistive
% force theory, accounting for elastodynamics of the filaments but NOT
% non-local hydrodynamic interactions between filaments.

% Inputs
% - model:      Structure created by CreateModelStruct() containing problem data.
% - Y0:         Initial condition.
% - tmax:       Time at which to stop the simulation.

% Outputs
% - solutions: 	Solution data at time points tps.
% - tps:        Vector of time points at which solution is given.
% - coords:     Coordinate data of filament positions.   

%% Global parameter setup

warning off
fprintf('\nRELAXING FILAMENT(S) (RFT)\n')

Q       = model.Q;
Nfil = model.Nfil;

%% Solve

fprintf('Solve started...')

options = odeset('OutputFcn',@odetpbar);

dY_RFT  = @(t,Y) RatesRFTRelaxing(t,Y,model);
RFT_sol = ode15s(dY_RFT, [0 tmax], Y0,options);

tps_dt      = .0005;
tps         = 0:tps_dt:round(RFT_sol.x(end),2);
solutions   = deval(RFT_sol, tps);

fprintf(' complete! \n')

%% Get filament coordinates.

Ntps = length(tps);
for i = 1:Ntps
    soldata = solutions(:,i);
    soldata = reshape(soldata,[],Nfil)';
    for n = 1:Nfil
        fil(n).sol                          = soldata(n,:);
        [coords(n,i).x, coords(n,i).y,~]    = GetFilamentCoordinates(fil(n).sol,1/Q);
    end
end

fprintf('Complete!\n\n')
    
end % function

