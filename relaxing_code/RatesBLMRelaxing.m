function [dY] = RatesBLMRelaxing(t,Y,model)

% This code is for the case of planar deformation. Simple alterations can
% be made for a 3d scenario.
% blm: bead and link model.

%% Reshape input data as required for bead model functions
%  ode15s feeds input data as 1x3N vector, whereas bead model functions
%  require data as a 3xN matrix.

N       = length(Y)/3;
X(1,:)  = Y(1:N);
X(2,:)  = Y(N+1:2*N);
X(3,:)  = Y(2*N+1:3*N);

%% Main 

K   = model.K;          % non-dimensional parameter. Large value needed to ensure inextensibility.
N   = size(X,2);        % number of beads

Q   = N-1;              % number of links
ds  = 1/Q;              % bead separation/equilibruim distance
    
F   = zeros(3,N);
F   = F + Q*GetBendingForces(X) + K*GetSpringForces(X,ds);
F   = reshape(F',[],1);

stokeslets  = RegStokesletsDJS(Y,Y,model.eps); 
dY          = stokeslets*F;

end % function
