function [solutions,tps, coords] = FilamentsRelaxingFunctionMGG(model,Y0,tmax)

%% Relaxing filaments

% Code by Moreau et al., restructured by A.L. Hall-McNair (2018-19).

% Inputs
% - model:      Structure created by CreateModelStruct() containing problem data.
% - Y0:         Initial condition.
% - tmax:       Time at which to stop the simulation.

% Outputs
% - solutions: 	Solution data at time points tps.
% - tps:        Vector of time points at which solution is given.
% - coords:     Coordinate data of filament positions.   

%% setup 

warning off
close all; fprintf('\nRELAXING FILAMENT(S) (MGG)\n')

global tmax epsilon gamma Sp
Q       = model.Q;
epsilon = model.eps;

% 'sperm number', scaled to match EHD models by Smith & Hall-McNair
% q is the characteristic wavelength of the filament.
q       = 2; 
cT      = 4*pi/( log(2*q/epsilon) + 0.5);
gamma2  = 1.8;  
gamma   = 1/gamma2;
Sp_c    = (gamma2*cT)^(1/4);
Sp      = (Sp_c/Q)^(4/3);

%% Solve.

fprintf('Solve started...')

% The MGG method requires alterations to the initial condition we input due
% to how the non-dimensionalisation in this method is performed:
Y0          = [Q*Y0(1); Q*Y0(2); Y0(3); diff(Y0(3:end))];

options     = odeset('OutputFcn',@odetpbar);
dY_HG       = @(t,Y) relaxation(t,Y,Q);
HG_sol      = ode15s(dY_HG, [0,tmax], Y0, options);

tps_dt      = 0.0005;
tps         = 0:tps_dt:round(HG_sol.x(end),2);
solutions   = deval(HG_sol, tps);

fprintf(' complete!\n')

%% Get coordinate data.

for i = 1:length(tps)   
    [coords(i).x, coords(i).y,~] = coordinates_filament(solutions(:,i),Q);
end

fprintf('Complete!\n\n')

end % main function

%% *********** END OF MAIN SCRIPT ***********
% below are the functions used by hermes' code.
% codes used by the hall-mcnair and smith formulations are stored in their
% own seperate .m files.

% ***** SUMMARY OF FOLLOWING FUNCTIONS *****
% relaxation
% oscillation
% magnetism
% matrixNparam
% matrixNparam_oscillation
% matrix3Nparameters
% coordinates_filament
% coordinates_parabola
% *******************************************

function B=relaxation(t,z,N)
% This function computes the value of \dot{X} at time t by computing A, Q and B from Eq. (20)
% see Appendix VII-C, Eq. (20)

th=zeros(N,1);
th(2)=z(3);

 for i=3:N+1
     th(i)=th(i-1)+z(i+1);
 end

B1=zeros(N+2,1);

B1(N+2)=(th(N+1)-th(N));
for i=N-2:-1:0
    B1(3+i)=(th(i+2)-th(i+1));
end

B1(3)=0;

BB=B1;

% call to the function that computes Sp^4*A*Q
M=matrixNparam(t,z,N);

% solving the linear system
B=M\BB;

end

% **************************************
function B=oscillation(t,z,N,amp)
% This function solves the linear system (20)
% from Appendix VII-C in the case of a pinned end with a forced
% angular actuation

th=zeros(N+1,1);
th(2)=z(3);
for i=3:N+1
    th(i)=th(i-1)+z(i+1);
end
B1=zeros(N+2,1);
B1(N+2)=(th(N+1)-th(N));

% for forced angular actuation
a0p=amp*cos(t);

for i=N-2:-1:0
    B1(3+i)=(th(i+2)-th(i+1));
end

B1(3)=a0p;

M=matrixNparam_oscillation(t,z,N);
B=M\B1;

end

% **************************************
function B=magnetism(t,z,N,Mag,Hx,Hy)
% This function is similar to second_member, but with the magnetic effects added
% (see Appendix VII-E, Eq. 24)
% Hx and Hy are the external magnetic fields, defined in the MAIN file.

th=zeros(N+1,1);
th(2)=z(3);
for i=3:N+1
    th(i)=th(i-1)+z(i+1);
end
B1=zeros(N+2,1);
B2=zeros(N+2,1);
B3=zeros(N+2,1);
B1(N+2)=(th(N+1)-th(N));

% adding the magnetic effects 
B2(N+2)=-Mag(N)*sin(th(N));
B3(N+2)=Mag(N)*cos(th(N));
for i=N-2:-1:0
    B1(3+i)=(th(i+2)-th(i+1));
    B2(3+i)=B2(3+i+1)-Mag(i+1)*sin(th(i+2));
    B3(3+i)=B3(3+i+1)+Mag(i+1)*cos(th(i+2));
end
B1(3)=0;

BB=B1+Hx(t)*B2+Hy(t)*B3;
M=matrixNparam(t,z,N);
B=M\BB;

end

% **************************************
function M=matrixNparam(t,z,N)
% This function fills the matrix Q as defined in the text
% (see Appendix VII-B and C, equation (19) and (20))
% and returns the product Sp^4*A*Q

z3=zeros(3*N,1);
z3(1)=z(1);
z3(N+1)=z(2);
z3(2*N+1)=z(3);
for i=2:N
    z3(2*N+i)=z3(2*N+i-1)+z(i+2);
    z3(i)=z3(i-1)+cos(z3(2*N+i-1));
    z3(N+i)=z3(N+i-1)+sin(z3(2*N+i-1));
end

M3=matrix3Nparameters(t,z3,N);

C1=zeros(N,N);C2=zeros(N,N);C3=zeros(N,N);
for i=1:N
    for j=1:N
        C3(i,j)=i>=j;
    end
end
for i=2:N
    C1(i,i-1)=-sin(z3(2*N+i-1));
    C2(i,i-1)=cos(z3(2*N+i-1));
end
for i=N:-1:3
    for j=i-2:-1:1
        C1(i,j)=C1(i,j+1)-sin(z3(2*N+j));
        C2(i,j)=C2(i,j+1)+cos(z3(2*N+j));
    end
end
C=[ones(N,1),zeros(N,1),C1;zeros(N,1),ones(N,1),C2;zeros(N,2),C3];
M=M3*C;

end

% **************************************
function  M=matrixNparam_oscillation(t,z,N)
% This function is similar to matrixNparam but in the 
% case of a pinned proximal end with a forced angular actuation.

z3=zeros(3*N,1);
z3(1)=z(1);
z3(N+1)=z(2);
z3(2*N+1)=z(3);
for i=2:N
    z3(2*N+i)=z3(2*N+i-1)+z(i+2);
    z3(i)=z3(i-1)+cos(z3(2*N+i-1));
    z3(N+i)=z3(N+i-1)+sin(z3(2*N+i-1));
end

M3=matrix3Nparameters(t,z3,N);

C1=zeros(N,N);C2=zeros(N,N);C3=zeros(N,N);
for i=1:N
    for j=1:N
        C3(i,j)=i>=j;
    end
end
for i=2:N
    C1(i,i-1)=-sin(z3(2*N+i-1));
    C2(i,i-1)=cos(z3(2*N+i-1));
end
for i=N:-1:3
    for j=i-2:-1:1
        C1(i,j)=C1(i,j+1)-sin(z3(2*N+j));
        C2(i,j)=C2(i,j+1)+cos(z3(2*N+j));
    end
end
C=[ones(N,1),zeros(N,1),C1;zeros(N,1),ones(N,1),C2;zeros(N,2),C3];
M=M3(1:N+2,:)*C;

% --- this is where the first two equations are being changed to 
% implement the pinned proximal end

M(1,:)=[1,zeros(1,N+1)];
M(2,:)=[0,1,zeros(1,N)];
M(3,:)=[0,0,1,zeros(1,N-1)];
end

% **************************************
function M=matrix3Nparameters(t,z,N)
% This function fills the matrix defined as A in the text 
% (see Appendix VII-C, equation (20) and following)
% and returns Sp^4 * A

global gamma Sp

x=z(1:N);
y=z(N+1:2*N);
th=z(2*N+1:3*N);
F=zeros(2,3*N);
T=zeros(N,3*N);

for i=1:N
    u=cos(th(i));
    v=sin(th(i));
    F(1,i)=-(gamma*u^2+v^2);
    F(1,N+i)=-(gamma-1)*u*v;
    F(2,i)=-(gamma-1)*u*v;
    F(2,N+i)=-(u^2+gamma*v^2);
    F(1,2*N+i)=1/2*v;
    F(2,2*N+i)=-1/2*u;
end

F=Sp^3*F;

for i=1:N
    for j=i:N
        u=cos(th(j));
        v=sin(th(j));
        A=(x(j)-x(i));
        B=(y(j)-y(i));
        T(i,j)=1/2*v+...
            A*(-gamma+1)*v*u+...
            B*(gamma*u*u+v*v);
        T(i,N+j)=-1/2*u+...
            B*(gamma-1)*v*u-...
            A*(u*u+gamma*v*v);
        T(i,2*N+j)=-1/3-...
            1/2*A*u...
            -1/2*B*v;
    end
end

T=Sp^3*T;

M=[F;T];
end

% **************************************
function [X,Y,TH]=coordinates_filament(z,N)
% This function computes the '3N coordinates' -- X_3N in the text
% from the 'N+2 coordinates' -- X in the text 

% --- input : N+2 coordinates, number of links N
% --- output : X, Y coordinates of the N links
% TH orientation of each link

X=zeros(N+1,1);
Y=zeros(N+1,1);
TH=zeros(N,1);

X(1)=z(1);
Y(1)=z(2);
TH(1)=z(3);

for i=2:N
    X(i)=X(i-1)+cos(TH(i-1));
    Y(i)=Y(i-1)+sin(TH(i-1));
    TH(i)=TH(i-1)+z(i+2);
end

X(N+1)=X(N)+cos(TH(N));
Y(N+1)=Y(N)+sin(TH(N));
X=X/N;
Y=Y/N;

end

% **************************************
function [X,Y,TH]=coordinates_parabola(N,x1,x2)
% This function computes the coordinates of N links of constant length on a parabola y=x^2 arc
% between x=x1 and x=x2
% normalized at the end such that total length=N
% Used for the parabola initial condition

F=@(x) (x*sqrt(1+x^2)+log(x+sqrt(1+x^2)))/2;
f=@(xa,xb) abs(xa-xb)*sqrt(1+(xa+xb)^2);

L_arc=(F(2*x2)-F(2*x1))/2;

% dichotomy initialization
a=L_arc/N;
b=L_arc/N/2;

x=x1;
tol=1e-10;

while abs(x-x2)>tol
    c=(a+b)/2;
    x=x1;
    %abscissa of the last point
    
    for i=1:N
        A=x;
        B=x+c;
        C=B;
        while abs(f(x,C)-c)>tol
            C=(A+B)/2;
            if (f(x,C)-c)>0
                B=C;
            else
                A=C;
            end
        end
        x=C;       
    end
    %dichotomy step
    if (x-x2) > 0
        a=c;
    else
        b=c;
    end
end

%coordinates
X(1)=x1;
x=x1;
for i=1:N
        A=x;
        B=x+c;
        C=B;
        while abs(f(x,C)-c)>tol
            C=(A+B)/2;
            if (f(x,C)-c)>0
                B=C;
            else
                A=C;
            end
        end
        X=[X C];
        x=C;
end

Y=X.^2;
TH=atan((Y(2:end)-Y(1:end-1))./(X(2:end)-X(1:end-1)));

l=f(X(1),X(2));
X=X/(l);
Y=Y/(l);
end
