function [dY] = RatesRFTRelaxing(t,Y,model)

% This code calculates the rate of movement of N filaments using
% the EHD model with forces decribed by the resistive force theory.

% The input parameters are epsilon: the regularization parameter; Y the
% filament configurations vector; N the number of filaments.

% This "reduced" version of the code aims to speed up computation time by
% reformulating the system of equations to avoid excessive inverting of the
% hydrodynamic matrix AC.

% Note that this code has not been setup to handle multiple filaments, is
% bespoke and only for use in "EIFModelError.m".

%% Get filament data.
%  Using input data, the joint coordinates and segment midpoints are
%  calculated.

[Q,ds,fil,~]= GetFilamentData(Y,model.Nfil,[]);

%% Hydrodynamics: resistive force theory.

rft.gamma   = 1.8;
rft.q       = 2;
rft.a       = model.eps;
rft.cT      = 4*pi/( log(2*rft.q/rft.a) + 0.5);

T(1,:)      = cos(fil(1).th);
T(2,:)      = sin(fil(1).th);
N(1,:)      = -sin(fil(1).th);
N(2,:)      = cos(fil(1).th);

I   = eye(2*Q);
TT  = [diag(T(1,:).*T(1,:)), diag(T(1,:).*T(2,:)); diag(T(2,:).*T(1,:)), diag(T(2,:).*T(2,:))];
AH  = rft.cT.* (TT+rft.gamma.*(I-TT))  ;
AH  = -inv(AH);

%% Kinematics
%  Calculates the kinematic filament velocity from the X0,theta
%  configuration.

fil(1).K  = [ ones(Q,1)  zeros(Q,1) tril(repmat(-ds*sin(fil(1).th(:))',Q,1),-1)+diag(-ds/2*sin(fil(1).th(:))')    ;
              zeros(Q,1) ones(Q,1)  tril(repmat( ds*cos(fil(1).th(:))',Q,1),-1)+diag( ds/2*cos(fil(1).th(:))')   ];

fil(1).Kx = fil(1).K(1:Q,:);
fil(1).Ky = fil(1).K(Q+1:2*Q,:);

AK = [blkdiag(fil(1).Kx); blkdiag(fil(1).Ky)];

%% Elastodynamics
%  Moment balance across each segment and the entire filament, as well as
%  total force balance.

AE=-[triu(repmat( ds*fil(1).y(1:Q)',1,Q)) + triu(repmat(-ds*fil(1).ym,Q,1))        ...
    triu(repmat(-ds*fil(1).x(1:Q)',1,Q)) + triu(repmat( ds*fil(1).xm,Q,1))        ;
    ds*ones(1,Q)                          zeros(1,Q)                       ;
    zeros(1,Q)                            ds*ones(1,Q)                ]    ;

  
%% Construct linear system

A   = [zeros(Q+2) AE; AK AH];

for n = 1:1
    fil(n).mom = [0; diff(fil(n).th)'/ds];
    fil(n).dev = diff(fil(n).mom);
    fil(n).RHS = [fil(n).mom; zeros(2,1)];
end
b   = [vertcat(fil(1).RHS); zeros(2*Q,1)];
  
dZ  = A\b;
dY  = dZ(1:(Q+2));

end % function
