function ModelError(Q)

% EIF model error.
% Comparing the EIF method with regularised stokeslets to three other
% methods for modelling flexible filaments:
% - the EIF method with resistive force theory (EIF-RFT),
% - the EIF method by Moreau & Gadelha (2018) (MGG),
% - the bead and link model (BLM).

%% Preliminaries.
%  All data is stored in sensible structures for easy grouping.

tps     = struct;
sol     = struct;
coords  = struct;
times   = struct;

global tmax parab_a
tmax        = 0.02;
parab_a     = 0.5;

%% Loop over all Q.

for k = 1:length(Q)
    
    disp(['Q=',num2str(Q(k))]);
    BLM_K   = 5e6; % viscous-elastic parameter in BLM - high value required to ensure inextensibility.
    model   = CreateModelStruct(Q(k),[1,1],[0,0],'relaxing',BLM_K);
    Y0      = GetParabolicIntCond(parab_a,model);
    
    %% EIF method w/ regularised stokeslets.

    tstart1                                 = tic;   
    [sol(k).rsm,tps(k).rsm,coords(k).rsm]   = FilamentsRelaxingFunction(model, Y0, tmax);
    times.rsm(k)                            = toc(tstart1);

    %% EIF method w/ resistive force theory.

    tstart2                                 = tic;
    [sol(k).rft,tps(k).rft,coords(k).rft]   = FilamentsRelaxingFunctionRFT(model, Y0, tmax);
    times.rft(k)                            = toc(tstart2);

    %% BLM method w/ regularised stokeslets.

    tstart3                                 = tic;
    [sol(k).blm,tps(k).blm,coords(k).blm]   = FilamentsRelaxingFunctionBLM(model, Y0, tmax);
    times.blm(k)                            = toc(tstart3);
    
    %% EIF method by Moreau et al.
    
    % MGG method struggles severly with Q > 100 - we skip this case to keep
    % runtime down.
    if Q(k) == 100
        model.Q                                 = 99;
        Y0                                      = GetParabolicIntCond(parab_a,model);
        tstart4                                 = tic;
        [sol(k).hg.data,tps(k).hg,coords(k).hg] = FilamentsRelaxingFunctionMGG(model, Y0, tmax);
        times.hg(k)                             = toc(tstart4);
    else
        tstart4                                 = tic;
        [sol(k).hg.data,tps(k).hg,coords(k).hg] = FilamentsRelaxingFunctionMGG(model, Y0, tmax);
        times.hg(k)                             = toc(tstart4);
    end
  
    %% Save workspace
    
    if ~exist('./workspaces','dir') == 7
        mkdir('./workspaces')
    end
    save('./workspaces/figure2-data.mat');
    
end

disp('Solves complete!')

end % function



