function [solutions,tps,coords] = FilamentsRelaxingFunctionBLM(model,Y0,tmax)

%% Filaments relaxing

% Code by A.L. Hall-McNair, University of Birmingham (2018).
% Nfil filaments relaxing in Stokes flow using the bead and link model,
% which uses the method of regularised stokeslets.

% Inputs
% - model:      Structure created by CreateModelStruct() containing problem data.
% - Y0:         Initial condition.
% - tmax:       Time at which to stop the simulation.

% Outputs
% - solutions: 	Solution data at time points tps.
% - tps:        Vector of time points at which solution is given.
% - coords:     Coordinate data of filament positions.   


%% Global parameter setup

warning off
close all; fprintf('\nRELAXING FILAMENT(S) (BLM)\n')

Q       = model.Q;
Nfil    = model.Nfil;
    
%% Initial condition: NxM grid of filaments
% The "int" toggle is to use a different initial condition when being
% called for the BLM convergence tests (see Supplemental Material)

if ~IsField(model,'int')
    [x,y,~] = GetFilamentCoordinates(Y0,1/Q);
else
    Nfil = 1;
    x = linspace(0,1,Q+1);
    y = 0.5*(x-0.5).^2;
end
Y0      = [x(:); y(:); zeros(Q+1,1)];

%% Solve

fprintf('Solve started...')

options = odeset('OutputFcn',@odetpbar);

dY_BLM  = @(t,Y) RatesBLMRelaxing(t,Y,model);
BLM_sol = ode15s(dY_BLM, [0 tmax], Y0,options);

tps_dt      = .0005;
tps         = 0:tps_dt:round(BLM_sol.x(end),2);
solutions   = deval(BLM_sol, tps);

fprintf(' complete! \n')

%% Get coordinate data.

Ntps    = length(tps);

for i = 1:Ntps  
    soldata = solutions(:,i);
    soldata = reshape(soldata,[],Nfil)';
    for n = 1:Nfil
        N               = Q+1;
        coords(n,i).x   = soldata(n,1:N);
        coords(n,i).y   = soldata(n,N+1:2*N);
    end
end
fprintf('Complete!\n\n')

end % function